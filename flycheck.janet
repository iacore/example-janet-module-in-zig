(import ./zig-out/lib/libjanet-zig :as "zig")

(assert (= :string (type (zig/hello-native))))
(assert (= 3 (zig/+ 1 2)))
(let [[suc b] (protect (print (zig/+ 1 "hi")))]
  (assert (not suc)))

(print "(janet) All tests passed")
