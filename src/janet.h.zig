pub const __builtin_bswap16 = @import("std").zig.c_builtins.__builtin_bswap16;
pub const __builtin_bswap32 = @import("std").zig.c_builtins.__builtin_bswap32;
pub const __builtin_bswap64 = @import("std").zig.c_builtins.__builtin_bswap64;
pub const __builtin_signbit = @import("std").zig.c_builtins.__builtin_signbit;
pub const __builtin_signbitf = @import("std").zig.c_builtins.__builtin_signbitf;
pub const __builtin_popcount = @import("std").zig.c_builtins.__builtin_popcount;
pub const __builtin_ctz = @import("std").zig.c_builtins.__builtin_ctz;
pub const __builtin_clz = @import("std").zig.c_builtins.__builtin_clz;
pub const __builtin_sqrt = @import("std").zig.c_builtins.__builtin_sqrt;
pub const __builtin_sqrtf = @import("std").zig.c_builtins.__builtin_sqrtf;
pub const __builtin_sin = @import("std").zig.c_builtins.__builtin_sin;
pub const __builtin_sinf = @import("std").zig.c_builtins.__builtin_sinf;
pub const __builtin_cos = @import("std").zig.c_builtins.__builtin_cos;
pub const __builtin_cosf = @import("std").zig.c_builtins.__builtin_cosf;
pub const __builtin_exp = @import("std").zig.c_builtins.__builtin_exp;
pub const __builtin_expf = @import("std").zig.c_builtins.__builtin_expf;
pub const __builtin_exp2 = @import("std").zig.c_builtins.__builtin_exp2;
pub const __builtin_exp2f = @import("std").zig.c_builtins.__builtin_exp2f;
pub const __builtin_log = @import("std").zig.c_builtins.__builtin_log;
pub const __builtin_logf = @import("std").zig.c_builtins.__builtin_logf;
pub const __builtin_log2 = @import("std").zig.c_builtins.__builtin_log2;
pub const __builtin_log2f = @import("std").zig.c_builtins.__builtin_log2f;
pub const __builtin_log10 = @import("std").zig.c_builtins.__builtin_log10;
pub const __builtin_log10f = @import("std").zig.c_builtins.__builtin_log10f;
pub const __builtin_abs = @import("std").zig.c_builtins.__builtin_abs;
pub const __builtin_fabs = @import("std").zig.c_builtins.__builtin_fabs;
pub const __builtin_fabsf = @import("std").zig.c_builtins.__builtin_fabsf;
pub const __builtin_floor = @import("std").zig.c_builtins.__builtin_floor;
pub const __builtin_floorf = @import("std").zig.c_builtins.__builtin_floorf;
pub const __builtin_ceil = @import("std").zig.c_builtins.__builtin_ceil;
pub const __builtin_ceilf = @import("std").zig.c_builtins.__builtin_ceilf;
pub const __builtin_trunc = @import("std").zig.c_builtins.__builtin_trunc;
pub const __builtin_truncf = @import("std").zig.c_builtins.__builtin_truncf;
pub const __builtin_round = @import("std").zig.c_builtins.__builtin_round;
pub const __builtin_roundf = @import("std").zig.c_builtins.__builtin_roundf;
pub const __builtin_strlen = @import("std").zig.c_builtins.__builtin_strlen;
pub const __builtin_strcmp = @import("std").zig.c_builtins.__builtin_strcmp;
pub const __builtin_object_size = @import("std").zig.c_builtins.__builtin_object_size;
pub const __builtin___memset_chk = @import("std").zig.c_builtins.__builtin___memset_chk;
pub const __builtin_memset = @import("std").zig.c_builtins.__builtin_memset;
pub const __builtin___memcpy_chk = @import("std").zig.c_builtins.__builtin___memcpy_chk;
pub const __builtin_memcpy = @import("std").zig.c_builtins.__builtin_memcpy;
pub const __builtin_expect = @import("std").zig.c_builtins.__builtin_expect;
pub const __builtin_nanf = @import("std").zig.c_builtins.__builtin_nanf;
pub const __builtin_huge_valf = @import("std").zig.c_builtins.__builtin_huge_valf;
pub const __builtin_inff = @import("std").zig.c_builtins.__builtin_inff;
pub const __builtin_isnan = @import("std").zig.c_builtins.__builtin_isnan;
pub const __builtin_isinf = @import("std").zig.c_builtins.__builtin_isinf;
pub const __builtin_isinf_sign = @import("std").zig.c_builtins.__builtin_isinf_sign;
pub const __has_builtin = @import("std").zig.c_builtins.__has_builtin;
pub const __builtin_assume = @import("std").zig.c_builtins.__builtin_assume;
pub const __builtin_unreachable = @import("std").zig.c_builtins.__builtin_unreachable;
pub const __builtin_constant_p = @import("std").zig.c_builtins.__builtin_constant_p;
pub const __builtin_mul_overflow = @import("std").zig.c_builtins.__builtin_mul_overflow;
pub const JanetBuildConfig = extern struct {
    major: c_uint,
    minor: c_uint,
    patch: c_uint,
    bits: c_uint,
};
pub const struct_JanetOSMutex = opaque {};
pub const JanetOSMutex = struct_JanetOSMutex;
pub const struct_JanetOSRWLock = opaque {};
pub const JanetOSRWLock = struct_JanetOSRWLock;
pub const wchar_t = c_int;
pub const _Float32 = f32;
pub const _Float64 = f64;
pub const _Float32x = f64;
pub const _Float64x = c_longdouble;
pub const div_t = extern struct {
    quot: c_int,
    rem: c_int,
};
pub const ldiv_t = extern struct {
    quot: c_long,
    rem: c_long,
};
pub const lldiv_t = extern struct {
    quot: c_longlong,
    rem: c_longlong,
};
pub extern fn __ctype_get_mb_cur_max() usize;
pub extern fn atof(__nptr: [*c]const u8) f64;
pub extern fn atoi(__nptr: [*c]const u8) c_int;
pub extern fn atol(__nptr: [*c]const u8) c_long;
pub extern fn atoll(__nptr: [*c]const u8) c_longlong;
pub extern fn strtod(__nptr: [*c]const u8, __endptr: [*c][*c]u8) f64;
pub extern fn strtof(__nptr: [*c]const u8, __endptr: [*c][*c]u8) f32;
pub extern fn strtold(__nptr: [*c]const u8, __endptr: [*c][*c]u8) c_longdouble;
pub extern fn strtol(__nptr: [*c]const u8, __endptr: [*c][*c]u8, __base: c_int) c_long;
pub extern fn strtoul(__nptr: [*c]const u8, __endptr: [*c][*c]u8, __base: c_int) c_ulong;
pub extern fn strtoq(noalias __nptr: [*c]const u8, noalias __endptr: [*c][*c]u8, __base: c_int) c_longlong;
pub extern fn strtouq(noalias __nptr: [*c]const u8, noalias __endptr: [*c][*c]u8, __base: c_int) c_ulonglong;
pub extern fn strtoll(__nptr: [*c]const u8, __endptr: [*c][*c]u8, __base: c_int) c_longlong;
pub extern fn strtoull(__nptr: [*c]const u8, __endptr: [*c][*c]u8, __base: c_int) c_ulonglong;
pub extern fn l64a(__n: c_long) [*c]u8;
pub extern fn a64l(__s: [*c]const u8) c_long;
pub const __u_char = u8;
pub const __u_short = c_ushort;
pub const __u_int = c_uint;
pub const __u_long = c_ulong;
pub const __int8_t = i8;
pub const __uint8_t = u8;
pub const __int16_t = c_short;
pub const __uint16_t = c_ushort;
pub const __int32_t = c_int;
pub const __uint32_t = c_uint;
pub const __int64_t = c_long;
pub const __uint64_t = c_ulong;
pub const __int_least8_t = __int8_t;
pub const __uint_least8_t = __uint8_t;
pub const __int_least16_t = __int16_t;
pub const __uint_least16_t = __uint16_t;
pub const __int_least32_t = __int32_t;
pub const __uint_least32_t = __uint32_t;
pub const __int_least64_t = __int64_t;
pub const __uint_least64_t = __uint64_t;
pub const __quad_t = c_long;
pub const __u_quad_t = c_ulong;
pub const __intmax_t = c_long;
pub const __uintmax_t = c_ulong;
pub const __dev_t = c_ulong;
pub const __uid_t = c_uint;
pub const __gid_t = c_uint;
pub const __ino_t = c_ulong;
pub const __ino64_t = c_ulong;
pub const __mode_t = c_uint;
pub const __nlink_t = c_ulong;
pub const __off_t = c_long;
pub const __off64_t = c_long;
pub const __pid_t = c_int;
pub const __fsid_t = extern struct {
    __val: [2]c_int,
};
pub const __clock_t = c_long;
pub const __rlim_t = c_ulong;
pub const __rlim64_t = c_ulong;
pub const __id_t = c_uint;
pub const __time_t = c_long;
pub const __useconds_t = c_uint;
pub const __suseconds_t = c_long;
pub const __suseconds64_t = c_long;
pub const __daddr_t = c_int;
pub const __key_t = c_int;
pub const __clockid_t = c_int;
pub const __timer_t = ?*anyopaque;
pub const __blksize_t = c_long;
pub const __blkcnt_t = c_long;
pub const __blkcnt64_t = c_long;
pub const __fsblkcnt_t = c_ulong;
pub const __fsblkcnt64_t = c_ulong;
pub const __fsfilcnt_t = c_ulong;
pub const __fsfilcnt64_t = c_ulong;
pub const __fsword_t = c_long;
pub const __ssize_t = c_long;
pub const __syscall_slong_t = c_long;
pub const __syscall_ulong_t = c_ulong;
pub const __loff_t = __off64_t;
pub const __caddr_t = [*c]u8;
pub const __intptr_t = c_long;
pub const __socklen_t = c_uint;
pub const __sig_atomic_t = c_int;
pub const u_char = __u_char;
pub const u_short = __u_short;
pub const u_int = __u_int;
pub const u_long = __u_long;
pub const quad_t = __quad_t;
pub const u_quad_t = __u_quad_t;
pub const fsid_t = __fsid_t;
pub const loff_t = __loff_t;
pub const ino_t = __ino_t;
pub const dev_t = __dev_t;
pub const gid_t = __gid_t;
pub const mode_t = __mode_t;
pub const nlink_t = __nlink_t;
pub const uid_t = __uid_t;
pub const off_t = __off_t;
pub const pid_t = __pid_t;
pub const id_t = __id_t;
pub const daddr_t = __daddr_t;
pub const caddr_t = __caddr_t;
pub const key_t = __key_t;
pub const clock_t = __clock_t;
pub const clockid_t = __clockid_t;
pub const time_t = __time_t;
pub const timer_t = __timer_t;
pub const ulong = c_ulong;
pub const ushort = c_ushort;
pub const uint = c_uint;
pub const u_int8_t = __uint8_t;
pub const u_int16_t = __uint16_t;
pub const u_int32_t = __uint32_t;
pub const u_int64_t = __uint64_t;
pub const register_t = c_long;
pub fn __bswap_16(arg___bsx: __uint16_t) callconv(.C) __uint16_t {
    var __bsx = arg___bsx;
    return @bitCast(__uint16_t, @truncate(c_short, ((@bitCast(c_int, @as(c_uint, __bsx)) >> @intCast(@import("std").math.Log2Int(c_int), 8)) & @as(c_int, 255)) | ((@bitCast(c_int, @as(c_uint, __bsx)) & @as(c_int, 255)) << @intCast(@import("std").math.Log2Int(c_int), 8))));
}
pub fn __bswap_32(arg___bsx: __uint32_t) callconv(.C) __uint32_t {
    var __bsx = arg___bsx;
    return ((((__bsx & @as(c_uint, 4278190080)) >> @intCast(@import("std").math.Log2Int(c_uint), 24)) | ((__bsx & @as(c_uint, 16711680)) >> @intCast(@import("std").math.Log2Int(c_uint), 8))) | ((__bsx & @as(c_uint, 65280)) << @intCast(@import("std").math.Log2Int(c_uint), 8))) | ((__bsx & @as(c_uint, 255)) << @intCast(@import("std").math.Log2Int(c_uint), 24));
}
pub fn __bswap_64(arg___bsx: __uint64_t) callconv(.C) __uint64_t {
    var __bsx = arg___bsx;
    return @bitCast(__uint64_t, @truncate(c_ulong, ((((((((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 18374686479671623680)) >> @intCast(@import("std").math.Log2Int(c_ulonglong), 56)) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 71776119061217280)) >> @intCast(@import("std").math.Log2Int(c_ulonglong), 40))) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 280375465082880)) >> @intCast(@import("std").math.Log2Int(c_ulonglong), 24))) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 1095216660480)) >> @intCast(@import("std").math.Log2Int(c_ulonglong), 8))) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 4278190080)) << @intCast(@import("std").math.Log2Int(c_ulonglong), 8))) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 16711680)) << @intCast(@import("std").math.Log2Int(c_ulonglong), 24))) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 65280)) << @intCast(@import("std").math.Log2Int(c_ulonglong), 40))) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 255)) << @intCast(@import("std").math.Log2Int(c_ulonglong), 56))));
}
pub fn __uint16_identity(arg___x: __uint16_t) callconv(.C) __uint16_t {
    var __x = arg___x;
    return __x;
}
pub fn __uint32_identity(arg___x: __uint32_t) callconv(.C) __uint32_t {
    var __x = arg___x;
    return __x;
}
pub fn __uint64_identity(arg___x: __uint64_t) callconv(.C) __uint64_t {
    var __x = arg___x;
    return __x;
}
pub const __sigset_t = extern struct {
    __val: [16]c_ulong,
};
pub const sigset_t = __sigset_t;
pub const struct_timeval = extern struct {
    tv_sec: __time_t,
    tv_usec: __suseconds_t,
};
pub const struct_timespec = extern struct {
    tv_sec: __time_t,
    tv_nsec: __syscall_slong_t,
};
pub const suseconds_t = __suseconds_t;
pub const __fd_mask = c_long;
pub const fd_set = extern struct {
    __fds_bits: [16]__fd_mask,
};
pub const fd_mask = __fd_mask;
pub extern fn select(__nfds: c_int, noalias __readfds: [*c]fd_set, noalias __writefds: [*c]fd_set, noalias __exceptfds: [*c]fd_set, noalias __timeout: [*c]struct_timeval) c_int;
pub extern fn pselect(__nfds: c_int, noalias __readfds: [*c]fd_set, noalias __writefds: [*c]fd_set, noalias __exceptfds: [*c]fd_set, noalias __timeout: [*c]const struct_timespec, noalias __sigmask: [*c]const __sigset_t) c_int;
pub const blksize_t = __blksize_t;
pub const blkcnt_t = __blkcnt_t;
pub const fsblkcnt_t = __fsblkcnt_t;
pub const fsfilcnt_t = __fsfilcnt_t;
const struct_unnamed_1 = extern struct {
    __low: c_uint,
    __high: c_uint,
};
pub const __atomic_wide_counter = extern union {
    __value64: c_ulonglong,
    __value32: struct_unnamed_1,
};
pub const struct___pthread_internal_list = extern struct {
    __prev: [*c]struct___pthread_internal_list,
    __next: [*c]struct___pthread_internal_list,
};
pub const __pthread_list_t = struct___pthread_internal_list;
pub const struct___pthread_internal_slist = extern struct {
    __next: [*c]struct___pthread_internal_slist,
};
pub const __pthread_slist_t = struct___pthread_internal_slist;
pub const struct___pthread_mutex_s = extern struct {
    __lock: c_int,
    __count: c_uint,
    __owner: c_int,
    __nusers: c_uint,
    __kind: c_int,
    __spins: c_short,
    __elision: c_short,
    __list: __pthread_list_t,
};
pub const struct___pthread_rwlock_arch_t = extern struct {
    __readers: c_uint,
    __writers: c_uint,
    __wrphase_futex: c_uint,
    __writers_futex: c_uint,
    __pad3: c_uint,
    __pad4: c_uint,
    __cur_writer: c_int,
    __shared: c_int,
    __rwelision: i8,
    __pad1: [7]u8,
    __pad2: c_ulong,
    __flags: c_uint,
};
pub const struct___pthread_cond_s = extern struct {
    __wseq: __atomic_wide_counter,
    __g1_start: __atomic_wide_counter,
    __g_refs: [2]c_uint,
    __g_size: [2]c_uint,
    __g1_orig_size: c_uint,
    __wrefs: c_uint,
    __g_signals: [2]c_uint,
};
pub const __tss_t = c_uint;
pub const __thrd_t = c_ulong;
pub const __once_flag = extern struct {
    __data: c_int,
};
pub const pthread_t = c_ulong;
pub const pthread_mutexattr_t = extern union {
    __size: [4]u8,
    __align: c_int,
};
pub const pthread_condattr_t = extern union {
    __size: [4]u8,
    __align: c_int,
};
pub const pthread_key_t = c_uint;
pub const pthread_once_t = c_int;
pub const union_pthread_attr_t = extern union {
    __size: [56]u8,
    __align: c_long,
};
pub const pthread_attr_t = union_pthread_attr_t;
pub const pthread_mutex_t = extern union {
    __data: struct___pthread_mutex_s,
    __size: [40]u8,
    __align: c_long,
};
pub const pthread_cond_t = extern union {
    __data: struct___pthread_cond_s,
    __size: [48]u8,
    __align: c_longlong,
};
pub const pthread_rwlock_t = extern union {
    __data: struct___pthread_rwlock_arch_t,
    __size: [56]u8,
    __align: c_long,
};
pub const pthread_rwlockattr_t = extern union {
    __size: [8]u8,
    __align: c_long,
};
pub const pthread_spinlock_t = c_int;
pub const pthread_barrier_t = extern union {
    __size: [32]u8,
    __align: c_long,
};
pub const pthread_barrierattr_t = extern union {
    __size: [4]u8,
    __align: c_int,
};
pub extern fn random() c_long;
pub extern fn srandom(__seed: c_uint) void;
pub extern fn initstate(__seed: c_uint, __statebuf: [*c]u8, __statelen: usize) [*c]u8;
pub extern fn setstate(__statebuf: [*c]u8) [*c]u8;
pub const struct_random_data = extern struct {
    fptr: [*c]i32,
    rptr: [*c]i32,
    state: [*c]i32,
    rand_type: c_int,
    rand_deg: c_int,
    rand_sep: c_int,
    end_ptr: [*c]i32,
};
pub extern fn random_r(noalias __buf: [*c]struct_random_data, noalias __result: [*c]i32) c_int;
pub extern fn srandom_r(__seed: c_uint, __buf: [*c]struct_random_data) c_int;
pub extern fn initstate_r(__seed: c_uint, noalias __statebuf: [*c]u8, __statelen: usize, noalias __buf: [*c]struct_random_data) c_int;
pub extern fn setstate_r(noalias __statebuf: [*c]u8, noalias __buf: [*c]struct_random_data) c_int;
pub extern fn rand() c_int;
pub extern fn srand(__seed: c_uint) void;
pub extern fn rand_r(__seed: [*c]c_uint) c_int;
pub extern fn drand48() f64;
pub extern fn erand48(__xsubi: [*c]c_ushort) f64;
pub extern fn lrand48() c_long;
pub extern fn nrand48(__xsubi: [*c]c_ushort) c_long;
pub extern fn mrand48() c_long;
pub extern fn jrand48(__xsubi: [*c]c_ushort) c_long;
pub extern fn srand48(__seedval: c_long) void;
pub extern fn seed48(__seed16v: [*c]c_ushort) [*c]c_ushort;
pub extern fn lcong48(__param: [*c]c_ushort) void;
pub const struct_drand48_data = extern struct {
    __x: [3]c_ushort,
    __old_x: [3]c_ushort,
    __c: c_ushort,
    __init: c_ushort,
    __a: c_ulonglong,
};
pub extern fn drand48_r(noalias __buffer: [*c]struct_drand48_data, noalias __result: [*c]f64) c_int;
pub extern fn erand48_r(__xsubi: [*c]c_ushort, noalias __buffer: [*c]struct_drand48_data, noalias __result: [*c]f64) c_int;
pub extern fn lrand48_r(noalias __buffer: [*c]struct_drand48_data, noalias __result: [*c]c_long) c_int;
pub extern fn nrand48_r(__xsubi: [*c]c_ushort, noalias __buffer: [*c]struct_drand48_data, noalias __result: [*c]c_long) c_int;
pub extern fn mrand48_r(noalias __buffer: [*c]struct_drand48_data, noalias __result: [*c]c_long) c_int;
pub extern fn jrand48_r(__xsubi: [*c]c_ushort, noalias __buffer: [*c]struct_drand48_data, noalias __result: [*c]c_long) c_int;
pub extern fn srand48_r(__seedval: c_long, __buffer: [*c]struct_drand48_data) c_int;
pub extern fn seed48_r(__seed16v: [*c]c_ushort, __buffer: [*c]struct_drand48_data) c_int;
pub extern fn lcong48_r(__param: [*c]c_ushort, __buffer: [*c]struct_drand48_data) c_int;
pub extern fn arc4random() __uint32_t;
pub extern fn arc4random_buf(__buf: ?*anyopaque, __size: usize) void;
pub extern fn arc4random_uniform(__upper_bound: __uint32_t) __uint32_t;
pub extern fn malloc(__size: c_ulong) ?*anyopaque;
pub extern fn calloc(__nmemb: c_ulong, __size: c_ulong) ?*anyopaque;
pub extern fn realloc(__ptr: ?*anyopaque, __size: c_ulong) ?*anyopaque;
pub extern fn free(__ptr: ?*anyopaque) void;
pub extern fn reallocarray(__ptr: ?*anyopaque, __nmemb: usize, __size: usize) ?*anyopaque;
pub extern fn alloca(__size: c_ulong) ?*anyopaque;
pub extern fn valloc(__size: usize) ?*anyopaque;
pub extern fn posix_memalign(__memptr: [*c]?*anyopaque, __alignment: usize, __size: usize) c_int;
pub extern fn aligned_alloc(__alignment: c_ulong, __size: c_ulong) ?*anyopaque;
pub extern fn abort() noreturn;
pub extern fn atexit(__func: ?*const fn () callconv(.C) void) c_int;
pub extern fn at_quick_exit(__func: ?*const fn () callconv(.C) void) c_int;
pub extern fn on_exit(__func: ?*const fn (c_int, ?*anyopaque) callconv(.C) void, __arg: ?*anyopaque) c_int;
pub extern fn exit(__status: c_int) noreturn;
pub extern fn quick_exit(__status: c_int) noreturn;
pub extern fn _Exit(__status: c_int) noreturn;
pub extern fn getenv(__name: [*c]const u8) [*c]u8;
pub extern fn putenv(__string: [*c]u8) c_int;
pub extern fn setenv(__name: [*c]const u8, __value: [*c]const u8, __replace: c_int) c_int;
pub extern fn unsetenv(__name: [*c]const u8) c_int;
pub extern fn clearenv() c_int;
pub extern fn mktemp(__template: [*c]u8) [*c]u8;
pub extern fn mkstemp(__template: [*c]u8) c_int;
pub extern fn mkstemps(__template: [*c]u8, __suffixlen: c_int) c_int;
pub extern fn mkdtemp(__template: [*c]u8) [*c]u8;
pub extern fn system(__command: [*c]const u8) c_int;
pub extern fn realpath(noalias __name: [*c]const u8, noalias __resolved: [*c]u8) [*c]u8;
pub const __compar_fn_t = ?*const fn (?*const anyopaque, ?*const anyopaque) callconv(.C) c_int;
pub extern fn bsearch(__key: ?*const anyopaque, __base: ?*const anyopaque, __nmemb: usize, __size: usize, __compar: __compar_fn_t) ?*anyopaque;
pub extern fn qsort(__base: ?*anyopaque, __nmemb: usize, __size: usize, __compar: __compar_fn_t) void;
pub extern fn abs(__x: c_int) c_int;
pub extern fn labs(__x: c_long) c_long;
pub extern fn llabs(__x: c_longlong) c_longlong;
pub extern fn div(__numer: c_int, __denom: c_int) div_t;
pub extern fn ldiv(__numer: c_long, __denom: c_long) ldiv_t;
pub extern fn lldiv(__numer: c_longlong, __denom: c_longlong) lldiv_t;
pub extern fn ecvt(__value: f64, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int) [*c]u8;
pub extern fn fcvt(__value: f64, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int) [*c]u8;
pub extern fn gcvt(__value: f64, __ndigit: c_int, __buf: [*c]u8) [*c]u8;
pub extern fn qecvt(__value: c_longdouble, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int) [*c]u8;
pub extern fn qfcvt(__value: c_longdouble, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int) [*c]u8;
pub extern fn qgcvt(__value: c_longdouble, __ndigit: c_int, __buf: [*c]u8) [*c]u8;
pub extern fn ecvt_r(__value: f64, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int, noalias __buf: [*c]u8, __len: usize) c_int;
pub extern fn fcvt_r(__value: f64, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int, noalias __buf: [*c]u8, __len: usize) c_int;
pub extern fn qecvt_r(__value: c_longdouble, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int, noalias __buf: [*c]u8, __len: usize) c_int;
pub extern fn qfcvt_r(__value: c_longdouble, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int, noalias __buf: [*c]u8, __len: usize) c_int;
pub extern fn mblen(__s: [*c]const u8, __n: usize) c_int;
pub extern fn mbtowc(noalias __pwc: [*c]wchar_t, noalias __s: [*c]const u8, __n: usize) c_int;
pub extern fn wctomb(__s: [*c]u8, __wchar: wchar_t) c_int;
pub extern fn mbstowcs(noalias __pwcs: [*c]wchar_t, noalias __s: [*c]const u8, __n: usize) usize;
pub extern fn wcstombs(noalias __s: [*c]u8, noalias __pwcs: [*c]const wchar_t, __n: usize) usize;
pub extern fn rpmatch(__response: [*c]const u8) c_int;
pub extern fn getsubopt(noalias __optionp: [*c][*c]u8, noalias __tokens: [*c]const [*c]u8, noalias __valuep: [*c][*c]u8) c_int;
pub extern fn getloadavg(__loadavg: [*c]f64, __nelem: c_int) c_int;
pub const int_least8_t = __int_least8_t;
pub const int_least16_t = __int_least16_t;
pub const int_least32_t = __int_least32_t;
pub const int_least64_t = __int_least64_t;
pub const uint_least8_t = __uint_least8_t;
pub const uint_least16_t = __uint_least16_t;
pub const uint_least32_t = __uint_least32_t;
pub const uint_least64_t = __uint_least64_t;
pub const int_fast8_t = i8;
pub const int_fast16_t = c_long;
pub const int_fast32_t = c_long;
pub const int_fast64_t = c_long;
pub const uint_fast8_t = u8;
pub const uint_fast16_t = c_ulong;
pub const uint_fast32_t = c_ulong;
pub const uint_fast64_t = c_ulong;
pub const intmax_t = __intmax_t;
pub const uintmax_t = __uintmax_t;
pub extern fn memcpy(__dest: ?*anyopaque, __src: ?*const anyopaque, __n: c_ulong) ?*anyopaque;
pub extern fn memmove(__dest: ?*anyopaque, __src: ?*const anyopaque, __n: c_ulong) ?*anyopaque;
pub extern fn memccpy(__dest: ?*anyopaque, __src: ?*const anyopaque, __c: c_int, __n: c_ulong) ?*anyopaque;
pub extern fn memset(__s: ?*anyopaque, __c: c_int, __n: c_ulong) ?*anyopaque;
pub extern fn memcmp(__s1: ?*const anyopaque, __s2: ?*const anyopaque, __n: c_ulong) c_int;
pub extern fn __memcmpeq(__s1: ?*const anyopaque, __s2: ?*const anyopaque, __n: usize) c_int;
pub extern fn memchr(__s: ?*const anyopaque, __c: c_int, __n: c_ulong) ?*anyopaque;
pub extern fn strcpy(__dest: [*c]u8, __src: [*c]const u8) [*c]u8;
pub extern fn strncpy(__dest: [*c]u8, __src: [*c]const u8, __n: c_ulong) [*c]u8;
pub extern fn strcat(__dest: [*c]u8, __src: [*c]const u8) [*c]u8;
pub extern fn strncat(__dest: [*c]u8, __src: [*c]const u8, __n: c_ulong) [*c]u8;
pub extern fn strcmp(__s1: [*c]const u8, __s2: [*c]const u8) c_int;
pub extern fn strncmp(__s1: [*c]const u8, __s2: [*c]const u8, __n: c_ulong) c_int;
pub extern fn strcoll(__s1: [*c]const u8, __s2: [*c]const u8) c_int;
pub extern fn strxfrm(__dest: [*c]u8, __src: [*c]const u8, __n: c_ulong) c_ulong;
pub const struct___locale_data = opaque {};
pub const struct___locale_struct = extern struct {
    __locales: [13]?*struct___locale_data,
    __ctype_b: [*c]const c_ushort,
    __ctype_tolower: [*c]const c_int,
    __ctype_toupper: [*c]const c_int,
    __names: [13][*c]const u8,
};
pub const __locale_t = [*c]struct___locale_struct;
pub const locale_t = __locale_t;
pub extern fn strcoll_l(__s1: [*c]const u8, __s2: [*c]const u8, __l: locale_t) c_int;
pub extern fn strxfrm_l(__dest: [*c]u8, __src: [*c]const u8, __n: usize, __l: locale_t) usize;
pub extern fn strdup(__s: [*c]const u8) [*c]u8;
pub extern fn strndup(__string: [*c]const u8, __n: c_ulong) [*c]u8;
pub extern fn strchr(__s: [*c]const u8, __c: c_int) [*c]u8;
pub extern fn strrchr(__s: [*c]const u8, __c: c_int) [*c]u8;
pub extern fn strcspn(__s: [*c]const u8, __reject: [*c]const u8) c_ulong;
pub extern fn strspn(__s: [*c]const u8, __accept: [*c]const u8) c_ulong;
pub extern fn strpbrk(__s: [*c]const u8, __accept: [*c]const u8) [*c]u8;
pub extern fn strstr(__haystack: [*c]const u8, __needle: [*c]const u8) [*c]u8;
pub extern fn strtok(__s: [*c]u8, __delim: [*c]const u8) [*c]u8;
pub extern fn __strtok_r(noalias __s: [*c]u8, noalias __delim: [*c]const u8, noalias __save_ptr: [*c][*c]u8) [*c]u8;
pub extern fn strtok_r(noalias __s: [*c]u8, noalias __delim: [*c]const u8, noalias __save_ptr: [*c][*c]u8) [*c]u8;
pub extern fn strlen(__s: [*c]const u8) c_ulong;
pub extern fn strnlen(__string: [*c]const u8, __maxlen: usize) usize;
pub extern fn strerror(__errnum: c_int) [*c]u8;
pub extern fn strerror_r(__errnum: c_int, __buf: [*c]u8, __buflen: usize) c_int;
pub extern fn strerror_l(__errnum: c_int, __l: locale_t) [*c]u8;
pub extern fn bcmp(__s1: ?*const anyopaque, __s2: ?*const anyopaque, __n: c_ulong) c_int;
pub extern fn bcopy(__src: ?*const anyopaque, __dest: ?*anyopaque, __n: usize) void;
pub extern fn bzero(__s: ?*anyopaque, __n: c_ulong) void;
pub extern fn index(__s: [*c]const u8, __c: c_int) [*c]u8;
pub extern fn rindex(__s: [*c]const u8, __c: c_int) [*c]u8;
pub extern fn ffs(__i: c_int) c_int;
pub extern fn ffsl(__l: c_long) c_int;
pub extern fn ffsll(__ll: c_longlong) c_int;
pub extern fn strcasecmp(__s1: [*c]const u8, __s2: [*c]const u8) c_int;
pub extern fn strncasecmp(__s1: [*c]const u8, __s2: [*c]const u8, __n: c_ulong) c_int;
pub extern fn strcasecmp_l(__s1: [*c]const u8, __s2: [*c]const u8, __loc: locale_t) c_int;
pub extern fn strncasecmp_l(__s1: [*c]const u8, __s2: [*c]const u8, __n: usize, __loc: locale_t) c_int;
pub extern fn explicit_bzero(__s: ?*anyopaque, __n: usize) void;
pub extern fn strsep(noalias __stringp: [*c][*c]u8, noalias __delim: [*c]const u8) [*c]u8;
pub extern fn strsignal(__sig: c_int) [*c]u8;
pub extern fn __stpcpy(noalias __dest: [*c]u8, noalias __src: [*c]const u8) [*c]u8;
pub extern fn stpcpy(__dest: [*c]u8, __src: [*c]const u8) [*c]u8;
pub extern fn __stpncpy(noalias __dest: [*c]u8, noalias __src: [*c]const u8, __n: usize) [*c]u8;
pub extern fn stpncpy(__dest: [*c]u8, __src: [*c]const u8, __n: c_ulong) [*c]u8;
pub const struct___va_list_tag = extern struct {
    gp_offset: c_uint,
    fp_offset: c_uint,
    overflow_arg_area: ?*anyopaque,
    reg_save_area: ?*anyopaque,
};
pub const __builtin_va_list = [1]struct___va_list_tag;
pub const va_list = __builtin_va_list;
pub const __gnuc_va_list = __builtin_va_list;
pub const __jmp_buf = [8]c_long;
pub const struct___jmp_buf_tag = extern struct {
    __jmpbuf: __jmp_buf,
    __mask_was_saved: c_int,
    __saved_mask: __sigset_t,
};
pub const jmp_buf = [1]struct___jmp_buf_tag;
pub extern fn setjmp(__env: [*c]struct___jmp_buf_tag) c_int;
pub extern fn __sigsetjmp(__env: [*c]struct___jmp_buf_tag, __savemask: c_int) c_int;
pub extern fn _setjmp(__env: [*c]struct___jmp_buf_tag) c_int;
pub extern fn longjmp(__env: [*c]struct___jmp_buf_tag, __val: c_int) noreturn;
pub extern fn _longjmp(__env: [*c]struct___jmp_buf_tag, __val: c_int) noreturn;
pub const sigjmp_buf = [1]struct___jmp_buf_tag;
pub extern fn siglongjmp(__env: [*c]struct___jmp_buf_tag, __val: c_int) noreturn;
pub const ptrdiff_t = c_long;
pub const max_align_t = extern struct {
    __clang_max_align_nonce1: c_longlong align(8),
    __clang_max_align_nonce2: c_longdouble align(16),
};
const union_unnamed_2 = extern union {
    __wch: c_uint,
    __wchb: [4]u8,
};
pub const __mbstate_t = extern struct {
    __count: c_int,
    __value: union_unnamed_2,
};
pub const struct__G_fpos_t = extern struct {
    __pos: __off_t,
    __state: __mbstate_t,
};
pub const __fpos_t = struct__G_fpos_t;
pub const struct__G_fpos64_t = extern struct {
    __pos: __off64_t,
    __state: __mbstate_t,
};
pub const __fpos64_t = struct__G_fpos64_t;
pub const struct__IO_marker = opaque {};
pub const _IO_lock_t = anyopaque;
pub const struct__IO_codecvt = opaque {};
pub const struct__IO_wide_data = opaque {};
pub const struct__IO_FILE = extern struct {
    _flags: c_int,
    _IO_read_ptr: [*c]u8,
    _IO_read_end: [*c]u8,
    _IO_read_base: [*c]u8,
    _IO_write_base: [*c]u8,
    _IO_write_ptr: [*c]u8,
    _IO_write_end: [*c]u8,
    _IO_buf_base: [*c]u8,
    _IO_buf_end: [*c]u8,
    _IO_save_base: [*c]u8,
    _IO_backup_base: [*c]u8,
    _IO_save_end: [*c]u8,
    _markers: ?*struct__IO_marker,
    _chain: [*c]struct__IO_FILE,
    _fileno: c_int,
    _flags2: c_int,
    _old_offset: __off_t,
    _cur_column: c_ushort,
    _vtable_offset: i8,
    _shortbuf: [1]u8,
    _lock: ?*_IO_lock_t,
    _offset: __off64_t,
    _codecvt: ?*struct__IO_codecvt,
    _wide_data: ?*struct__IO_wide_data,
    _freeres_list: [*c]struct__IO_FILE,
    _freeres_buf: ?*anyopaque,
    __pad5: usize,
    _mode: c_int,
    _unused2: [20]u8,
};
pub const __FILE = struct__IO_FILE;
pub const FILE = struct__IO_FILE;
pub const fpos_t = __fpos_t;
pub extern var stdin: [*c]FILE;
pub extern var stdout: [*c]FILE;
pub extern var stderr: [*c]FILE;
pub extern fn remove(__filename: [*c]const u8) c_int;
pub extern fn rename(__old: [*c]const u8, __new: [*c]const u8) c_int;
pub extern fn renameat(__oldfd: c_int, __old: [*c]const u8, __newfd: c_int, __new: [*c]const u8) c_int;
pub extern fn fclose(__stream: [*c]FILE) c_int;
pub extern fn tmpfile() [*c]FILE;
pub extern fn tmpnam([*c]u8) [*c]u8;
pub extern fn tmpnam_r(__s: [*c]u8) [*c]u8;
pub extern fn tempnam(__dir: [*c]const u8, __pfx: [*c]const u8) [*c]u8;
pub extern fn fflush(__stream: [*c]FILE) c_int;
pub extern fn fflush_unlocked(__stream: [*c]FILE) c_int;
pub extern fn fopen(__filename: [*c]const u8, __modes: [*c]const u8) [*c]FILE;
pub extern fn freopen(noalias __filename: [*c]const u8, noalias __modes: [*c]const u8, noalias __stream: [*c]FILE) [*c]FILE;
pub extern fn fdopen(__fd: c_int, __modes: [*c]const u8) [*c]FILE;
pub extern fn fmemopen(__s: ?*anyopaque, __len: usize, __modes: [*c]const u8) [*c]FILE;
pub extern fn open_memstream(__bufloc: [*c][*c]u8, __sizeloc: [*c]usize) [*c]FILE;
pub extern fn setbuf(noalias __stream: [*c]FILE, noalias __buf: [*c]u8) void;
pub extern fn setvbuf(noalias __stream: [*c]FILE, noalias __buf: [*c]u8, __modes: c_int, __n: usize) c_int;
pub extern fn setbuffer(noalias __stream: [*c]FILE, noalias __buf: [*c]u8, __size: usize) void;
pub extern fn setlinebuf(__stream: [*c]FILE) void;
pub extern fn fprintf(__stream: [*c]FILE, __format: [*c]const u8, ...) c_int;
pub extern fn printf(__format: [*c]const u8, ...) c_int;
pub extern fn sprintf(__s: [*c]u8, __format: [*c]const u8, ...) c_int;
pub extern fn vfprintf(__s: [*c]FILE, __format: [*c]const u8, __arg: [*c]struct___va_list_tag) c_int;
pub extern fn vprintf(__format: [*c]const u8, __arg: [*c]struct___va_list_tag) c_int;
pub extern fn vsprintf(__s: [*c]u8, __format: [*c]const u8, __arg: [*c]struct___va_list_tag) c_int;
pub extern fn snprintf(__s: [*c]u8, __maxlen: c_ulong, __format: [*c]const u8, ...) c_int;
pub extern fn vsnprintf(__s: [*c]u8, __maxlen: c_ulong, __format: [*c]const u8, __arg: [*c]struct___va_list_tag) c_int;
pub extern fn vdprintf(__fd: c_int, noalias __fmt: [*c]const u8, __arg: [*c]struct___va_list_tag) c_int;
pub extern fn dprintf(__fd: c_int, noalias __fmt: [*c]const u8, ...) c_int;
pub extern fn fscanf(noalias __stream: [*c]FILE, noalias __format: [*c]const u8, ...) c_int;
pub extern fn scanf(noalias __format: [*c]const u8, ...) c_int;
pub extern fn sscanf(noalias __s: [*c]const u8, noalias __format: [*c]const u8, ...) c_int;
pub extern fn vfscanf(noalias __s: [*c]FILE, noalias __format: [*c]const u8, __arg: [*c]struct___va_list_tag) c_int;
pub extern fn vscanf(noalias __format: [*c]const u8, __arg: [*c]struct___va_list_tag) c_int;
pub extern fn vsscanf(noalias __s: [*c]const u8, noalias __format: [*c]const u8, __arg: [*c]struct___va_list_tag) c_int;
pub extern fn fgetc(__stream: [*c]FILE) c_int;
pub extern fn getc(__stream: [*c]FILE) c_int;
pub extern fn getchar() c_int;
pub extern fn getc_unlocked(__stream: [*c]FILE) c_int;
pub extern fn getchar_unlocked() c_int;
pub extern fn fgetc_unlocked(__stream: [*c]FILE) c_int;
pub extern fn fputc(__c: c_int, __stream: [*c]FILE) c_int;
pub extern fn putc(__c: c_int, __stream: [*c]FILE) c_int;
pub extern fn putchar(__c: c_int) c_int;
pub extern fn fputc_unlocked(__c: c_int, __stream: [*c]FILE) c_int;
pub extern fn putc_unlocked(__c: c_int, __stream: [*c]FILE) c_int;
pub extern fn putchar_unlocked(__c: c_int) c_int;
pub extern fn getw(__stream: [*c]FILE) c_int;
pub extern fn putw(__w: c_int, __stream: [*c]FILE) c_int;
pub extern fn fgets(noalias __s: [*c]u8, __n: c_int, noalias __stream: [*c]FILE) [*c]u8;
pub extern fn __getdelim(noalias __lineptr: [*c][*c]u8, noalias __n: [*c]usize, __delimiter: c_int, noalias __stream: [*c]FILE) __ssize_t;
pub extern fn getdelim(noalias __lineptr: [*c][*c]u8, noalias __n: [*c]usize, __delimiter: c_int, noalias __stream: [*c]FILE) __ssize_t;
pub extern fn getline(noalias __lineptr: [*c][*c]u8, noalias __n: [*c]usize, noalias __stream: [*c]FILE) __ssize_t;
pub extern fn fputs(noalias __s: [*c]const u8, noalias __stream: [*c]FILE) c_int;
pub extern fn puts(__s: [*c]const u8) c_int;
pub extern fn ungetc(__c: c_int, __stream: [*c]FILE) c_int;
pub extern fn fread(__ptr: ?*anyopaque, __size: c_ulong, __n: c_ulong, __stream: [*c]FILE) c_ulong;
pub extern fn fwrite(__ptr: ?*const anyopaque, __size: c_ulong, __n: c_ulong, __s: [*c]FILE) c_ulong;
pub extern fn fread_unlocked(noalias __ptr: ?*anyopaque, __size: usize, __n: usize, noalias __stream: [*c]FILE) usize;
pub extern fn fwrite_unlocked(noalias __ptr: ?*const anyopaque, __size: usize, __n: usize, noalias __stream: [*c]FILE) usize;
pub extern fn fseek(__stream: [*c]FILE, __off: c_long, __whence: c_int) c_int;
pub extern fn ftell(__stream: [*c]FILE) c_long;
pub extern fn rewind(__stream: [*c]FILE) void;
pub extern fn fseeko(__stream: [*c]FILE, __off: __off_t, __whence: c_int) c_int;
pub extern fn ftello(__stream: [*c]FILE) __off_t;
pub extern fn fgetpos(noalias __stream: [*c]FILE, noalias __pos: [*c]fpos_t) c_int;
pub extern fn fsetpos(__stream: [*c]FILE, __pos: [*c]const fpos_t) c_int;
pub extern fn clearerr(__stream: [*c]FILE) void;
pub extern fn feof(__stream: [*c]FILE) c_int;
pub extern fn ferror(__stream: [*c]FILE) c_int;
pub extern fn clearerr_unlocked(__stream: [*c]FILE) void;
pub extern fn feof_unlocked(__stream: [*c]FILE) c_int;
pub extern fn ferror_unlocked(__stream: [*c]FILE) c_int;
pub extern fn perror(__s: [*c]const u8) void;
pub extern fn fileno(__stream: [*c]FILE) c_int;
pub extern fn fileno_unlocked(__stream: [*c]FILE) c_int;
pub extern fn pclose(__stream: [*c]FILE) c_int;
pub extern fn popen(__command: [*c]const u8, __modes: [*c]const u8) [*c]FILE;
pub extern fn ctermid(__s: [*c]u8) [*c]u8;
pub extern fn flockfile(__stream: [*c]FILE) void;
pub extern fn ftrylockfile(__stream: [*c]FILE) c_int;
pub extern fn funlockfile(__stream: [*c]FILE) void;
pub extern fn __uflow([*c]FILE) c_int;
pub extern fn __overflow([*c]FILE, c_int) c_int;
pub extern const janet_type_names: [16][*c]const u8;
pub extern const janet_signal_names: [14][*c]const u8;
pub extern const janet_status_names: [16][*c]const u8;
pub const JanetHandle = c_int;
pub const JANET_SIGNAL_OK: c_int = 0;
pub const JANET_SIGNAL_ERROR: c_int = 1;
pub const JANET_SIGNAL_DEBUG: c_int = 2;
pub const JANET_SIGNAL_YIELD: c_int = 3;
pub const JANET_SIGNAL_USER0: c_int = 4;
pub const JANET_SIGNAL_USER1: c_int = 5;
pub const JANET_SIGNAL_USER2: c_int = 6;
pub const JANET_SIGNAL_USER3: c_int = 7;
pub const JANET_SIGNAL_USER4: c_int = 8;
pub const JANET_SIGNAL_USER5: c_int = 9;
pub const JANET_SIGNAL_USER6: c_int = 10;
pub const JANET_SIGNAL_USER7: c_int = 11;
pub const JANET_SIGNAL_USER8: c_int = 12;
pub const JANET_SIGNAL_USER9: c_int = 13;
pub const JanetSignal = c_uint;
pub const JANET_STATUS_DEAD: c_int = 0;
pub const JANET_STATUS_ERROR: c_int = 1;
pub const JANET_STATUS_DEBUG: c_int = 2;
pub const JANET_STATUS_PENDING: c_int = 3;
pub const JANET_STATUS_USER0: c_int = 4;
pub const JANET_STATUS_USER1: c_int = 5;
pub const JANET_STATUS_USER2: c_int = 6;
pub const JANET_STATUS_USER3: c_int = 7;
pub const JANET_STATUS_USER4: c_int = 8;
pub const JANET_STATUS_USER5: c_int = 9;
pub const JANET_STATUS_USER6: c_int = 10;
pub const JANET_STATUS_USER7: c_int = 11;
pub const JANET_STATUS_USER8: c_int = 12;
pub const JANET_STATUS_USER9: c_int = 13;
pub const JANET_STATUS_NEW: c_int = 14;
pub const JANET_STATUS_ALIVE: c_int = 15;
pub const JanetFiberStatus = c_uint;
pub const struct_JanetVM = opaque {};
pub const JanetVM = struct_JanetVM;
pub const JanetGCObject = struct_JanetGCObject;
const union_unnamed_3 = extern union {
    next: [*c]JanetGCObject,
    refcount: i32,
};
pub const struct_JanetGCObject = extern struct {
    flags: i32,
    data: union_unnamed_3,
};
pub const union_Janet = extern union {
    u64: u64,
    i64: i64,
    number: f64,
    pointer: ?*anyopaque,
};
pub const Janet = union_Janet;
pub const struct_JanetSourceMapping = extern struct {
    line: i32,
    column: i32,
};
pub const JanetSourceMapping = struct_JanetSourceMapping;
pub const JanetString = [*c]const u8;
pub const struct_JanetFuncDef = extern struct {
    gc: JanetGCObject,
    environments: [*c]i32,
    constants: [*c]Janet,
    defs: [*c][*c]JanetFuncDef,
    bytecode: [*c]u32,
    closure_bitset: [*c]u32,
    sourcemap: [*c]JanetSourceMapping,
    source: JanetString,
    name: JanetString,
    flags: i32,
    slotcount: i32,
    arity: i32,
    min_arity: i32,
    max_arity: i32,
    constants_length: i32,
    bytecode_length: i32,
    environments_length: i32,
    defs_length: i32,
};
pub const JanetFuncDef = struct_JanetFuncDef;
pub const struct_JanetKV = extern struct {
    key: Janet,
    value: Janet,
};
pub const JanetKV = struct_JanetKV;
pub const struct_JanetTable = extern struct {
    gc: JanetGCObject,
    count: i32,
    capacity: i32,
    deleted: i32,
    data: [*c]JanetKV,
    proto: [*c]JanetTable,
};
pub const JanetTable = struct_JanetTable;
pub const JanetListener = ?*const fn ([*c]JanetListenerState, JanetAsyncEvent) callconv(.C) JanetAsyncStatus;
pub const struct_JanetStream = extern struct {
    handle: JanetHandle,
    flags: u32,
    state: [*c]JanetListenerState,
    methods: ?*const anyopaque,
    _mask: c_int,
};
pub const JanetStream = struct_JanetStream;
pub const struct_JanetListenerState = extern struct {
    machine: JanetListener,
    fiber: [*c]JanetFiber,
    stream: [*c]JanetStream,
    event: ?*anyopaque,
    _index: usize,
    _mask: c_int,
    _next: [*c]JanetListenerState,
};
pub const JanetListenerState = struct_JanetListenerState;
pub const struct_JanetFiber = extern struct {
    gc: JanetGCObject,
    flags: i32,
    frame: i32,
    stackstart: i32,
    stacktop: i32,
    capacity: i32,
    maxstack: i32,
    env: [*c]JanetTable,
    data: [*c]Janet,
    child: [*c]JanetFiber,
    last_value: Janet,
    waiting: [*c]JanetListenerState,
    sched_id: u32,
    supervisor_channel: ?*anyopaque,
};
pub const JanetFiber = struct_JanetFiber;
const union_unnamed_4 = extern union {
    fiber: [*c]JanetFiber,
    values: [*c]Janet,
};
pub const struct_JanetFuncEnv = extern struct {
    gc: JanetGCObject,
    as: union_unnamed_4,
    length: i32,
    offset: i32,
};
pub const JanetFuncEnv = struct_JanetFuncEnv;
pub const struct_JanetFunction = extern struct {
    gc: JanetGCObject align(8),
    def: [*c]JanetFuncDef,
    pub fn envs(self: anytype) @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), [*c]JanetFuncEnv) {
        const Intermediate = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), u8);
        const ReturnType = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), [*c]JanetFuncEnv);
        return @ptrCast(ReturnType, @alignCast(@alignOf([*c]JanetFuncEnv), @ptrCast(Intermediate, self) + 24));
    }
};
pub const JanetFunction = struct_JanetFunction;
pub const struct_JanetArray = extern struct {
    gc: JanetGCObject,
    count: i32,
    capacity: i32,
    data: [*c]Janet,
};
pub const JanetArray = struct_JanetArray;
pub const struct_JanetBuffer = extern struct {
    gc: JanetGCObject,
    count: i32,
    capacity: i32,
    data: [*c]u8,
};
pub const JanetBuffer = struct_JanetBuffer;
pub const struct_JanetTupleHead = extern struct {
    gc: JanetGCObject align(8),
    length: i32,
    hash: i32,
    sm_line: i32,
    sm_column: i32,
    pub fn data(self: anytype) @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), Janet) {
        const Intermediate = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), u8);
        const ReturnType = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), Janet);
        return @ptrCast(ReturnType, @alignCast(@alignOf(Janet), @ptrCast(Intermediate, self) + 32));
    }
};
pub const JanetTupleHead = struct_JanetTupleHead;
pub const struct_JanetStructHead = extern struct {
    gc: JanetGCObject align(8),
    length: i32,
    hash: i32,
    capacity: i32,
    proto: [*c]const JanetKV,
    pub fn data(self: anytype) @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), JanetKV) {
        const Intermediate = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), u8);
        const ReturnType = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), JanetKV);
        return @ptrCast(ReturnType, @alignCast(@alignOf(JanetKV), @ptrCast(Intermediate, self) + 40));
    }
};
pub const JanetStructHead = struct_JanetStructHead;
pub const struct_JanetStringHead = extern struct {
    gc: JanetGCObject align(8),
    length: i32,
    hash: i32,
    pub fn data(self: anytype) @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), u8) {
        const Intermediate = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), u8);
        const ReturnType = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), u8);
        return @ptrCast(ReturnType, @alignCast(@alignOf(u8), @ptrCast(Intermediate, self) + 24));
    }
};
pub const JanetStringHead = struct_JanetStringHead;
pub const struct_JanetAbstractType = extern struct {
    name: [*c]const u8,
    gc: ?*const fn (?*anyopaque, usize) callconv(.C) c_int,
    gcmark: ?*const fn (?*anyopaque, usize) callconv(.C) c_int,
    get: ?*const fn (?*anyopaque, Janet, [*c]Janet) callconv(.C) c_int,
    put: ?*const fn (?*anyopaque, Janet, Janet) callconv(.C) void,
    marshal: ?*const fn (?*anyopaque, [*c]JanetMarshalContext) callconv(.C) void,
    unmarshal: ?*const fn ([*c]JanetMarshalContext) callconv(.C) ?*anyopaque,
    tostring: ?*const fn (?*anyopaque, [*c]JanetBuffer) callconv(.C) void,
    compare: ?*const fn (?*anyopaque, ?*anyopaque) callconv(.C) c_int,
    hash: ?*const fn (?*anyopaque, usize) callconv(.C) i32,
    next: ?*const fn (?*anyopaque, Janet) callconv(.C) Janet,
    call: ?*const fn (?*anyopaque, i32, [*c]Janet) callconv(.C) Janet,
};
pub const JanetAbstractType = struct_JanetAbstractType;
pub const struct_JanetAbstractHead = extern struct {
    gc: JanetGCObject align(8),
    type: [*c]const JanetAbstractType,
    size: usize,
    pub fn data(self: anytype) @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), c_longlong) {
        const Intermediate = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), u8);
        const ReturnType = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), c_longlong);
        return @ptrCast(ReturnType, @alignCast(@alignOf(c_longlong), @ptrCast(Intermediate, self) + 32));
    }
};
pub const JanetAbstractHead = struct_JanetAbstractHead;
pub const struct_JanetStackFrame = extern struct {
    func: [*c]JanetFunction,
    pc: [*c]u32,
    env: [*c]JanetFuncEnv,
    prevframe: i32,
    flags: i32,
};
pub const JanetStackFrame = struct_JanetStackFrame;
pub const JanetCFunction = ?*const fn (i32, [*c]Janet) callconv(.C) Janet;
pub const struct_JanetReg = extern struct {
    name: [*c]const u8,
    cfun: JanetCFunction,
    documentation: [*c]const u8,
};
pub const JanetReg = struct_JanetReg;
pub const struct_JanetRegExt = extern struct {
    name: [*c]const u8,
    cfun: JanetCFunction,
    documentation: [*c]const u8,
    source_file: [*c]const u8,
    source_line: i32,
};
pub const JanetRegExt = struct_JanetRegExt;
pub const struct_JanetMethod = extern struct {
    name: [*c]const u8,
    cfun: JanetCFunction,
};
pub const JanetMethod = struct_JanetMethod;
pub const struct_JanetView = extern struct {
    items: [*c]const Janet,
    len: i32,
};
pub const JanetView = struct_JanetView;
pub const struct_JanetByteView = extern struct {
    bytes: [*c]const u8,
    len: i32,
};
pub const JanetByteView = struct_JanetByteView;
pub const struct_JanetDictView = extern struct {
    kvs: [*c]const JanetKV,
    len: i32,
    cap: i32,
};
pub const JanetDictView = struct_JanetDictView;
pub const struct_JanetRange = extern struct {
    start: i32,
    end: i32,
};
pub const JanetRange = struct_JanetRange;
pub const struct_JanetRNG = extern struct {
    a: u32,
    b: u32,
    c: u32,
    d: u32,
    counter: u32,
};
pub const JanetRNG = struct_JanetRNG;
pub const JANET_NUMBER: c_int = 0;
pub const JANET_NIL: c_int = 1;
pub const JANET_BOOLEAN: c_int = 2;
pub const JANET_FIBER: c_int = 3;
pub const JANET_STRING: c_int = 4;
pub const JANET_SYMBOL: c_int = 5;
pub const JANET_KEYWORD: c_int = 6;
pub const JANET_ARRAY: c_int = 7;
pub const JANET_TUPLE: c_int = 8;
pub const JANET_TABLE: c_int = 9;
pub const JANET_STRUCT: c_int = 10;
pub const JANET_BUFFER: c_int = 11;
pub const JANET_FUNCTION: c_int = 12;
pub const JANET_CFUNCTION: c_int = 13;
pub const JANET_ABSTRACT: c_int = 14;
pub const JANET_POINTER: c_int = 15;
pub const enum_JanetType = c_uint;
pub const JanetType = enum_JanetType;
pub const JanetSymbol = [*c]const u8;
pub const JanetKeyword = [*c]const u8;
pub const JanetTuple = [*c]const Janet;
pub const JanetStruct = [*c]const JanetKV;
pub const JanetAbstract = ?*anyopaque;
pub const JANET_ASYNC_EVENT_INIT: c_int = 0;
pub const JANET_ASYNC_EVENT_MARK: c_int = 1;
pub const JANET_ASYNC_EVENT_DEINIT: c_int = 2;
pub const JANET_ASYNC_EVENT_CLOSE: c_int = 3;
pub const JANET_ASYNC_EVENT_ERR: c_int = 4;
pub const JANET_ASYNC_EVENT_HUP: c_int = 5;
pub const JANET_ASYNC_EVENT_READ: c_int = 6;
pub const JANET_ASYNC_EVENT_WRITE: c_int = 7;
pub const JANET_ASYNC_EVENT_CANCEL: c_int = 8;
pub const JANET_ASYNC_EVENT_COMPLETE: c_int = 9;
pub const JANET_ASYNC_EVENT_USER: c_int = 10;
pub const JanetAsyncEvent = c_uint;
pub const JANET_ASYNC_STATUS_NOT_DONE: c_int = 0;
pub const JANET_ASYNC_STATUS_DONE: c_int = 1;
pub const JanetAsyncStatus = c_uint;
pub extern fn janet_struct_head(st: [*c]const JanetKV) [*c]JanetStructHead;
pub extern fn janet_abstract_head(abstract: ?*const anyopaque) [*c]JanetAbstractHead;
pub extern fn janet_string_head(s: [*c]const u8) [*c]JanetStringHead;
pub extern fn janet_tuple_head(tuple: [*c]const Janet) [*c]JanetTupleHead;
pub extern fn janet_type(x: Janet) JanetType;
pub extern fn janet_checktype(x: Janet, @"type": JanetType) c_int;
pub extern fn janet_checktypes(x: Janet, typeflags: c_int) c_int;
pub extern fn janet_truthy(x: Janet) c_int;
pub extern fn janet_unwrap_struct(x: Janet) [*c]const JanetKV;
pub extern fn janet_unwrap_tuple(x: Janet) [*c]const Janet;
pub extern fn janet_unwrap_fiber(x: Janet) [*c]JanetFiber;
pub extern fn janet_unwrap_array(x: Janet) [*c]JanetArray;
pub extern fn janet_unwrap_table(x: Janet) [*c]JanetTable;
pub extern fn janet_unwrap_buffer(x: Janet) [*c]JanetBuffer;
pub extern fn janet_unwrap_string(x: Janet) [*c]const u8;
pub extern fn janet_unwrap_symbol(x: Janet) [*c]const u8;
pub extern fn janet_unwrap_keyword(x: Janet) [*c]const u8;
pub extern fn janet_unwrap_abstract(x: Janet) ?*anyopaque;
pub extern fn janet_unwrap_pointer(x: Janet) ?*anyopaque;
pub extern fn janet_unwrap_function(x: Janet) [*c]JanetFunction;
pub extern fn janet_unwrap_cfunction(x: Janet) JanetCFunction;
pub extern fn janet_unwrap_boolean(x: Janet) c_int;
pub extern fn janet_unwrap_number(x: Janet) f64;
pub extern fn janet_unwrap_integer(x: Janet) i32;
pub extern fn janet_wrap_nil() Janet;
pub extern fn janet_wrap_number(x: f64) Janet;
pub extern fn janet_wrap_true() Janet;
pub extern fn janet_wrap_false() Janet;
pub extern fn janet_wrap_boolean(x: c_int) Janet;
pub extern fn janet_wrap_string(x: [*c]const u8) Janet;
pub extern fn janet_wrap_symbol(x: [*c]const u8) Janet;
pub extern fn janet_wrap_keyword(x: [*c]const u8) Janet;
pub extern fn janet_wrap_array(x: [*c]JanetArray) Janet;
pub extern fn janet_wrap_tuple(x: [*c]const Janet) Janet;
pub extern fn janet_wrap_struct(x: [*c]const JanetKV) Janet;
pub extern fn janet_wrap_fiber(x: [*c]JanetFiber) Janet;
pub extern fn janet_wrap_buffer(x: [*c]JanetBuffer) Janet;
pub extern fn janet_wrap_function(x: [*c]JanetFunction) Janet;
pub extern fn janet_wrap_cfunction(x: JanetCFunction) Janet;
pub extern fn janet_wrap_table(x: [*c]JanetTable) Janet;
pub extern fn janet_wrap_abstract(x: ?*anyopaque) Janet;
pub extern fn janet_wrap_pointer(x: ?*anyopaque) Janet;
pub extern fn janet_wrap_integer(x: i32) Janet;
pub const float_t = f32;
pub const double_t = f64;
pub extern fn __fpclassify(__value: f64) c_int;
pub extern fn __signbit(__value: f64) c_int;
pub extern fn __isinf(__value: f64) c_int;
pub extern fn __finite(__value: f64) c_int;
pub extern fn __isnan(__value: f64) c_int;
pub extern fn __iseqsig(__x: f64, __y: f64) c_int;
pub extern fn __issignaling(__value: f64) c_int;
pub extern fn acos(__x: f64) f64;
pub extern fn __acos(__x: f64) f64;
pub extern fn asin(__x: f64) f64;
pub extern fn __asin(__x: f64) f64;
pub extern fn atan(__x: f64) f64;
pub extern fn __atan(__x: f64) f64;
pub extern fn atan2(__y: f64, __x: f64) f64;
pub extern fn __atan2(__y: f64, __x: f64) f64;
pub extern fn cos(__x: f64) f64;
pub extern fn __cos(__x: f64) f64;
pub extern fn sin(__x: f64) f64;
pub extern fn __sin(__x: f64) f64;
pub extern fn tan(__x: f64) f64;
pub extern fn __tan(__x: f64) f64;
pub extern fn cosh(__x: f64) f64;
pub extern fn __cosh(__x: f64) f64;
pub extern fn sinh(__x: f64) f64;
pub extern fn __sinh(__x: f64) f64;
pub extern fn tanh(__x: f64) f64;
pub extern fn __tanh(__x: f64) f64;
pub extern fn acosh(__x: f64) f64;
pub extern fn __acosh(__x: f64) f64;
pub extern fn asinh(__x: f64) f64;
pub extern fn __asinh(__x: f64) f64;
pub extern fn atanh(__x: f64) f64;
pub extern fn __atanh(__x: f64) f64;
pub extern fn exp(__x: f64) f64;
pub extern fn __exp(__x: f64) f64;
pub extern fn frexp(__x: f64, __exponent: [*c]c_int) f64;
pub extern fn __frexp(__x: f64, __exponent: [*c]c_int) f64;
pub extern fn ldexp(__x: f64, __exponent: c_int) f64;
pub extern fn __ldexp(__x: f64, __exponent: c_int) f64;
pub extern fn log(__x: f64) f64;
pub extern fn __log(__x: f64) f64;
pub extern fn log10(__x: f64) f64;
pub extern fn __log10(__x: f64) f64;
pub extern fn modf(__x: f64, __iptr: [*c]f64) f64;
pub extern fn __modf(__x: f64, __iptr: [*c]f64) f64;
pub extern fn expm1(__x: f64) f64;
pub extern fn __expm1(__x: f64) f64;
pub extern fn log1p(__x: f64) f64;
pub extern fn __log1p(__x: f64) f64;
pub extern fn logb(__x: f64) f64;
pub extern fn __logb(__x: f64) f64;
pub extern fn exp2(__x: f64) f64;
pub extern fn __exp2(__x: f64) f64;
pub extern fn log2(__x: f64) f64;
pub extern fn __log2(__x: f64) f64;
pub extern fn pow(__x: f64, __y: f64) f64;
pub extern fn __pow(__x: f64, __y: f64) f64;
pub extern fn sqrt(__x: f64) f64;
pub extern fn __sqrt(__x: f64) f64;
pub extern fn hypot(__x: f64, __y: f64) f64;
pub extern fn __hypot(__x: f64, __y: f64) f64;
pub extern fn cbrt(__x: f64) f64;
pub extern fn __cbrt(__x: f64) f64;
pub extern fn ceil(__x: f64) f64;
pub extern fn __ceil(__x: f64) f64;
pub extern fn fabs(__x: f64) f64;
pub extern fn __fabs(__x: f64) f64;
pub extern fn floor(__x: f64) f64;
pub extern fn __floor(__x: f64) f64;
pub extern fn fmod(__x: f64, __y: f64) f64;
pub extern fn __fmod(__x: f64, __y: f64) f64;
pub extern fn isinf(__value: f64) c_int;
pub extern fn finite(__value: f64) c_int;
pub extern fn drem(__x: f64, __y: f64) f64;
pub extern fn __drem(__x: f64, __y: f64) f64;
pub extern fn significand(__x: f64) f64;
pub extern fn __significand(__x: f64) f64;
pub extern fn copysign(__x: f64, __y: f64) f64;
pub extern fn __copysign(__x: f64, __y: f64) f64;
pub extern fn nan(__tagb: [*c]const u8) f64;
pub extern fn __nan(__tagb: [*c]const u8) f64;
pub extern fn isnan(__value: f64) c_int;
pub extern fn j0(f64) f64;
pub extern fn __j0(f64) f64;
pub extern fn j1(f64) f64;
pub extern fn __j1(f64) f64;
pub extern fn jn(c_int, f64) f64;
pub extern fn __jn(c_int, f64) f64;
pub extern fn y0(f64) f64;
pub extern fn __y0(f64) f64;
pub extern fn y1(f64) f64;
pub extern fn __y1(f64) f64;
pub extern fn yn(c_int, f64) f64;
pub extern fn __yn(c_int, f64) f64;
pub extern fn erf(f64) f64;
pub extern fn __erf(f64) f64;
pub extern fn erfc(f64) f64;
pub extern fn __erfc(f64) f64;
pub extern fn lgamma(f64) f64;
pub extern fn __lgamma(f64) f64;
pub extern fn tgamma(f64) f64;
pub extern fn __tgamma(f64) f64;
pub extern fn gamma(f64) f64;
pub extern fn __gamma(f64) f64;
pub extern fn lgamma_r(f64, __signgamp: [*c]c_int) f64;
pub extern fn __lgamma_r(f64, __signgamp: [*c]c_int) f64;
pub extern fn rint(__x: f64) f64;
pub extern fn __rint(__x: f64) f64;
pub extern fn nextafter(__x: f64, __y: f64) f64;
pub extern fn __nextafter(__x: f64, __y: f64) f64;
pub extern fn nexttoward(__x: f64, __y: c_longdouble) f64;
pub extern fn __nexttoward(__x: f64, __y: c_longdouble) f64;
pub extern fn remainder(__x: f64, __y: f64) f64;
pub extern fn __remainder(__x: f64, __y: f64) f64;
pub extern fn scalbn(__x: f64, __n: c_int) f64;
pub extern fn __scalbn(__x: f64, __n: c_int) f64;
pub extern fn ilogb(__x: f64) c_int;
pub extern fn __ilogb(__x: f64) c_int;
pub extern fn scalbln(__x: f64, __n: c_long) f64;
pub extern fn __scalbln(__x: f64, __n: c_long) f64;
pub extern fn nearbyint(__x: f64) f64;
pub extern fn __nearbyint(__x: f64) f64;
pub extern fn round(__x: f64) f64;
pub extern fn __round(__x: f64) f64;
pub extern fn trunc(__x: f64) f64;
pub extern fn __trunc(__x: f64) f64;
pub extern fn remquo(__x: f64, __y: f64, __quo: [*c]c_int) f64;
pub extern fn __remquo(__x: f64, __y: f64, __quo: [*c]c_int) f64;
pub extern fn lrint(__x: f64) c_long;
pub extern fn __lrint(__x: f64) c_long;
pub extern fn llrint(__x: f64) c_longlong;
pub extern fn __llrint(__x: f64) c_longlong;
pub extern fn lround(__x: f64) c_long;
pub extern fn __lround(__x: f64) c_long;
pub extern fn llround(__x: f64) c_longlong;
pub extern fn __llround(__x: f64) c_longlong;
pub extern fn fdim(__x: f64, __y: f64) f64;
pub extern fn __fdim(__x: f64, __y: f64) f64;
pub extern fn fmax(__x: f64, __y: f64) f64;
pub extern fn __fmax(__x: f64, __y: f64) f64;
pub extern fn fmin(__x: f64, __y: f64) f64;
pub extern fn __fmin(__x: f64, __y: f64) f64;
pub extern fn fma(__x: f64, __y: f64, __z: f64) f64;
pub extern fn __fma(__x: f64, __y: f64, __z: f64) f64;
pub extern fn scalb(__x: f64, __n: f64) f64;
pub extern fn __scalb(__x: f64, __n: f64) f64;
pub extern fn __fpclassifyf(__value: f32) c_int;
pub extern fn __signbitf(__value: f32) c_int;
pub extern fn __isinff(__value: f32) c_int;
pub extern fn __finitef(__value: f32) c_int;
pub extern fn __isnanf(__value: f32) c_int;
pub extern fn __iseqsigf(__x: f32, __y: f32) c_int;
pub extern fn __issignalingf(__value: f32) c_int;
pub extern fn acosf(__x: f32) f32;
pub extern fn __acosf(__x: f32) f32;
pub extern fn asinf(__x: f32) f32;
pub extern fn __asinf(__x: f32) f32;
pub extern fn atanf(__x: f32) f32;
pub extern fn __atanf(__x: f32) f32;
pub extern fn atan2f(__y: f32, __x: f32) f32;
pub extern fn __atan2f(__y: f32, __x: f32) f32;
pub extern fn cosf(__x: f32) f32;
pub extern fn __cosf(__x: f32) f32;
pub extern fn sinf(__x: f32) f32;
pub extern fn __sinf(__x: f32) f32;
pub extern fn tanf(__x: f32) f32;
pub extern fn __tanf(__x: f32) f32;
pub extern fn coshf(__x: f32) f32;
pub extern fn __coshf(__x: f32) f32;
pub extern fn sinhf(__x: f32) f32;
pub extern fn __sinhf(__x: f32) f32;
pub extern fn tanhf(__x: f32) f32;
pub extern fn __tanhf(__x: f32) f32;
pub extern fn acoshf(__x: f32) f32;
pub extern fn __acoshf(__x: f32) f32;
pub extern fn asinhf(__x: f32) f32;
pub extern fn __asinhf(__x: f32) f32;
pub extern fn atanhf(__x: f32) f32;
pub extern fn __atanhf(__x: f32) f32;
pub extern fn expf(__x: f32) f32;
pub extern fn __expf(__x: f32) f32;
pub extern fn frexpf(__x: f32, __exponent: [*c]c_int) f32;
pub extern fn __frexpf(__x: f32, __exponent: [*c]c_int) f32;
pub extern fn ldexpf(__x: f32, __exponent: c_int) f32;
pub extern fn __ldexpf(__x: f32, __exponent: c_int) f32;
pub extern fn logf(__x: f32) f32;
pub extern fn __logf(__x: f32) f32;
pub extern fn log10f(__x: f32) f32;
pub extern fn __log10f(__x: f32) f32;
pub extern fn modff(__x: f32, __iptr: [*c]f32) f32;
pub extern fn __modff(__x: f32, __iptr: [*c]f32) f32;
pub extern fn expm1f(__x: f32) f32;
pub extern fn __expm1f(__x: f32) f32;
pub extern fn log1pf(__x: f32) f32;
pub extern fn __log1pf(__x: f32) f32;
pub extern fn logbf(__x: f32) f32;
pub extern fn __logbf(__x: f32) f32;
pub extern fn exp2f(__x: f32) f32;
pub extern fn __exp2f(__x: f32) f32;
pub extern fn log2f(__x: f32) f32;
pub extern fn __log2f(__x: f32) f32;
pub extern fn powf(__x: f32, __y: f32) f32;
pub extern fn __powf(__x: f32, __y: f32) f32;
pub extern fn sqrtf(__x: f32) f32;
pub extern fn __sqrtf(__x: f32) f32;
pub extern fn hypotf(__x: f32, __y: f32) f32;
pub extern fn __hypotf(__x: f32, __y: f32) f32;
pub extern fn cbrtf(__x: f32) f32;
pub extern fn __cbrtf(__x: f32) f32;
pub extern fn ceilf(__x: f32) f32;
pub extern fn __ceilf(__x: f32) f32;
pub extern fn fabsf(__x: f32) f32;
pub extern fn __fabsf(__x: f32) f32;
pub extern fn floorf(__x: f32) f32;
pub extern fn __floorf(__x: f32) f32;
pub extern fn fmodf(__x: f32, __y: f32) f32;
pub extern fn __fmodf(__x: f32, __y: f32) f32;
pub extern fn isinff(__value: f32) c_int;
pub extern fn finitef(__value: f32) c_int;
pub extern fn dremf(__x: f32, __y: f32) f32;
pub extern fn __dremf(__x: f32, __y: f32) f32;
pub extern fn significandf(__x: f32) f32;
pub extern fn __significandf(__x: f32) f32;
pub extern fn copysignf(__x: f32, __y: f32) f32;
pub extern fn __copysignf(__x: f32, __y: f32) f32;
pub extern fn nanf(__tagb: [*c]const u8) f32;
pub extern fn __nanf(__tagb: [*c]const u8) f32;
pub extern fn isnanf(__value: f32) c_int;
pub extern fn j0f(f32) f32;
pub extern fn __j0f(f32) f32;
pub extern fn j1f(f32) f32;
pub extern fn __j1f(f32) f32;
pub extern fn jnf(c_int, f32) f32;
pub extern fn __jnf(c_int, f32) f32;
pub extern fn y0f(f32) f32;
pub extern fn __y0f(f32) f32;
pub extern fn y1f(f32) f32;
pub extern fn __y1f(f32) f32;
pub extern fn ynf(c_int, f32) f32;
pub extern fn __ynf(c_int, f32) f32;
pub extern fn erff(f32) f32;
pub extern fn __erff(f32) f32;
pub extern fn erfcf(f32) f32;
pub extern fn __erfcf(f32) f32;
pub extern fn lgammaf(f32) f32;
pub extern fn __lgammaf(f32) f32;
pub extern fn tgammaf(f32) f32;
pub extern fn __tgammaf(f32) f32;
pub extern fn gammaf(f32) f32;
pub extern fn __gammaf(f32) f32;
pub extern fn lgammaf_r(f32, __signgamp: [*c]c_int) f32;
pub extern fn __lgammaf_r(f32, __signgamp: [*c]c_int) f32;
pub extern fn rintf(__x: f32) f32;
pub extern fn __rintf(__x: f32) f32;
pub extern fn nextafterf(__x: f32, __y: f32) f32;
pub extern fn __nextafterf(__x: f32, __y: f32) f32;
pub extern fn nexttowardf(__x: f32, __y: c_longdouble) f32;
pub extern fn __nexttowardf(__x: f32, __y: c_longdouble) f32;
pub extern fn remainderf(__x: f32, __y: f32) f32;
pub extern fn __remainderf(__x: f32, __y: f32) f32;
pub extern fn scalbnf(__x: f32, __n: c_int) f32;
pub extern fn __scalbnf(__x: f32, __n: c_int) f32;
pub extern fn ilogbf(__x: f32) c_int;
pub extern fn __ilogbf(__x: f32) c_int;
pub extern fn scalblnf(__x: f32, __n: c_long) f32;
pub extern fn __scalblnf(__x: f32, __n: c_long) f32;
pub extern fn nearbyintf(__x: f32) f32;
pub extern fn __nearbyintf(__x: f32) f32;
pub extern fn roundf(__x: f32) f32;
pub extern fn __roundf(__x: f32) f32;
pub extern fn truncf(__x: f32) f32;
pub extern fn __truncf(__x: f32) f32;
pub extern fn remquof(__x: f32, __y: f32, __quo: [*c]c_int) f32;
pub extern fn __remquof(__x: f32, __y: f32, __quo: [*c]c_int) f32;
pub extern fn lrintf(__x: f32) c_long;
pub extern fn __lrintf(__x: f32) c_long;
pub extern fn llrintf(__x: f32) c_longlong;
pub extern fn __llrintf(__x: f32) c_longlong;
pub extern fn lroundf(__x: f32) c_long;
pub extern fn __lroundf(__x: f32) c_long;
pub extern fn llroundf(__x: f32) c_longlong;
pub extern fn __llroundf(__x: f32) c_longlong;
pub extern fn fdimf(__x: f32, __y: f32) f32;
pub extern fn __fdimf(__x: f32, __y: f32) f32;
pub extern fn fmaxf(__x: f32, __y: f32) f32;
pub extern fn __fmaxf(__x: f32, __y: f32) f32;
pub extern fn fminf(__x: f32, __y: f32) f32;
pub extern fn __fminf(__x: f32, __y: f32) f32;
pub extern fn fmaf(__x: f32, __y: f32, __z: f32) f32;
pub extern fn __fmaf(__x: f32, __y: f32, __z: f32) f32;
pub extern fn scalbf(__x: f32, __n: f32) f32;
pub extern fn __scalbf(__x: f32, __n: f32) f32;
pub extern fn __fpclassifyl(__value: c_longdouble) c_int;
pub extern fn __signbitl(__value: c_longdouble) c_int;
pub extern fn __isinfl(__value: c_longdouble) c_int;
pub extern fn __finitel(__value: c_longdouble) c_int;
pub extern fn __isnanl(__value: c_longdouble) c_int;
pub extern fn __iseqsigl(__x: c_longdouble, __y: c_longdouble) c_int;
pub extern fn __issignalingl(__value: c_longdouble) c_int;
pub extern fn acosl(__x: c_longdouble) c_longdouble;
pub extern fn __acosl(__x: c_longdouble) c_longdouble;
pub extern fn asinl(__x: c_longdouble) c_longdouble;
pub extern fn __asinl(__x: c_longdouble) c_longdouble;
pub extern fn atanl(__x: c_longdouble) c_longdouble;
pub extern fn __atanl(__x: c_longdouble) c_longdouble;
pub extern fn atan2l(__y: c_longdouble, __x: c_longdouble) c_longdouble;
pub extern fn __atan2l(__y: c_longdouble, __x: c_longdouble) c_longdouble;
pub extern fn cosl(__x: c_longdouble) c_longdouble;
pub extern fn __cosl(__x: c_longdouble) c_longdouble;
pub extern fn sinl(__x: c_longdouble) c_longdouble;
pub extern fn __sinl(__x: c_longdouble) c_longdouble;
pub extern fn tanl(__x: c_longdouble) c_longdouble;
pub extern fn __tanl(__x: c_longdouble) c_longdouble;
pub extern fn coshl(__x: c_longdouble) c_longdouble;
pub extern fn __coshl(__x: c_longdouble) c_longdouble;
pub extern fn sinhl(__x: c_longdouble) c_longdouble;
pub extern fn __sinhl(__x: c_longdouble) c_longdouble;
pub extern fn tanhl(__x: c_longdouble) c_longdouble;
pub extern fn __tanhl(__x: c_longdouble) c_longdouble;
pub extern fn acoshl(__x: c_longdouble) c_longdouble;
pub extern fn __acoshl(__x: c_longdouble) c_longdouble;
pub extern fn asinhl(__x: c_longdouble) c_longdouble;
pub extern fn __asinhl(__x: c_longdouble) c_longdouble;
pub extern fn atanhl(__x: c_longdouble) c_longdouble;
pub extern fn __atanhl(__x: c_longdouble) c_longdouble;
pub extern fn expl(__x: c_longdouble) c_longdouble;
pub extern fn __expl(__x: c_longdouble) c_longdouble;
pub extern fn frexpl(__x: c_longdouble, __exponent: [*c]c_int) c_longdouble;
pub extern fn __frexpl(__x: c_longdouble, __exponent: [*c]c_int) c_longdouble;
pub extern fn ldexpl(__x: c_longdouble, __exponent: c_int) c_longdouble;
pub extern fn __ldexpl(__x: c_longdouble, __exponent: c_int) c_longdouble;
pub extern fn logl(__x: c_longdouble) c_longdouble;
pub extern fn __logl(__x: c_longdouble) c_longdouble;
pub extern fn log10l(__x: c_longdouble) c_longdouble;
pub extern fn __log10l(__x: c_longdouble) c_longdouble;
pub extern fn modfl(__x: c_longdouble, __iptr: [*c]c_longdouble) c_longdouble;
pub extern fn __modfl(__x: c_longdouble, __iptr: [*c]c_longdouble) c_longdouble;
pub extern fn expm1l(__x: c_longdouble) c_longdouble;
pub extern fn __expm1l(__x: c_longdouble) c_longdouble;
pub extern fn log1pl(__x: c_longdouble) c_longdouble;
pub extern fn __log1pl(__x: c_longdouble) c_longdouble;
pub extern fn logbl(__x: c_longdouble) c_longdouble;
pub extern fn __logbl(__x: c_longdouble) c_longdouble;
pub extern fn exp2l(__x: c_longdouble) c_longdouble;
pub extern fn __exp2l(__x: c_longdouble) c_longdouble;
pub extern fn log2l(__x: c_longdouble) c_longdouble;
pub extern fn __log2l(__x: c_longdouble) c_longdouble;
pub extern fn powl(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn __powl(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn sqrtl(__x: c_longdouble) c_longdouble;
pub extern fn __sqrtl(__x: c_longdouble) c_longdouble;
pub extern fn hypotl(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn __hypotl(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn cbrtl(__x: c_longdouble) c_longdouble;
pub extern fn __cbrtl(__x: c_longdouble) c_longdouble;
pub extern fn ceill(__x: c_longdouble) c_longdouble;
pub extern fn __ceill(__x: c_longdouble) c_longdouble;
pub extern fn fabsl(__x: c_longdouble) c_longdouble;
pub extern fn __fabsl(__x: c_longdouble) c_longdouble;
pub extern fn floorl(__x: c_longdouble) c_longdouble;
pub extern fn __floorl(__x: c_longdouble) c_longdouble;
pub extern fn fmodl(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn __fmodl(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn isinfl(__value: c_longdouble) c_int;
pub extern fn finitel(__value: c_longdouble) c_int;
pub extern fn dreml(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn __dreml(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn significandl(__x: c_longdouble) c_longdouble;
pub extern fn __significandl(__x: c_longdouble) c_longdouble;
pub extern fn copysignl(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn __copysignl(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn nanl(__tagb: [*c]const u8) c_longdouble;
pub extern fn __nanl(__tagb: [*c]const u8) c_longdouble;
pub extern fn isnanl(__value: c_longdouble) c_int;
pub extern fn j0l(c_longdouble) c_longdouble;
pub extern fn __j0l(c_longdouble) c_longdouble;
pub extern fn j1l(c_longdouble) c_longdouble;
pub extern fn __j1l(c_longdouble) c_longdouble;
pub extern fn jnl(c_int, c_longdouble) c_longdouble;
pub extern fn __jnl(c_int, c_longdouble) c_longdouble;
pub extern fn y0l(c_longdouble) c_longdouble;
pub extern fn __y0l(c_longdouble) c_longdouble;
pub extern fn y1l(c_longdouble) c_longdouble;
pub extern fn __y1l(c_longdouble) c_longdouble;
pub extern fn ynl(c_int, c_longdouble) c_longdouble;
pub extern fn __ynl(c_int, c_longdouble) c_longdouble;
pub extern fn erfl(c_longdouble) c_longdouble;
pub extern fn __erfl(c_longdouble) c_longdouble;
pub extern fn erfcl(c_longdouble) c_longdouble;
pub extern fn __erfcl(c_longdouble) c_longdouble;
pub extern fn lgammal(c_longdouble) c_longdouble;
pub extern fn __lgammal(c_longdouble) c_longdouble;
pub extern fn tgammal(c_longdouble) c_longdouble;
pub extern fn __tgammal(c_longdouble) c_longdouble;
pub extern fn gammal(c_longdouble) c_longdouble;
pub extern fn __gammal(c_longdouble) c_longdouble;
pub extern fn lgammal_r(c_longdouble, __signgamp: [*c]c_int) c_longdouble;
pub extern fn __lgammal_r(c_longdouble, __signgamp: [*c]c_int) c_longdouble;
pub extern fn rintl(__x: c_longdouble) c_longdouble;
pub extern fn __rintl(__x: c_longdouble) c_longdouble;
pub extern fn nextafterl(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn __nextafterl(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn nexttowardl(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn __nexttowardl(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn remainderl(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn __remainderl(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn scalbnl(__x: c_longdouble, __n: c_int) c_longdouble;
pub extern fn __scalbnl(__x: c_longdouble, __n: c_int) c_longdouble;
pub extern fn ilogbl(__x: c_longdouble) c_int;
pub extern fn __ilogbl(__x: c_longdouble) c_int;
pub extern fn scalblnl(__x: c_longdouble, __n: c_long) c_longdouble;
pub extern fn __scalblnl(__x: c_longdouble, __n: c_long) c_longdouble;
pub extern fn nearbyintl(__x: c_longdouble) c_longdouble;
pub extern fn __nearbyintl(__x: c_longdouble) c_longdouble;
pub extern fn roundl(__x: c_longdouble) c_longdouble;
pub extern fn __roundl(__x: c_longdouble) c_longdouble;
pub extern fn truncl(__x: c_longdouble) c_longdouble;
pub extern fn __truncl(__x: c_longdouble) c_longdouble;
pub extern fn remquol(__x: c_longdouble, __y: c_longdouble, __quo: [*c]c_int) c_longdouble;
pub extern fn __remquol(__x: c_longdouble, __y: c_longdouble, __quo: [*c]c_int) c_longdouble;
pub extern fn lrintl(__x: c_longdouble) c_long;
pub extern fn __lrintl(__x: c_longdouble) c_long;
pub extern fn llrintl(__x: c_longdouble) c_longlong;
pub extern fn __llrintl(__x: c_longdouble) c_longlong;
pub extern fn lroundl(__x: c_longdouble) c_long;
pub extern fn __lroundl(__x: c_longdouble) c_long;
pub extern fn llroundl(__x: c_longdouble) c_longlong;
pub extern fn __llroundl(__x: c_longdouble) c_longlong;
pub extern fn fdiml(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn __fdiml(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn fmaxl(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn __fmaxl(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn fminl(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn __fminl(__x: c_longdouble, __y: c_longdouble) c_longdouble;
pub extern fn fmal(__x: c_longdouble, __y: c_longdouble, __z: c_longdouble) c_longdouble;
pub extern fn __fmal(__x: c_longdouble, __y: c_longdouble, __z: c_longdouble) c_longdouble;
pub extern fn scalbl(__x: c_longdouble, __n: c_longdouble) c_longdouble;
pub extern fn __scalbl(__x: c_longdouble, __n: c_longdouble) c_longdouble;
pub extern var signgam: c_int;
pub const FP_NAN: c_int = 0;
pub const FP_INFINITE: c_int = 1;
pub const FP_ZERO: c_int = 2;
pub const FP_SUBNORMAL: c_int = 3;
pub const FP_NORMAL: c_int = 4;
const enum_unnamed_5 = c_uint;
pub extern fn janet_nanbox_to_pointer(x: Janet) ?*anyopaque;
pub extern fn janet_nanbox_from_pointer(p: ?*anyopaque, tagmask: u64) Janet;
pub extern fn janet_nanbox_from_cpointer(p: ?*const anyopaque, tagmask: u64) Janet;
pub extern fn janet_nanbox_from_double(d: f64) Janet;
pub extern fn janet_nanbox_from_bits(bits: u64) Janet;
pub extern fn janet_checkint(x: Janet) c_int;
pub extern fn janet_checkint64(x: Janet) c_int;
pub extern fn janet_checkuint64(x: Janet) c_int;
pub extern fn janet_checksize(x: Janet) c_int;
pub extern fn janet_checkabstract(x: Janet, at: [*c]const JanetAbstractType) JanetAbstract;
pub const struct_JanetParseState = opaque {};
pub const JanetParseState = struct_JanetParseState;
pub const struct_JanetParser = extern struct {
    args: [*c]Janet,
    @"error": [*c]const u8,
    states: ?*JanetParseState,
    buf: [*c]u8,
    argcount: usize,
    argcap: usize,
    statecount: usize,
    statecap: usize,
    bufcount: usize,
    bufcap: usize,
    line: usize,
    column: usize,
    pending: usize,
    lookback: c_int,
    flag: c_int,
};
pub const JanetParser = struct_JanetParser;
pub const JANET_PARSE_ROOT: c_int = 0;
pub const JANET_PARSE_ERROR: c_int = 1;
pub const JANET_PARSE_PENDING: c_int = 2;
pub const JANET_PARSE_DEAD: c_int = 3;
pub const enum_JanetParserStatus = c_uint;
pub const JanetMarshalContext = extern struct {
    m_state: ?*anyopaque,
    u_state: ?*anyopaque,
    flags: c_int,
    data: [*c]const u8,
    at: [*c]const JanetAbstractType,
};
pub const struct_JanetFile = extern struct {
    file: [*c]FILE,
    flags: i32,
};
pub const JanetFile = struct_JanetFile;
pub const JanetTryState = extern struct {
    stackn: i32,
    gc_handle: c_int,
    vm_fiber: [*c]JanetFiber,
    vm_jmp_buf: [*c]jmp_buf,
    vm_return_reg: [*c]Janet,
    buf: jmp_buf,
    payload: Janet,
};
pub const JANET_OAT_SLOT: c_int = 0;
pub const JANET_OAT_ENVIRONMENT: c_int = 1;
pub const JANET_OAT_CONSTANT: c_int = 2;
pub const JANET_OAT_INTEGER: c_int = 3;
pub const JANET_OAT_TYPE: c_int = 4;
pub const JANET_OAT_SIMPLETYPE: c_int = 5;
pub const JANET_OAT_LABEL: c_int = 6;
pub const JANET_OAT_FUNCDEF: c_int = 7;
pub const enum_JanetOpArgType = c_uint;
pub const JINT_0: c_int = 0;
pub const JINT_S: c_int = 1;
pub const JINT_L: c_int = 2;
pub const JINT_SS: c_int = 3;
pub const JINT_SL: c_int = 4;
pub const JINT_ST: c_int = 5;
pub const JINT_SI: c_int = 6;
pub const JINT_SD: c_int = 7;
pub const JINT_SU: c_int = 8;
pub const JINT_SSS: c_int = 9;
pub const JINT_SSI: c_int = 10;
pub const JINT_SSU: c_int = 11;
pub const JINT_SES: c_int = 12;
pub const JINT_SC: c_int = 13;
pub const enum_JanetInstructionType = c_uint;
pub const JOP_NOOP: c_int = 0;
pub const JOP_ERROR: c_int = 1;
pub const JOP_TYPECHECK: c_int = 2;
pub const JOP_RETURN: c_int = 3;
pub const JOP_RETURN_NIL: c_int = 4;
pub const JOP_ADD_IMMEDIATE: c_int = 5;
pub const JOP_ADD: c_int = 6;
pub const JOP_SUBTRACT: c_int = 7;
pub const JOP_MULTIPLY_IMMEDIATE: c_int = 8;
pub const JOP_MULTIPLY: c_int = 9;
pub const JOP_DIVIDE_IMMEDIATE: c_int = 10;
pub const JOP_DIVIDE: c_int = 11;
pub const JOP_MODULO: c_int = 12;
pub const JOP_REMAINDER: c_int = 13;
pub const JOP_BAND: c_int = 14;
pub const JOP_BOR: c_int = 15;
pub const JOP_BXOR: c_int = 16;
pub const JOP_BNOT: c_int = 17;
pub const JOP_SHIFT_LEFT: c_int = 18;
pub const JOP_SHIFT_LEFT_IMMEDIATE: c_int = 19;
pub const JOP_SHIFT_RIGHT: c_int = 20;
pub const JOP_SHIFT_RIGHT_IMMEDIATE: c_int = 21;
pub const JOP_SHIFT_RIGHT_UNSIGNED: c_int = 22;
pub const JOP_SHIFT_RIGHT_UNSIGNED_IMMEDIATE: c_int = 23;
pub const JOP_MOVE_FAR: c_int = 24;
pub const JOP_MOVE_NEAR: c_int = 25;
pub const JOP_JUMP: c_int = 26;
pub const JOP_JUMP_IF: c_int = 27;
pub const JOP_JUMP_IF_NOT: c_int = 28;
pub const JOP_JUMP_IF_NIL: c_int = 29;
pub const JOP_JUMP_IF_NOT_NIL: c_int = 30;
pub const JOP_GREATER_THAN: c_int = 31;
pub const JOP_GREATER_THAN_IMMEDIATE: c_int = 32;
pub const JOP_LESS_THAN: c_int = 33;
pub const JOP_LESS_THAN_IMMEDIATE: c_int = 34;
pub const JOP_EQUALS: c_int = 35;
pub const JOP_EQUALS_IMMEDIATE: c_int = 36;
pub const JOP_COMPARE: c_int = 37;
pub const JOP_LOAD_NIL: c_int = 38;
pub const JOP_LOAD_TRUE: c_int = 39;
pub const JOP_LOAD_FALSE: c_int = 40;
pub const JOP_LOAD_INTEGER: c_int = 41;
pub const JOP_LOAD_CONSTANT: c_int = 42;
pub const JOP_LOAD_UPVALUE: c_int = 43;
pub const JOP_LOAD_SELF: c_int = 44;
pub const JOP_SET_UPVALUE: c_int = 45;
pub const JOP_CLOSURE: c_int = 46;
pub const JOP_PUSH: c_int = 47;
pub const JOP_PUSH_2: c_int = 48;
pub const JOP_PUSH_3: c_int = 49;
pub const JOP_PUSH_ARRAY: c_int = 50;
pub const JOP_CALL: c_int = 51;
pub const JOP_TAILCALL: c_int = 52;
pub const JOP_RESUME: c_int = 53;
pub const JOP_SIGNAL: c_int = 54;
pub const JOP_PROPAGATE: c_int = 55;
pub const JOP_IN: c_int = 56;
pub const JOP_GET: c_int = 57;
pub const JOP_PUT: c_int = 58;
pub const JOP_GET_INDEX: c_int = 59;
pub const JOP_PUT_INDEX: c_int = 60;
pub const JOP_LENGTH: c_int = 61;
pub const JOP_MAKE_ARRAY: c_int = 62;
pub const JOP_MAKE_BUFFER: c_int = 63;
pub const JOP_MAKE_STRING: c_int = 64;
pub const JOP_MAKE_STRUCT: c_int = 65;
pub const JOP_MAKE_TABLE: c_int = 66;
pub const JOP_MAKE_TUPLE: c_int = 67;
pub const JOP_MAKE_BRACKET_TUPLE: c_int = 68;
pub const JOP_GREATER_THAN_EQUAL: c_int = 69;
pub const JOP_LESS_THAN_EQUAL: c_int = 70;
pub const JOP_NEXT: c_int = 71;
pub const JOP_NOT_EQUALS: c_int = 72;
pub const JOP_NOT_EQUALS_IMMEDIATE: c_int = 73;
pub const JOP_CANCEL: c_int = 74;
pub const JOP_INSTRUCTION_COUNT: c_int = 75;
pub const enum_JanetOpCode = c_uint;
pub extern var janet_instructions: [75]enum_JanetInstructionType;
pub extern const janet_stream_type: JanetAbstractType;
pub extern const janet_channel_type: JanetAbstractType;
pub extern fn janet_loop() void;
pub extern fn janet_loop_done() c_int;
pub extern fn janet_loop1() [*c]JanetFiber;
pub extern fn janet_loop1_interrupt(vm: ?*JanetVM) void;
pub extern fn janet_stream(handle: JanetHandle, flags: u32, methods: [*c]const JanetMethod) [*c]JanetStream;
pub extern fn janet_stream_close(stream: [*c]JanetStream) void;
pub extern fn janet_cfun_stream_close(argc: i32, argv: [*c]Janet) Janet;
pub extern fn janet_cfun_stream_read(argc: i32, argv: [*c]Janet) Janet;
pub extern fn janet_cfun_stream_chunk(argc: i32, argv: [*c]Janet) Janet;
pub extern fn janet_cfun_stream_write(argc: i32, argv: [*c]Janet) Janet;
pub extern fn janet_stream_flags(stream: [*c]JanetStream, flags: u32) void;
pub extern fn janet_schedule(fiber: [*c]JanetFiber, value: Janet) void;
pub extern fn janet_cancel(fiber: [*c]JanetFiber, value: Janet) void;
pub extern fn janet_schedule_signal(fiber: [*c]JanetFiber, value: Janet, sig: JanetSignal) void;
pub extern fn janet_listen(stream: [*c]JanetStream, behavior: JanetListener, mask: c_int, size: usize, user: ?*anyopaque) [*c]JanetListenerState;
pub extern fn janet_await() noreturn;
pub extern fn janet_sleep_await(sec: f64) noreturn;
pub extern fn janet_addtimeout(sec: f64) void;
pub extern fn janet_ev_inc_refcount() void;
pub extern fn janet_ev_dec_refcount() void;
pub extern fn janet_abstract_begin_threaded(atype: [*c]const JanetAbstractType, size: usize) ?*anyopaque;
pub extern fn janet_abstract_end_threaded(x: ?*anyopaque) ?*anyopaque;
pub extern fn janet_abstract_threaded(atype: [*c]const JanetAbstractType, size: usize) ?*anyopaque;
pub extern fn janet_abstract_incref(abst: ?*anyopaque) i32;
pub extern fn janet_abstract_decref(abst: ?*anyopaque) i32;
pub extern fn janet_os_mutex_size() usize;
pub extern fn janet_os_rwlock_size() usize;
pub extern fn janet_os_mutex_init(mutex: ?*JanetOSMutex) void;
pub extern fn janet_os_mutex_deinit(mutex: ?*JanetOSMutex) void;
pub extern fn janet_os_mutex_lock(mutex: ?*JanetOSMutex) void;
pub extern fn janet_os_mutex_unlock(mutex: ?*JanetOSMutex) void;
pub extern fn janet_os_rwlock_init(rwlock: ?*JanetOSRWLock) void;
pub extern fn janet_os_rwlock_deinit(rwlock: ?*JanetOSRWLock) void;
pub extern fn janet_os_rwlock_rlock(rwlock: ?*JanetOSRWLock) void;
pub extern fn janet_os_rwlock_wlock(rwlock: ?*JanetOSRWLock) void;
pub extern fn janet_os_rwlock_runlock(rwlock: ?*JanetOSRWLock) void;
pub extern fn janet_os_rwlock_wunlock(rwlock: ?*JanetOSRWLock) void;
pub extern fn janet_ev_lasterr() Janet;
pub const JanetEVGenericMessage = extern struct {
    tag: c_int,
    argi: c_int,
    argp: ?*anyopaque,
    argj: Janet,
    fiber: [*c]JanetFiber,
};
pub const JanetThreadedSubroutine = ?*const fn (JanetEVGenericMessage) callconv(.C) JanetEVGenericMessage;
pub const JanetCallback = ?*const fn (JanetEVGenericMessage) callconv(.C) void;
pub const JanetThreadedCallback = ?*const fn (JanetEVGenericMessage) callconv(.C) void;
pub extern fn janet_ev_threaded_call(fp: JanetThreadedSubroutine, arguments: JanetEVGenericMessage, cb: JanetThreadedCallback) void;
pub extern fn janet_ev_threaded_await(fp: JanetThreadedSubroutine, tag: c_int, argi: c_int, argp: ?*anyopaque) noreturn;
pub extern fn janet_ev_post_event(vm: ?*JanetVM, cb: JanetCallback, msg: JanetEVGenericMessage) void;
pub extern fn janet_ev_default_threaded_callback(return_value: JanetEVGenericMessage) void;
pub extern fn janet_ev_read(stream: [*c]JanetStream, buf: [*c]JanetBuffer, nbytes: i32) void;
pub extern fn janet_ev_readchunk(stream: [*c]JanetStream, buf: [*c]JanetBuffer, nbytes: i32) void;
pub extern fn janet_ev_recv(stream: [*c]JanetStream, buf: [*c]JanetBuffer, nbytes: i32, flags: c_int) void;
pub extern fn janet_ev_recvchunk(stream: [*c]JanetStream, buf: [*c]JanetBuffer, nbytes: i32, flags: c_int) void;
pub extern fn janet_ev_recvfrom(stream: [*c]JanetStream, buf: [*c]JanetBuffer, nbytes: i32, flags: c_int) void;
pub extern fn janet_ev_write_buffer(stream: [*c]JanetStream, buf: [*c]JanetBuffer) void;
pub extern fn janet_ev_write_string(stream: [*c]JanetStream, str: JanetString) void;
pub extern fn janet_ev_send_buffer(stream: [*c]JanetStream, buf: [*c]JanetBuffer, flags: c_int) void;
pub extern fn janet_ev_send_string(stream: [*c]JanetStream, str: JanetString, flags: c_int) void;
pub extern fn janet_ev_sendto_buffer(stream: [*c]JanetStream, buf: [*c]JanetBuffer, dest: ?*anyopaque, flags: c_int) void;
pub extern fn janet_ev_sendto_string(stream: [*c]JanetStream, str: JanetString, dest: ?*anyopaque, flags: c_int) void;
pub extern const janet_parser_type: JanetAbstractType;
pub extern fn janet_parser_init(parser: [*c]JanetParser) void;
pub extern fn janet_parser_deinit(parser: [*c]JanetParser) void;
pub extern fn janet_parser_consume(parser: [*c]JanetParser, c: u8) void;
pub extern fn janet_parser_status(parser: [*c]JanetParser) enum_JanetParserStatus;
pub extern fn janet_parser_produce(parser: [*c]JanetParser) Janet;
pub extern fn janet_parser_produce_wrapped(parser: [*c]JanetParser) Janet;
pub extern fn janet_parser_error(parser: [*c]JanetParser) [*c]const u8;
pub extern fn janet_parser_flush(parser: [*c]JanetParser) void;
pub extern fn janet_parser_eof(parser: [*c]JanetParser) void;
pub extern fn janet_parser_has_more(parser: [*c]JanetParser) c_int;
pub const JANET_ASSEMBLE_OK: c_int = 0;
pub const JANET_ASSEMBLE_ERROR: c_int = 1;
pub const enum_JanetAssembleStatus = c_uint;
pub const struct_JanetAssembleResult = extern struct {
    funcdef: [*c]JanetFuncDef,
    @"error": JanetString,
    status: enum_JanetAssembleStatus,
};
pub const JanetAssembleResult = struct_JanetAssembleResult;
pub extern fn janet_asm(source: Janet, flags: c_int) JanetAssembleResult;
pub extern fn janet_disasm(def: [*c]JanetFuncDef) Janet;
pub extern fn janet_asm_decode_instruction(instr: u32) Janet;
pub const JANET_COMPILE_OK: c_int = 0;
pub const JANET_COMPILE_ERROR: c_int = 1;
pub const enum_JanetCompileStatus = c_uint;
pub const struct_JanetCompileResult = extern struct {
    funcdef: [*c]JanetFuncDef,
    @"error": JanetString,
    macrofiber: [*c]JanetFiber,
    error_mapping: JanetSourceMapping,
    status: enum_JanetCompileStatus,
};
pub const JanetCompileResult = struct_JanetCompileResult;
pub extern fn janet_compile(source: Janet, env: [*c]JanetTable, where: JanetString) JanetCompileResult;
pub extern fn janet_compile_lint(source: Janet, env: [*c]JanetTable, where: JanetString, lints: [*c]JanetArray) JanetCompileResult;
pub extern fn janet_core_env(replacements: [*c]JanetTable) [*c]JanetTable;
pub extern fn janet_core_lookup_table(replacements: [*c]JanetTable) [*c]JanetTable;
pub extern fn janet_dobytes(env: [*c]JanetTable, bytes: [*c]const u8, len: i32, sourcePath: [*c]const u8, out: [*c]Janet) c_int;
pub extern fn janet_dostring(env: [*c]JanetTable, str: [*c]const u8, sourcePath: [*c]const u8, out: [*c]Janet) c_int;
pub extern fn janet_loop_fiber(fiber: [*c]JanetFiber) c_int;
pub extern fn janet_scan_number(str: [*c]const u8, len: i32, out: [*c]f64) c_int;
pub extern fn janet_scan_number_base(str: [*c]const u8, len: i32, base: i32, out: [*c]f64) c_int;
pub extern fn janet_scan_int64(str: [*c]const u8, len: i32, out: [*c]i64) c_int;
pub extern fn janet_scan_uint64(str: [*c]const u8, len: i32, out: [*c]u64) c_int;
pub extern fn janet_debug_break(def: [*c]JanetFuncDef, pc: i32) void;
pub extern fn janet_debug_unbreak(def: [*c]JanetFuncDef, pc: i32) void;
pub extern fn janet_debug_find(def_out: [*c][*c]JanetFuncDef, pc_out: [*c]i32, source: JanetString, line: i32, column: i32) void;
pub extern const janet_rng_type: JanetAbstractType;
pub extern fn janet_default_rng() [*c]JanetRNG;
pub extern fn janet_rng_seed(rng: [*c]JanetRNG, seed: u32) void;
pub extern fn janet_rng_longseed(rng: [*c]JanetRNG, bytes: [*c]const u8, len: i32) void;
pub extern fn janet_rng_u32(rng: [*c]JanetRNG) u32;
pub extern fn janet_rng_double(rng: [*c]JanetRNG) f64;
pub extern fn janet_array(capacity: i32) [*c]JanetArray;
pub extern fn janet_array_n(elements: [*c]const Janet, n: i32) [*c]JanetArray;
pub extern fn janet_array_ensure(array: [*c]JanetArray, capacity: i32, growth: i32) void;
pub extern fn janet_array_setcount(array: [*c]JanetArray, count: i32) void;
pub extern fn janet_array_push(array: [*c]JanetArray, x: Janet) void;
pub extern fn janet_array_pop(array: [*c]JanetArray) Janet;
pub extern fn janet_array_peek(array: [*c]JanetArray) Janet;
pub extern fn janet_buffer(capacity: i32) [*c]JanetBuffer;
pub extern fn janet_buffer_init(buffer: [*c]JanetBuffer, capacity: i32) [*c]JanetBuffer;
pub extern fn janet_buffer_deinit(buffer: [*c]JanetBuffer) void;
pub extern fn janet_buffer_ensure(buffer: [*c]JanetBuffer, capacity: i32, growth: i32) void;
pub extern fn janet_buffer_setcount(buffer: [*c]JanetBuffer, count: i32) void;
pub extern fn janet_buffer_extra(buffer: [*c]JanetBuffer, n: i32) void;
pub extern fn janet_buffer_push_bytes(buffer: [*c]JanetBuffer, string: [*c]const u8, len: i32) void;
pub extern fn janet_buffer_push_string(buffer: [*c]JanetBuffer, string: JanetString) void;
pub extern fn janet_buffer_push_cstring(buffer: [*c]JanetBuffer, cstring: [*c]const u8) void;
pub extern fn janet_buffer_push_u8(buffer: [*c]JanetBuffer, x: u8) void;
pub extern fn janet_buffer_push_u16(buffer: [*c]JanetBuffer, x: u16) void;
pub extern fn janet_buffer_push_u32(buffer: [*c]JanetBuffer, x: u32) void;
pub extern fn janet_buffer_push_u64(buffer: [*c]JanetBuffer, x: u64) void;
pub extern fn janet_tuple_begin(length: i32) [*c]Janet;
pub extern fn janet_tuple_end(tuple: [*c]Janet) JanetTuple;
pub extern fn janet_tuple_n(values: [*c]const Janet, n: i32) JanetTuple;
pub extern fn janet_string_begin(length: i32) [*c]u8;
pub extern fn janet_string_end(str: [*c]u8) JanetString;
pub extern fn janet_string(buf: [*c]const u8, len: i32) JanetString;
pub extern fn janet_cstring(cstring: [*c]const u8) JanetString;
pub extern fn janet_string_compare(lhs: JanetString, rhs: JanetString) c_int;
pub extern fn janet_string_equal(lhs: JanetString, rhs: JanetString) c_int;
pub extern fn janet_string_equalconst(lhs: JanetString, rhs: [*c]const u8, rlen: i32, rhash: i32) c_int;
pub extern fn janet_description(x: Janet) JanetString;
pub extern fn janet_to_string(x: Janet) JanetString;
pub extern fn janet_to_string_b(buffer: [*c]JanetBuffer, x: Janet) void;
pub extern fn janet_description_b(buffer: [*c]JanetBuffer, x: Janet) void;
pub extern fn janet_formatc(format: [*c]const u8, ...) JanetString;
pub extern fn janet_formatb(bufp: [*c]JanetBuffer, format: [*c]const u8, ...) [*c]JanetBuffer;
pub extern fn janet_formatbv(bufp: [*c]JanetBuffer, format: [*c]const u8, args: [*c]struct___va_list_tag) void;
pub extern fn janet_symbol(str: [*c]const u8, len: i32) JanetSymbol;
pub extern fn janet_csymbol(str: [*c]const u8) JanetSymbol;
pub extern fn janet_symbol_gen() JanetSymbol;
pub extern fn janet_struct_begin(count: i32) [*c]JanetKV;
pub extern fn janet_struct_put(st: [*c]JanetKV, key: Janet, value: Janet) void;
pub extern fn janet_struct_end(st: [*c]JanetKV) JanetStruct;
pub extern fn janet_struct_get(st: JanetStruct, key: Janet) Janet;
pub extern fn janet_struct_rawget(st: JanetStruct, key: Janet) Janet;
pub extern fn janet_struct_get_ex(st: JanetStruct, key: Janet, which: [*c]JanetStruct) Janet;
pub extern fn janet_struct_to_table(st: JanetStruct) [*c]JanetTable;
pub extern fn janet_struct_find(st: JanetStruct, key: Janet) [*c]const JanetKV;
pub extern fn janet_table(capacity: i32) [*c]JanetTable;
pub extern fn janet_table_init(table: [*c]JanetTable, capacity: i32) [*c]JanetTable;
pub extern fn janet_table_init_raw(table: [*c]JanetTable, capacity: i32) [*c]JanetTable;
pub extern fn janet_table_deinit(table: [*c]JanetTable) void;
pub extern fn janet_table_get(t: [*c]JanetTable, key: Janet) Janet;
pub extern fn janet_table_get_ex(t: [*c]JanetTable, key: Janet, which: [*c][*c]JanetTable) Janet;
pub extern fn janet_table_rawget(t: [*c]JanetTable, key: Janet) Janet;
pub extern fn janet_table_remove(t: [*c]JanetTable, key: Janet) Janet;
pub extern fn janet_table_put(t: [*c]JanetTable, key: Janet, value: Janet) void;
pub extern fn janet_table_to_struct(t: [*c]JanetTable) JanetStruct;
pub extern fn janet_table_merge_table(table: [*c]JanetTable, other: [*c]JanetTable) void;
pub extern fn janet_table_merge_struct(table: [*c]JanetTable, other: JanetStruct) void;
pub extern fn janet_table_find(t: [*c]JanetTable, key: Janet) [*c]JanetKV;
pub extern fn janet_table_clone(table: [*c]JanetTable) [*c]JanetTable;
pub extern fn janet_table_clear(table: [*c]JanetTable) void;
pub extern fn janet_fiber(callee: [*c]JanetFunction, capacity: i32, argc: i32, argv: [*c]const Janet) [*c]JanetFiber;
pub extern fn janet_fiber_reset(fiber: [*c]JanetFiber, callee: [*c]JanetFunction, argc: i32, argv: [*c]const Janet) [*c]JanetFiber;
pub extern fn janet_fiber_status(fiber: [*c]JanetFiber) JanetFiberStatus;
pub extern fn janet_current_fiber() [*c]JanetFiber;
pub extern fn janet_root_fiber() [*c]JanetFiber;
pub extern fn janet_indexed_view(seq: Janet, data: [*c][*c]const Janet, len: [*c]i32) c_int;
pub extern fn janet_bytes_view(str: Janet, data: [*c][*c]const u8, len: [*c]i32) c_int;
pub extern fn janet_dictionary_view(tab: Janet, data: [*c][*c]const JanetKV, len: [*c]i32, cap: [*c]i32) c_int;
pub extern fn janet_dictionary_get(data: [*c]const JanetKV, cap: i32, key: Janet) Janet;
pub extern fn janet_dictionary_next(kvs: [*c]const JanetKV, cap: i32, kv: [*c]const JanetKV) [*c]const JanetKV;
pub extern fn janet_abstract_begin(@"type": [*c]const JanetAbstractType, size: usize) ?*anyopaque;
pub extern fn janet_abstract_end(abstractTemplate: ?*anyopaque) JanetAbstract;
pub extern fn janet_abstract(@"type": [*c]const JanetAbstractType, size: usize) JanetAbstract;
pub const JanetModule = ?*const fn ([*c]JanetTable) callconv(.C) void;
pub const JanetModconf = ?*const fn () callconv(.C) JanetBuildConfig;
pub extern fn janet_native(name: [*c]const u8, @"error": [*c]JanetString) JanetModule;
pub extern fn janet_marshal(buf: [*c]JanetBuffer, x: Janet, rreg: [*c]JanetTable, flags: c_int) void;
pub extern fn janet_unmarshal(bytes: [*c]const u8, len: usize, flags: c_int, reg: [*c]JanetTable, next: [*c][*c]const u8) Janet;
pub extern fn janet_env_lookup(env: [*c]JanetTable) [*c]JanetTable;
pub extern fn janet_env_lookup_into(renv: [*c]JanetTable, env: [*c]JanetTable, prefix: [*c]const u8, recurse: c_int) void;
pub extern fn janet_mark(x: Janet) void;
pub extern fn janet_sweep() void;
pub extern fn janet_collect() void;
pub extern fn janet_clear_memory() void;
pub extern fn janet_gcroot(root: Janet) void;
pub extern fn janet_gcunroot(root: Janet) c_int;
pub extern fn janet_gcunrootall(root: Janet) c_int;
pub extern fn janet_gclock() c_int;
pub extern fn janet_gcunlock(handle: c_int) void;
pub extern fn janet_gcpressure(s: usize) void;
pub extern fn janet_funcdef_alloc() [*c]JanetFuncDef;
pub extern fn janet_thunk(def: [*c]JanetFuncDef) [*c]JanetFunction;
pub extern fn janet_verify(def: [*c]JanetFuncDef) c_int;
pub extern fn janet_pretty(buffer: [*c]JanetBuffer, depth: c_int, flags: c_int, x: Janet) [*c]JanetBuffer;
pub extern fn janet_try_init(state: [*c]JanetTryState) void;
pub extern fn janet_restore(state: [*c]JanetTryState) void;
pub extern fn janet_equals(x: Janet, y: Janet) c_int;
pub extern fn janet_hash(x: Janet) i32;
pub extern fn janet_compare(x: Janet, y: Janet) c_int;
pub extern fn janet_cstrcmp(str: JanetString, other: [*c]const u8) c_int;
pub extern fn janet_in(ds: Janet, key: Janet) Janet;
pub extern fn janet_get(ds: Janet, key: Janet) Janet;
pub extern fn janet_next(ds: Janet, key: Janet) Janet;
pub extern fn janet_getindex(ds: Janet, index: i32) Janet;
pub extern fn janet_length(x: Janet) i32;
pub extern fn janet_lengthv(x: Janet) Janet;
pub extern fn janet_put(ds: Janet, key: Janet, value: Janet) void;
pub extern fn janet_putindex(ds: Janet, index: i32, value: Janet) void;
pub extern fn janet_wrap_number_safe(x: f64) Janet;
pub extern fn janet_keyeq(x: Janet, cstring: [*c]const u8) c_int;
pub extern fn janet_streq(x: Janet, cstring: [*c]const u8) c_int;
pub extern fn janet_symeq(x: Janet, cstring: [*c]const u8) c_int;
pub extern fn janet_sorted_keys(dict: [*c]const JanetKV, cap: i32, index_buffer: [*c]i32) i32;
pub extern fn janet_init() c_int;
pub extern fn janet_deinit() void;
pub extern fn janet_vm_alloc() ?*JanetVM;
pub extern fn janet_local_vm() ?*JanetVM;
pub extern fn janet_vm_free(vm: ?*JanetVM) void;
pub extern fn janet_vm_save(into: ?*JanetVM) void;
pub extern fn janet_vm_load(from: ?*JanetVM) void;
pub extern fn janet_interpreter_interrupt(vm: ?*JanetVM) void;
pub extern fn janet_continue(fiber: [*c]JanetFiber, in: Janet, out: [*c]Janet) JanetSignal;
pub extern fn janet_continue_signal(fiber: [*c]JanetFiber, in: Janet, out: [*c]Janet, sig: JanetSignal) JanetSignal;
pub extern fn janet_pcall(fun: [*c]JanetFunction, argn: i32, argv: [*c]const Janet, out: [*c]Janet, f: [*c][*c]JanetFiber) JanetSignal;
pub extern fn janet_step(fiber: [*c]JanetFiber, in: Janet, out: [*c]Janet) JanetSignal;
pub extern fn janet_call(fun: [*c]JanetFunction, argc: i32, argv: [*c]const Janet) Janet;
pub extern fn janet_mcall(name: [*c]const u8, argc: i32, argv: [*c]Janet) Janet;
pub extern fn janet_stacktrace(fiber: [*c]JanetFiber, err: Janet) void;
pub extern fn janet_stacktrace_ext(fiber: [*c]JanetFiber, err: Janet, prefix: [*c]const u8) void;
pub const JanetScratchFinalizer = ?*const fn (?*anyopaque) callconv(.C) void;
pub extern fn janet_smalloc(size: usize) ?*anyopaque;
pub extern fn janet_srealloc(mem: ?*anyopaque, size: usize) ?*anyopaque;
pub extern fn janet_scalloc(nmemb: usize, size: usize) ?*anyopaque;
pub extern fn janet_sfinalizer(mem: ?*anyopaque, finalizer: JanetScratchFinalizer) void;
pub extern fn janet_sfree(mem: ?*anyopaque) void;
pub const JANET_BINDING_NONE: c_int = 0;
pub const JANET_BINDING_DEF: c_int = 1;
pub const JANET_BINDING_VAR: c_int = 2;
pub const JANET_BINDING_MACRO: c_int = 3;
pub const JANET_BINDING_DYNAMIC_DEF: c_int = 4;
pub const JANET_BINDING_DYNAMIC_MACRO: c_int = 5;
pub const JanetBindingType = c_uint;
pub const JANET_BINDING_DEP_NONE: c_int = 0;
pub const JANET_BINDING_DEP_RELAXED: c_int = 1;
pub const JANET_BINDING_DEP_NORMAL: c_int = 2;
pub const JANET_BINDING_DEP_STRICT: c_int = 3;
const enum_unnamed_6 = c_uint;
pub const JanetBinding = extern struct {
    type: JanetBindingType,
    value: Janet,
    deprecation: enum_unnamed_6,
};
pub extern fn janet_def(env: [*c]JanetTable, name: [*c]const u8, val: Janet, documentation: [*c]const u8) void;
pub extern fn janet_var(env: [*c]JanetTable, name: [*c]const u8, val: Janet, documentation: [*c]const u8) void;
pub extern fn janet_cfuns(env: [*c]JanetTable, regprefix: [*c]const u8, cfuns: [*c]const JanetReg) void;
pub extern fn janet_cfuns_prefix(env: [*c]JanetTable, regprefix: [*c]const u8, cfuns: [*c]const JanetReg) void;
pub extern fn janet_resolve(env: [*c]JanetTable, sym: JanetSymbol, out: [*c]Janet) JanetBindingType;
pub extern fn janet_resolve_ext(env: [*c]JanetTable, sym: JanetSymbol) JanetBinding;
pub extern fn janet_resolve_core(name: [*c]const u8) Janet;
pub extern fn janet_cfuns_ext(env: [*c]JanetTable, regprefix: [*c]const u8, cfuns: [*c]const JanetRegExt) void;
pub extern fn janet_cfuns_ext_prefix(env: [*c]JanetTable, regprefix: [*c]const u8, cfuns: [*c]const JanetRegExt) void;
pub extern fn janet_def_sm(env: [*c]JanetTable, name: [*c]const u8, val: Janet, documentation: [*c]const u8, source_file: [*c]const u8, source_line: i32) void;
pub extern fn janet_var_sm(env: [*c]JanetTable, name: [*c]const u8, val: Janet, documentation: [*c]const u8, source_file: [*c]const u8, source_line: i32) void;
pub extern fn janet_register(name: [*c]const u8, cfun: JanetCFunction) void;
pub extern fn janet_signalv(signal: JanetSignal, message: Janet) noreturn;
pub extern fn janet_panicv(message: Janet) noreturn;
pub extern fn janet_panic(message: [*c]const u8) noreturn;
pub extern fn janet_panics(message: JanetString) noreturn;
pub extern fn janet_panicf(format: [*c]const u8, ...) noreturn;
pub extern fn janet_dynprintf(name: [*c]const u8, dflt_file: [*c]FILE, format: [*c]const u8, ...) void;
pub extern fn janet_panic_type(x: Janet, n: i32, expected: c_int) noreturn;
pub extern fn janet_panic_abstract(x: Janet, n: i32, at: [*c]const JanetAbstractType) noreturn;
pub extern fn janet_arity(arity: i32, min: i32, max: i32) void;
pub extern fn janet_fixarity(arity: i32, fix: i32) void;
pub extern fn janet_getmethod(method: JanetKeyword, methods: [*c]const JanetMethod, out: [*c]Janet) c_int;
pub extern fn janet_nextmethod(methods: [*c]const JanetMethod, key: Janet) Janet;
pub extern fn janet_getnumber(argv: [*c]const Janet, n: i32) f64;
pub extern fn janet_getarray(argv: [*c]const Janet, n: i32) [*c]JanetArray;
pub extern fn janet_gettuple(argv: [*c]const Janet, n: i32) JanetTuple;
pub extern fn janet_gettable(argv: [*c]const Janet, n: i32) [*c]JanetTable;
pub extern fn janet_getstruct(argv: [*c]const Janet, n: i32) JanetStruct;
pub extern fn janet_getstring(argv: [*c]const Janet, n: i32) JanetString;
pub extern fn janet_getcstring(argv: [*c]const Janet, n: i32) [*c]const u8;
pub extern fn janet_getsymbol(argv: [*c]const Janet, n: i32) JanetSymbol;
pub extern fn janet_getkeyword(argv: [*c]const Janet, n: i32) JanetKeyword;
pub extern fn janet_getbuffer(argv: [*c]const Janet, n: i32) [*c]JanetBuffer;
pub extern fn janet_getfiber(argv: [*c]const Janet, n: i32) [*c]JanetFiber;
pub extern fn janet_getfunction(argv: [*c]const Janet, n: i32) [*c]JanetFunction;
pub extern fn janet_getcfunction(argv: [*c]const Janet, n: i32) JanetCFunction;
pub extern fn janet_getboolean(argv: [*c]const Janet, n: i32) c_int;
pub extern fn janet_getpointer(argv: [*c]const Janet, n: i32) ?*anyopaque;
pub extern fn janet_getnat(argv: [*c]const Janet, n: i32) i32;
pub extern fn janet_getinteger(argv: [*c]const Janet, n: i32) i32;
pub extern fn janet_getinteger64(argv: [*c]const Janet, n: i32) i64;
pub extern fn janet_getuinteger64(argv: [*c]const Janet, n: i32) u64;
pub extern fn janet_getsize(argv: [*c]const Janet, n: i32) usize;
pub extern fn janet_getindexed(argv: [*c]const Janet, n: i32) JanetView;
pub extern fn janet_getbytes(argv: [*c]const Janet, n: i32) JanetByteView;
pub extern fn janet_getdictionary(argv: [*c]const Janet, n: i32) JanetDictView;
pub extern fn janet_getabstract(argv: [*c]const Janet, n: i32, at: [*c]const JanetAbstractType) ?*anyopaque;
pub extern fn janet_getslice(argc: i32, argv: [*c]const Janet) JanetRange;
pub extern fn janet_gethalfrange(argv: [*c]const Janet, n: i32, length: i32, which: [*c]const u8) i32;
pub extern fn janet_getargindex(argv: [*c]const Janet, n: i32, length: i32, which: [*c]const u8) i32;
pub extern fn janet_getflags(argv: [*c]const Janet, n: i32, flags: [*c]const u8) u64;
pub extern fn janet_optnumber(argv: [*c]const Janet, argc: i32, n: i32, dflt: f64) f64;
pub extern fn janet_opttuple(argv: [*c]const Janet, argc: i32, n: i32, dflt: JanetTuple) JanetTuple;
pub extern fn janet_optstruct(argv: [*c]const Janet, argc: i32, n: i32, dflt: JanetStruct) JanetStruct;
pub extern fn janet_optstring(argv: [*c]const Janet, argc: i32, n: i32, dflt: JanetString) JanetString;
pub extern fn janet_optcstring(argv: [*c]const Janet, argc: i32, n: i32, dflt: [*c]const u8) [*c]const u8;
pub extern fn janet_optsymbol(argv: [*c]const Janet, argc: i32, n: i32, dflt: JanetString) JanetSymbol;
pub extern fn janet_optkeyword(argv: [*c]const Janet, argc: i32, n: i32, dflt: JanetString) JanetKeyword;
pub extern fn janet_optfiber(argv: [*c]const Janet, argc: i32, n: i32, dflt: [*c]JanetFiber) [*c]JanetFiber;
pub extern fn janet_optfunction(argv: [*c]const Janet, argc: i32, n: i32, dflt: [*c]JanetFunction) [*c]JanetFunction;
pub extern fn janet_optcfunction(argv: [*c]const Janet, argc: i32, n: i32, dflt: JanetCFunction) JanetCFunction;
pub extern fn janet_optboolean(argv: [*c]const Janet, argc: i32, n: i32, dflt: c_int) c_int;
pub extern fn janet_optpointer(argv: [*c]const Janet, argc: i32, n: i32, dflt: ?*anyopaque) ?*anyopaque;
pub extern fn janet_optnat(argv: [*c]const Janet, argc: i32, n: i32, dflt: i32) i32;
pub extern fn janet_optinteger(argv: [*c]const Janet, argc: i32, n: i32, dflt: i32) i32;
pub extern fn janet_optinteger64(argv: [*c]const Janet, argc: i32, n: i32, dflt: i64) i64;
pub extern fn janet_optsize(argv: [*c]const Janet, argc: i32, n: i32, dflt: usize) usize;
pub extern fn janet_optabstract(argv: [*c]const Janet, argc: i32, n: i32, at: [*c]const JanetAbstractType, dflt: JanetAbstract) JanetAbstract;
pub extern fn janet_optbuffer(argv: [*c]const Janet, argc: i32, n: i32, dflt_len: i32) [*c]JanetBuffer;
pub extern fn janet_opttable(argv: [*c]const Janet, argc: i32, n: i32, dflt_len: i32) [*c]JanetTable;
pub extern fn janet_optarray(argv: [*c]const Janet, argc: i32, n: i32, dflt_len: i32) [*c]JanetArray;
pub extern fn janet_dyn(name: [*c]const u8) Janet;
pub extern fn janet_setdyn(name: [*c]const u8, value: Janet) void;
pub extern const janet_file_type: JanetAbstractType;
pub extern fn janet_makefile(f: [*c]FILE, flags: i32) Janet;
pub extern fn janet_makejfile(f: [*c]FILE, flags: i32) [*c]JanetFile;
pub extern fn janet_getfile(argv: [*c]const Janet, n: i32, flags: [*c]i32) [*c]FILE;
pub extern fn janet_dynfile(name: [*c]const u8, def: [*c]FILE) [*c]FILE;
pub extern fn janet_getjfile(argv: [*c]const Janet, n: i32) [*c]JanetFile;
pub extern fn janet_checkfile(j: Janet) JanetAbstract;
pub extern fn janet_unwrapfile(j: Janet, flags: [*c]i32) [*c]FILE;
pub extern fn janet_file_close(file: [*c]JanetFile) c_int;
pub extern fn janet_cryptorand(out: [*c]u8, n: usize) c_int;
pub extern fn janet_marshal_size(ctx: [*c]JanetMarshalContext, value: usize) void;
pub extern fn janet_marshal_int(ctx: [*c]JanetMarshalContext, value: i32) void;
pub extern fn janet_marshal_int64(ctx: [*c]JanetMarshalContext, value: i64) void;
pub extern fn janet_marshal_byte(ctx: [*c]JanetMarshalContext, value: u8) void;
pub extern fn janet_marshal_bytes(ctx: [*c]JanetMarshalContext, bytes: [*c]const u8, len: usize) void;
pub extern fn janet_marshal_janet(ctx: [*c]JanetMarshalContext, x: Janet) void;
pub extern fn janet_marshal_abstract(ctx: [*c]JanetMarshalContext, abstract: JanetAbstract) void;
pub extern fn janet_unmarshal_ensure(ctx: [*c]JanetMarshalContext, size: usize) void;
pub extern fn janet_unmarshal_size(ctx: [*c]JanetMarshalContext) usize;
pub extern fn janet_unmarshal_int(ctx: [*c]JanetMarshalContext) i32;
pub extern fn janet_unmarshal_int64(ctx: [*c]JanetMarshalContext) i64;
pub extern fn janet_unmarshal_byte(ctx: [*c]JanetMarshalContext) u8;
pub extern fn janet_unmarshal_bytes(ctx: [*c]JanetMarshalContext, dest: [*c]u8, len: usize) void;
pub extern fn janet_unmarshal_janet(ctx: [*c]JanetMarshalContext) Janet;
pub extern fn janet_unmarshal_abstract(ctx: [*c]JanetMarshalContext, size: usize) JanetAbstract;
pub extern fn janet_unmarshal_abstract_reuse(ctx: [*c]JanetMarshalContext, p: ?*anyopaque) void;
pub extern fn janet_register_abstract_type(at: [*c]const JanetAbstractType) void;
pub extern fn janet_get_abstract_type(key: Janet) [*c]const JanetAbstractType;
pub extern const janet_peg_type: JanetAbstractType;
pub const RULE_LITERAL: c_int = 0;
pub const RULE_NCHAR: c_int = 1;
pub const RULE_NOTNCHAR: c_int = 2;
pub const RULE_RANGE: c_int = 3;
pub const RULE_SET: c_int = 4;
pub const RULE_LOOK: c_int = 5;
pub const RULE_CHOICE: c_int = 6;
pub const RULE_SEQUENCE: c_int = 7;
pub const RULE_IF: c_int = 8;
pub const RULE_IFNOT: c_int = 9;
pub const RULE_NOT: c_int = 10;
pub const RULE_BETWEEN: c_int = 11;
pub const RULE_GETTAG: c_int = 12;
pub const RULE_CAPTURE: c_int = 13;
pub const RULE_POSITION: c_int = 14;
pub const RULE_ARGUMENT: c_int = 15;
pub const RULE_CONSTANT: c_int = 16;
pub const RULE_ACCUMULATE: c_int = 17;
pub const RULE_GROUP: c_int = 18;
pub const RULE_REPLACE: c_int = 19;
pub const RULE_MATCHTIME: c_int = 20;
pub const RULE_ERROR: c_int = 21;
pub const RULE_DROP: c_int = 22;
pub const RULE_BACKMATCH: c_int = 23;
pub const RULE_TO: c_int = 24;
pub const RULE_THRU: c_int = 25;
pub const RULE_LENPREFIX: c_int = 26;
pub const RULE_READINT: c_int = 27;
pub const RULE_LINE: c_int = 28;
pub const RULE_COLUMN: c_int = 29;
pub const RULE_UNREF: c_int = 30;
pub const RULE_CAPTURE_NUM: c_int = 31;
pub const JanetPegOpcod = c_uint;
pub const JanetPeg = extern struct {
    bytecode: [*c]u32,
    constants: [*c]Janet,
    bytecode_len: usize,
    num_constants: u32,
    has_backref: c_int,
};
pub extern const janet_s64_type: JanetAbstractType;
pub extern const janet_u64_type: JanetAbstractType;
pub const JANET_INT_NONE: c_int = 0;
pub const JANET_INT_S64: c_int = 1;
pub const JANET_INT_U64: c_int = 2;
pub const JanetIntType = c_uint;
pub extern fn janet_is_int(x: Janet) JanetIntType;
pub extern fn janet_wrap_s64(x: i64) Janet;
pub extern fn janet_wrap_u64(x: u64) Janet;
pub extern fn janet_unwrap_s64(x: Janet) i64;
pub extern fn janet_unwrap_u64(x: Janet) u64;
pub extern fn janet_malloc(usize) ?*anyopaque;
pub extern fn janet_realloc(?*anyopaque, usize) ?*anyopaque;
pub extern fn janet_calloc(usize, usize) ?*anyopaque;
pub extern fn janet_free(?*anyopaque) void;
pub const __INTMAX_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `L`"); // (no file):80:9
pub const __UINTMAX_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `UL`"); // (no file):86:9
pub const __INT64_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `L`"); // (no file):169:9
pub const __UINT32_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `U`"); // (no file):191:9
pub const __UINT64_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `UL`"); // (no file):199:9
pub const __seg_gs = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // (no file):326:9
pub const __seg_fs = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // (no file):327:9
pub const JANET_THREAD_LOCAL = @compileError("unable to translate macro: undefined identifier `__thread`"); // /usr/local/include/janet/janet.h:217:9
pub const JANET_API = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/local/include/janet/janet.h:283:9
pub const JANET_NO_RETURN = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/local/include/janet/janet.h:292:9
pub const __GLIBC_USE = @compileError("unable to translate macro: undefined identifier `__GLIBC_USE_`"); // /usr/include/features.h:186:9
pub const __glibc_has_attribute = @compileError("unable to translate macro: undefined identifier `__has_attribute`"); // /usr/include/sys/cdefs.h:45:10
pub const __glibc_has_extension = @compileError("unable to translate macro: undefined identifier `__has_extension`"); // /usr/include/sys/cdefs.h:55:10
pub const __THROW = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:79:11
pub const __THROWNL = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:80:11
pub const __NTH = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:81:11
pub const __NTHNL = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:82:11
pub const __CONCAT = @compileError("unable to translate C expr: unexpected token '##'"); // /usr/include/sys/cdefs.h:124:9
pub const __STRING = @compileError("unable to translate C expr: unexpected token '#'"); // /usr/include/sys/cdefs.h:125:9
pub const __glibc_unsigned_or_positive = @compileError("unable to translate macro: undefined identifier `__typeof`"); // /usr/include/sys/cdefs.h:160:9
pub const __glibc_fortify = @compileError("unable to translate C expr: expected ')' instead got '...'"); // /usr/include/sys/cdefs.h:185:9
pub const __glibc_fortify_n = @compileError("unable to translate C expr: expected ')' instead got '...'"); // /usr/include/sys/cdefs.h:195:9
pub const __warnattr = @compileError("unable to translate C expr: unexpected token 'Eof'"); // /usr/include/sys/cdefs.h:207:10
pub const __errordecl = @compileError("unable to translate C expr: unexpected token 'extern'"); // /usr/include/sys/cdefs.h:208:10
pub const __flexarr = @compileError("unable to translate C expr: unexpected token '['"); // /usr/include/sys/cdefs.h:216:10
pub const __REDIRECT = @compileError("unable to translate macro: undefined identifier `__asm__`"); // /usr/include/sys/cdefs.h:247:10
pub const __REDIRECT_NTH = @compileError("unable to translate macro: undefined identifier `__asm__`"); // /usr/include/sys/cdefs.h:254:11
pub const __REDIRECT_NTHNL = @compileError("unable to translate macro: undefined identifier `__asm__`"); // /usr/include/sys/cdefs.h:256:11
pub const __ASMNAME2 = @compileError("unable to translate C expr: unexpected token 'Identifier'"); // /usr/include/sys/cdefs.h:260:10
pub const __attribute_malloc__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:281:10
pub const __attribute_alloc_size__ = @compileError("unable to translate C expr: unexpected token 'Eof'"); // /usr/include/sys/cdefs.h:292:10
pub const __attribute_alloc_align__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:298:10
pub const __attribute_pure__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:308:10
pub const __attribute_const__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:315:10
pub const __attribute_maybe_unused__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:321:10
pub const __attribute_used__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:330:10
pub const __attribute_noinline__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:331:10
pub const __attribute_deprecated__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:339:10
pub const __attribute_deprecated_msg__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:349:10
pub const __attribute_format_arg__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:362:10
pub const __attribute_format_strfmon__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:372:10
pub const __attribute_nonnull__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:384:11
pub const __returns_nonnull = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:397:10
pub const __attribute_warn_unused_result__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:406:10
pub const __always_inline = @compileError("unable to translate macro: undefined identifier `__inline`"); // /usr/include/sys/cdefs.h:424:10
pub const __attribute_artificial__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:433:10
pub const __extern_inline = @compileError("unable to translate macro: undefined identifier `__inline`"); // /usr/include/sys/cdefs.h:451:11
pub const __extern_always_inline = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:452:11
pub const __restrict_arr = @compileError("unable to translate macro: undefined identifier `__restrict`"); // /usr/include/sys/cdefs.h:495:10
pub const __attribute_copy__ = @compileError("unable to translate C expr: unexpected token 'Eof'"); // /usr/include/sys/cdefs.h:544:10
pub const __LDBL_REDIR2_DECL = @compileError("unable to translate C expr: unexpected token 'Eof'"); // /usr/include/sys/cdefs.h:620:10
pub const __LDBL_REDIR_DECL = @compileError("unable to translate C expr: unexpected token 'Eof'"); // /usr/include/sys/cdefs.h:621:10
pub const __glibc_macro_warning1 = @compileError("unable to translate macro: undefined identifier `_Pragma`"); // /usr/include/sys/cdefs.h:635:10
pub const __glibc_macro_warning = @compileError("unable to translate macro: undefined identifier `GCC`"); // /usr/include/sys/cdefs.h:636:10
pub const __fortified_attr_access = @compileError("unable to translate C expr: unexpected token 'Eof'"); // /usr/include/sys/cdefs.h:681:11
pub const __attr_access = @compileError("unable to translate C expr: unexpected token 'Eof'"); // /usr/include/sys/cdefs.h:682:11
pub const __attr_access_none = @compileError("unable to translate C expr: unexpected token 'Eof'"); // /usr/include/sys/cdefs.h:683:11
pub const __attr_dealloc = @compileError("unable to translate C expr: unexpected token 'Eof'"); // /usr/include/sys/cdefs.h:693:10
pub const __attribute_returns_twice__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:700:10
pub const __CFLOAT32 = @compileError("unable to translate: TODO _Complex"); // /usr/include/bits/floatn-common.h:149:12
pub const __CFLOAT64 = @compileError("unable to translate: TODO _Complex"); // /usr/include/bits/floatn-common.h:160:13
pub const __CFLOAT32X = @compileError("unable to translate: TODO _Complex"); // /usr/include/bits/floatn-common.h:169:12
pub const __CFLOAT64X = @compileError("unable to translate: TODO _Complex"); // /usr/include/bits/floatn-common.h:178:13
pub const __builtin_nansf32 = @compileError("unable to translate macro: undefined identifier `__builtin_nansf`"); // /usr/include/bits/floatn-common.h:221:12
pub const __builtin_huge_valf64 = @compileError("unable to translate macro: undefined identifier `__builtin_huge_val`"); // /usr/include/bits/floatn-common.h:255:13
pub const __builtin_inff64 = @compileError("unable to translate macro: undefined identifier `__builtin_inf`"); // /usr/include/bits/floatn-common.h:256:13
pub const __builtin_nanf64 = @compileError("unable to translate macro: undefined identifier `__builtin_nan`"); // /usr/include/bits/floatn-common.h:257:13
pub const __builtin_nansf64 = @compileError("unable to translate macro: undefined identifier `__builtin_nans`"); // /usr/include/bits/floatn-common.h:258:13
pub const __builtin_huge_valf32x = @compileError("unable to translate macro: undefined identifier `__builtin_huge_val`"); // /usr/include/bits/floatn-common.h:272:12
pub const __builtin_inff32x = @compileError("unable to translate macro: undefined identifier `__builtin_inf`"); // /usr/include/bits/floatn-common.h:273:12
pub const __builtin_nanf32x = @compileError("unable to translate macro: undefined identifier `__builtin_nan`"); // /usr/include/bits/floatn-common.h:274:12
pub const __builtin_nansf32x = @compileError("unable to translate macro: undefined identifier `__builtin_nans`"); // /usr/include/bits/floatn-common.h:275:12
pub const __builtin_huge_valf64x = @compileError("unable to translate macro: undefined identifier `__builtin_huge_vall`"); // /usr/include/bits/floatn-common.h:289:13
pub const __builtin_inff64x = @compileError("unable to translate macro: undefined identifier `__builtin_infl`"); // /usr/include/bits/floatn-common.h:290:13
pub const __builtin_nanf64x = @compileError("unable to translate macro: undefined identifier `__builtin_nanl`"); // /usr/include/bits/floatn-common.h:291:13
pub const __builtin_nansf64x = @compileError("unable to translate macro: undefined identifier `__builtin_nansl`"); // /usr/include/bits/floatn-common.h:292:13
pub const __STD_TYPE = @compileError("unable to translate C expr: unexpected token 'typedef'"); // /usr/include/bits/types.h:137:10
pub const __FSID_T_TYPE = @compileError("unable to translate macro: undefined identifier `__val`"); // /usr/include/bits/typesizes.h:73:9
pub const __FD_ZERO = @compileError("unable to translate macro: undefined identifier `__i`"); // /usr/include/bits/select.h:25:9
pub const __FD_SET = @compileError("unable to translate C expr: expected ')' instead got '|='"); // /usr/include/bits/select.h:32:9
pub const __FD_CLR = @compileError("unable to translate C expr: expected ')' instead got '&='"); // /usr/include/bits/select.h:34:9
pub const __PTHREAD_MUTEX_INITIALIZER = @compileError("unable to translate C expr: unexpected token '{'"); // /usr/include/bits/struct_mutex.h:56:10
pub const __PTHREAD_RWLOCK_ELISION_EXTRA = @compileError("unable to translate C expr: unexpected token '{'"); // /usr/include/bits/struct_rwlock.h:40:11
pub const __ONCE_FLAG_INIT = @compileError("unable to translate C expr: unexpected token '{'"); // /usr/include/bits/thread-shared-types.h:113:9
pub const va_start = @compileError("unable to translate macro: undefined identifier `__builtin_va_start`"); // /home/user/.local/share/zig/0.10.0-dev.3999+fda6d4477/files/lib/include/stdarg.h:17:9
pub const va_end = @compileError("unable to translate macro: undefined identifier `__builtin_va_end`"); // /home/user/.local/share/zig/0.10.0-dev.3999+fda6d4477/files/lib/include/stdarg.h:18:9
pub const va_arg = @compileError("unable to translate macro: undefined identifier `__builtin_va_arg`"); // /home/user/.local/share/zig/0.10.0-dev.3999+fda6d4477/files/lib/include/stdarg.h:19:9
pub const __va_copy = @compileError("unable to translate macro: undefined identifier `__builtin_va_copy`"); // /home/user/.local/share/zig/0.10.0-dev.3999+fda6d4477/files/lib/include/stdarg.h:24:9
pub const va_copy = @compileError("unable to translate macro: undefined identifier `__builtin_va_copy`"); // /home/user/.local/share/zig/0.10.0-dev.3999+fda6d4477/files/lib/include/stdarg.h:27:9
pub const offsetof = @compileError("unable to translate macro: undefined identifier `__builtin_offsetof`"); // /home/user/.local/share/zig/0.10.0-dev.3999+fda6d4477/files/lib/include/stddef.h:104:9
pub const __getc_unlocked_body = @compileError("TODO postfix inc/dec expr"); // /usr/include/bits/types/struct_FILE.h:102:9
pub const __putc_unlocked_body = @compileError("TODO postfix inc/dec expr"); // /usr/include/bits/types/struct_FILE.h:106:9
pub const JANET_OUT_OF_MEMORY = @compileError("unable to translate C expr: unexpected token 'do'"); // /usr/local/include/janet/janet.h:400:9
pub const HUGE_VAL = @compileError("unable to translate macro: undefined identifier `__builtin_huge_val`"); // /usr/include/math.h:48:10
pub const HUGE_VALL = @compileError("unable to translate macro: undefined identifier `__builtin_huge_vall`"); // /usr/include/math.h:60:11
pub const __SIMD_DECL = @compileError("unable to translate macro: undefined identifier `__DECL_SIMD_`"); // /usr/include/math.h:276:9
pub const __MATHCALL_VEC = @compileError("unable to translate C expr: unexpected token 'Identifier'"); // /usr/include/math.h:278:9
pub const __MATHDECL_VEC = @compileError("unable to translate C expr: unexpected token 'Identifier'"); // /usr/include/math.h:282:9
pub const __MATHDECL = @compileError("unable to translate macro: undefined identifier `__`"); // /usr/include/math.h:288:9
pub const __MATHDECLX = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/math.h:293:9
pub const __MATHDECL_1_IMPL = @compileError("unable to translate C expr: unexpected token 'extern'"); // /usr/include/math.h:296:9
pub const __MATHREDIR = @compileError("unable to translate C expr: unexpected token 'extern'"); // /usr/include/math.h:305:9
pub const __MATHCALL_NARROW_ARGS_1 = @compileError("unable to translate macro: undefined identifier `_Marg_`"); // /usr/include/math.h:550:9
pub const __MATHCALL_NARROW_ARGS_2 = @compileError("unable to translate macro: undefined identifier `_Marg_`"); // /usr/include/math.h:551:9
pub const __MATHCALL_NARROW_ARGS_3 = @compileError("unable to translate macro: undefined identifier `_Marg_`"); // /usr/include/math.h:552:9
pub const __MATHCALL_NARROW_NORMAL = @compileError("unable to translate macro: undefined identifier `_Mret_`"); // /usr/include/math.h:553:9
pub const __MATHCALL_NARROW_REDIR = @compileError("unable to translate macro: undefined identifier `_Mret_`"); // /usr/include/math.h:555:9
pub const __MATH_TG = @compileError("unable to translate macro: undefined identifier `f`"); // /usr/include/math.h:922:10
pub const fpclassify = @compileError("unable to translate macro: undefined identifier `__builtin_fpclassify`"); // /usr/include/math.h:967:11
pub const isfinite = @compileError("unable to translate macro: undefined identifier `__builtin_isfinite`"); // /usr/include/math.h:994:11
pub const isnormal = @compileError("unable to translate macro: undefined identifier `__builtin_isnormal`"); // /usr/include/math.h:1002:11
pub const isgreater = @compileError("unable to translate macro: undefined identifier `__builtin_isgreater`"); // /usr/include/math.h:1305:11
pub const isgreaterequal = @compileError("unable to translate macro: undefined identifier `__builtin_isgreaterequal`"); // /usr/include/math.h:1306:11
pub const isless = @compileError("unable to translate macro: undefined identifier `__builtin_isless`"); // /usr/include/math.h:1307:11
pub const islessequal = @compileError("unable to translate macro: undefined identifier `__builtin_islessequal`"); // /usr/include/math.h:1308:11
pub const islessgreater = @compileError("unable to translate macro: undefined identifier `__builtin_islessgreater`"); // /usr/include/math.h:1309:11
pub const isunordered = @compileError("unable to translate macro: undefined identifier `__builtin_isunordered`"); // /usr/include/math.h:1310:11
pub const janet_tuple_from_head = @compileError("unable to translate macro: undefined identifier `data`"); // /usr/local/include/janet/janet.h:1631:9
pub const janet_struct_from_head = @compileError("unable to translate macro: undefined identifier `gcobject`"); // /usr/local/include/janet/janet.h:1677:9
pub const janet_abstract_from_head = @compileError("unable to translate macro: undefined identifier `data`"); // /usr/local/include/janet/janet.h:1724:9
pub const JANET_CFUN = @compileError("unable to translate macro: undefined identifier `argc`"); // /usr/local/include/janet/janet.h:1869:9
pub const JANET_REG_END = @compileError("unable to translate C expr: unexpected token '{'"); // /usr/local/include/janet/janet.h:1872:9
pub const JANET_REG_ = @compileError("unable to translate C expr: unexpected token '{'"); // /usr/local/include/janet/janet.h:1875:9
pub const JANET_FN_ = @compileError("unable to translate macro: undefined identifier `argc`"); // /usr/local/include/janet/janet.h:1876:9
pub const JANET_REG_S = @compileError("unable to translate macro: undefined identifier `__FILE__`"); // /usr/local/include/janet/janet.h:1882:9
pub const JANET_FN_S = @compileError("unable to translate macro: undefined identifier `_sourceline_`"); // /usr/local/include/janet/janet.h:1883:9
pub const JANET_DEF_S = @compileError("unable to translate macro: undefined identifier `__FILE__`"); // /usr/local/include/janet/janet.h:1886:9
pub const JANET_REG_D = @compileError("unable to translate macro: undefined identifier `_docstring_`"); // /usr/local/include/janet/janet.h:1890:9
pub const JANET_FN_D = @compileError("unable to translate macro: undefined identifier `_docstring_`"); // /usr/local/include/janet/janet.h:1891:9
pub const JANET_REG_SD = @compileError("unable to translate macro: undefined identifier `_docstring_`"); // /usr/local/include/janet/janet.h:1898:9
pub const JANET_FN_SD = @compileError("unable to translate macro: undefined identifier `_sourceline_`"); // /usr/local/include/janet/janet.h:1899:9
pub const JANET_DEF_SD = @compileError("unable to translate macro: undefined identifier `__FILE__`"); // /usr/local/include/janet/janet.h:1903:9
pub const JANET_MODULE_ENTRY = @compileError("unable to translate macro: undefined identifier `_janet_mod_config`"); // /usr/local/include/janet/janet.h:1942:9
pub const janet_printf = @compileError("unable to translate C expr: expected ')' instead got '...'"); // /usr/local/include/janet/janet.h:1957:9
pub const janet_eprintf = @compileError("unable to translate C expr: expected ')' instead got '...'"); // /usr/local/include/janet/janet.h:1958:9
pub const __llvm__ = @as(c_int, 1);
pub const __clang__ = @as(c_int, 1);
pub const __clang_major__ = @as(c_int, 14);
pub const __clang_minor__ = @as(c_int, 0);
pub const __clang_patchlevel__ = @as(c_int, 6);
pub const __clang_version__ = "14.0.6 (git@github.com:ziglang/zig-bootstrap.git 73fba22053d7668fa2448a13b32c772f95c6e1c6)";
pub const __GNUC__ = @as(c_int, 4);
pub const __GNUC_MINOR__ = @as(c_int, 2);
pub const __GNUC_PATCHLEVEL__ = @as(c_int, 1);
pub const __GXX_ABI_VERSION = @as(c_int, 1002);
pub const __ATOMIC_RELAXED = @as(c_int, 0);
pub const __ATOMIC_CONSUME = @as(c_int, 1);
pub const __ATOMIC_ACQUIRE = @as(c_int, 2);
pub const __ATOMIC_RELEASE = @as(c_int, 3);
pub const __ATOMIC_ACQ_REL = @as(c_int, 4);
pub const __ATOMIC_SEQ_CST = @as(c_int, 5);
pub const __OPENCL_MEMORY_SCOPE_WORK_ITEM = @as(c_int, 0);
pub const __OPENCL_MEMORY_SCOPE_WORK_GROUP = @as(c_int, 1);
pub const __OPENCL_MEMORY_SCOPE_DEVICE = @as(c_int, 2);
pub const __OPENCL_MEMORY_SCOPE_ALL_SVM_DEVICES = @as(c_int, 3);
pub const __OPENCL_MEMORY_SCOPE_SUB_GROUP = @as(c_int, 4);
pub const __PRAGMA_REDEFINE_EXTNAME = @as(c_int, 1);
pub const __VERSION__ = "Clang 14.0.6 (git@github.com:ziglang/zig-bootstrap.git 73fba22053d7668fa2448a13b32c772f95c6e1c6)";
pub const __OBJC_BOOL_IS_BOOL = @as(c_int, 0);
pub const __CONSTANT_CFSTRINGS__ = @as(c_int, 1);
pub const __clang_literal_encoding__ = "UTF-8";
pub const __clang_wide_literal_encoding__ = "UTF-32";
pub const __ORDER_LITTLE_ENDIAN__ = @as(c_int, 1234);
pub const __ORDER_BIG_ENDIAN__ = @as(c_int, 4321);
pub const __ORDER_PDP_ENDIAN__ = @as(c_int, 3412);
pub const __BYTE_ORDER__ = __ORDER_LITTLE_ENDIAN__;
pub const __LITTLE_ENDIAN__ = @as(c_int, 1);
pub const _LP64 = @as(c_int, 1);
pub const __LP64__ = @as(c_int, 1);
pub const __CHAR_BIT__ = @as(c_int, 8);
pub const __BOOL_WIDTH__ = @as(c_int, 8);
pub const __SHRT_WIDTH__ = @as(c_int, 16);
pub const __INT_WIDTH__ = @as(c_int, 32);
pub const __LONG_WIDTH__ = @as(c_int, 64);
pub const __LLONG_WIDTH__ = @as(c_int, 64);
pub const __BITINT_MAXWIDTH__ = @as(c_int, 128);
pub const __SCHAR_MAX__ = @as(c_int, 127);
pub const __SHRT_MAX__ = @as(c_int, 32767);
pub const __INT_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __LONG_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __LONG_LONG_MAX__ = @as(c_longlong, 9223372036854775807);
pub const __WCHAR_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __WCHAR_WIDTH__ = @as(c_int, 32);
pub const __WINT_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const __WINT_WIDTH__ = @as(c_int, 32);
pub const __INTMAX_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INTMAX_WIDTH__ = @as(c_int, 64);
pub const __SIZE_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __SIZE_WIDTH__ = @as(c_int, 64);
pub const __UINTMAX_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __UINTMAX_WIDTH__ = @as(c_int, 64);
pub const __PTRDIFF_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __PTRDIFF_WIDTH__ = @as(c_int, 64);
pub const __INTPTR_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INTPTR_WIDTH__ = @as(c_int, 64);
pub const __UINTPTR_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __UINTPTR_WIDTH__ = @as(c_int, 64);
pub const __SIZEOF_DOUBLE__ = @as(c_int, 8);
pub const __SIZEOF_FLOAT__ = @as(c_int, 4);
pub const __SIZEOF_INT__ = @as(c_int, 4);
pub const __SIZEOF_LONG__ = @as(c_int, 8);
pub const __SIZEOF_LONG_DOUBLE__ = @as(c_int, 16);
pub const __SIZEOF_LONG_LONG__ = @as(c_int, 8);
pub const __SIZEOF_POINTER__ = @as(c_int, 8);
pub const __SIZEOF_SHORT__ = @as(c_int, 2);
pub const __SIZEOF_PTRDIFF_T__ = @as(c_int, 8);
pub const __SIZEOF_SIZE_T__ = @as(c_int, 8);
pub const __SIZEOF_WCHAR_T__ = @as(c_int, 4);
pub const __SIZEOF_WINT_T__ = @as(c_int, 4);
pub const __SIZEOF_INT128__ = @as(c_int, 16);
pub const __INTMAX_TYPE__ = c_long;
pub const __INTMAX_FMTd__ = "ld";
pub const __INTMAX_FMTi__ = "li";
pub const __UINTMAX_TYPE__ = c_ulong;
pub const __UINTMAX_FMTo__ = "lo";
pub const __UINTMAX_FMTu__ = "lu";
pub const __UINTMAX_FMTx__ = "lx";
pub const __UINTMAX_FMTX__ = "lX";
pub const __PTRDIFF_TYPE__ = c_long;
pub const __PTRDIFF_FMTd__ = "ld";
pub const __PTRDIFF_FMTi__ = "li";
pub const __INTPTR_TYPE__ = c_long;
pub const __INTPTR_FMTd__ = "ld";
pub const __INTPTR_FMTi__ = "li";
pub const __SIZE_TYPE__ = c_ulong;
pub const __SIZE_FMTo__ = "lo";
pub const __SIZE_FMTu__ = "lu";
pub const __SIZE_FMTx__ = "lx";
pub const __SIZE_FMTX__ = "lX";
pub const __WCHAR_TYPE__ = c_int;
pub const __WINT_TYPE__ = c_uint;
pub const __SIG_ATOMIC_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __SIG_ATOMIC_WIDTH__ = @as(c_int, 32);
pub const __CHAR16_TYPE__ = c_ushort;
pub const __CHAR32_TYPE__ = c_uint;
pub const __UINTPTR_TYPE__ = c_ulong;
pub const __UINTPTR_FMTo__ = "lo";
pub const __UINTPTR_FMTu__ = "lu";
pub const __UINTPTR_FMTx__ = "lx";
pub const __UINTPTR_FMTX__ = "lX";
pub const __FLT_DENORM_MIN__ = @as(f32, 1.40129846e-45);
pub const __FLT_HAS_DENORM__ = @as(c_int, 1);
pub const __FLT_DIG__ = @as(c_int, 6);
pub const __FLT_DECIMAL_DIG__ = @as(c_int, 9);
pub const __FLT_EPSILON__ = @as(f32, 1.19209290e-7);
pub const __FLT_HAS_INFINITY__ = @as(c_int, 1);
pub const __FLT_HAS_QUIET_NAN__ = @as(c_int, 1);
pub const __FLT_MANT_DIG__ = @as(c_int, 24);
pub const __FLT_MAX_10_EXP__ = @as(c_int, 38);
pub const __FLT_MAX_EXP__ = @as(c_int, 128);
pub const __FLT_MAX__ = @as(f32, 3.40282347e+38);
pub const __FLT_MIN_10_EXP__ = -@as(c_int, 37);
pub const __FLT_MIN_EXP__ = -@as(c_int, 125);
pub const __FLT_MIN__ = @as(f32, 1.17549435e-38);
pub const __DBL_DENORM_MIN__ = 4.9406564584124654e-324;
pub const __DBL_HAS_DENORM__ = @as(c_int, 1);
pub const __DBL_DIG__ = @as(c_int, 15);
pub const __DBL_DECIMAL_DIG__ = @as(c_int, 17);
pub const __DBL_EPSILON__ = 2.2204460492503131e-16;
pub const __DBL_HAS_INFINITY__ = @as(c_int, 1);
pub const __DBL_HAS_QUIET_NAN__ = @as(c_int, 1);
pub const __DBL_MANT_DIG__ = @as(c_int, 53);
pub const __DBL_MAX_10_EXP__ = @as(c_int, 308);
pub const __DBL_MAX_EXP__ = @as(c_int, 1024);
pub const __DBL_MAX__ = 1.7976931348623157e+308;
pub const __DBL_MIN_10_EXP__ = -@as(c_int, 307);
pub const __DBL_MIN_EXP__ = -@as(c_int, 1021);
pub const __DBL_MIN__ = 2.2250738585072014e-308;
pub const __LDBL_DENORM_MIN__ = @as(c_longdouble, 3.64519953188247460253e-4951);
pub const __LDBL_HAS_DENORM__ = @as(c_int, 1);
pub const __LDBL_DIG__ = @as(c_int, 18);
pub const __LDBL_DECIMAL_DIG__ = @as(c_int, 21);
pub const __LDBL_EPSILON__ = @as(c_longdouble, 1.08420217248550443401e-19);
pub const __LDBL_HAS_INFINITY__ = @as(c_int, 1);
pub const __LDBL_HAS_QUIET_NAN__ = @as(c_int, 1);
pub const __LDBL_MANT_DIG__ = @as(c_int, 64);
pub const __LDBL_MAX_10_EXP__ = @as(c_int, 4932);
pub const __LDBL_MAX_EXP__ = @as(c_int, 16384);
pub const __LDBL_MAX__ = @as(c_longdouble, 1.18973149535723176502e+4932);
pub const __LDBL_MIN_10_EXP__ = -@as(c_int, 4931);
pub const __LDBL_MIN_EXP__ = -@as(c_int, 16381);
pub const __LDBL_MIN__ = @as(c_longdouble, 3.36210314311209350626e-4932);
pub const __POINTER_WIDTH__ = @as(c_int, 64);
pub const __BIGGEST_ALIGNMENT__ = @as(c_int, 16);
pub const __WINT_UNSIGNED__ = @as(c_int, 1);
pub const __INT8_TYPE__ = i8;
pub const __INT8_FMTd__ = "hhd";
pub const __INT8_FMTi__ = "hhi";
pub const __INT8_C_SUFFIX__ = "";
pub const __INT16_TYPE__ = c_short;
pub const __INT16_FMTd__ = "hd";
pub const __INT16_FMTi__ = "hi";
pub const __INT16_C_SUFFIX__ = "";
pub const __INT32_TYPE__ = c_int;
pub const __INT32_FMTd__ = "d";
pub const __INT32_FMTi__ = "i";
pub const __INT32_C_SUFFIX__ = "";
pub const __INT64_TYPE__ = c_long;
pub const __INT64_FMTd__ = "ld";
pub const __INT64_FMTi__ = "li";
pub const __UINT8_TYPE__ = u8;
pub const __UINT8_FMTo__ = "hho";
pub const __UINT8_FMTu__ = "hhu";
pub const __UINT8_FMTx__ = "hhx";
pub const __UINT8_FMTX__ = "hhX";
pub const __UINT8_C_SUFFIX__ = "";
pub const __UINT8_MAX__ = @as(c_int, 255);
pub const __INT8_MAX__ = @as(c_int, 127);
pub const __UINT16_TYPE__ = c_ushort;
pub const __UINT16_FMTo__ = "ho";
pub const __UINT16_FMTu__ = "hu";
pub const __UINT16_FMTx__ = "hx";
pub const __UINT16_FMTX__ = "hX";
pub const __UINT16_C_SUFFIX__ = "";
pub const __UINT16_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal);
pub const __INT16_MAX__ = @as(c_int, 32767);
pub const __UINT32_TYPE__ = c_uint;
pub const __UINT32_FMTo__ = "o";
pub const __UINT32_FMTu__ = "u";
pub const __UINT32_FMTx__ = "x";
pub const __UINT32_FMTX__ = "X";
pub const __UINT32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const __INT32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __UINT64_TYPE__ = c_ulong;
pub const __UINT64_FMTo__ = "lo";
pub const __UINT64_FMTu__ = "lu";
pub const __UINT64_FMTx__ = "lx";
pub const __UINT64_FMTX__ = "lX";
pub const __UINT64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __INT64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INT_LEAST8_TYPE__ = i8;
pub const __INT_LEAST8_MAX__ = @as(c_int, 127);
pub const __INT_LEAST8_WIDTH__ = @as(c_int, 8);
pub const __INT_LEAST8_FMTd__ = "hhd";
pub const __INT_LEAST8_FMTi__ = "hhi";
pub const __UINT_LEAST8_TYPE__ = u8;
pub const __UINT_LEAST8_MAX__ = @as(c_int, 255);
pub const __UINT_LEAST8_FMTo__ = "hho";
pub const __UINT_LEAST8_FMTu__ = "hhu";
pub const __UINT_LEAST8_FMTx__ = "hhx";
pub const __UINT_LEAST8_FMTX__ = "hhX";
pub const __INT_LEAST16_TYPE__ = c_short;
pub const __INT_LEAST16_MAX__ = @as(c_int, 32767);
pub const __INT_LEAST16_WIDTH__ = @as(c_int, 16);
pub const __INT_LEAST16_FMTd__ = "hd";
pub const __INT_LEAST16_FMTi__ = "hi";
pub const __UINT_LEAST16_TYPE__ = c_ushort;
pub const __UINT_LEAST16_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal);
pub const __UINT_LEAST16_FMTo__ = "ho";
pub const __UINT_LEAST16_FMTu__ = "hu";
pub const __UINT_LEAST16_FMTx__ = "hx";
pub const __UINT_LEAST16_FMTX__ = "hX";
pub const __INT_LEAST32_TYPE__ = c_int;
pub const __INT_LEAST32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __INT_LEAST32_WIDTH__ = @as(c_int, 32);
pub const __INT_LEAST32_FMTd__ = "d";
pub const __INT_LEAST32_FMTi__ = "i";
pub const __UINT_LEAST32_TYPE__ = c_uint;
pub const __UINT_LEAST32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const __UINT_LEAST32_FMTo__ = "o";
pub const __UINT_LEAST32_FMTu__ = "u";
pub const __UINT_LEAST32_FMTx__ = "x";
pub const __UINT_LEAST32_FMTX__ = "X";
pub const __INT_LEAST64_TYPE__ = c_long;
pub const __INT_LEAST64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INT_LEAST64_WIDTH__ = @as(c_int, 64);
pub const __INT_LEAST64_FMTd__ = "ld";
pub const __INT_LEAST64_FMTi__ = "li";
pub const __UINT_LEAST64_TYPE__ = c_ulong;
pub const __UINT_LEAST64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __UINT_LEAST64_FMTo__ = "lo";
pub const __UINT_LEAST64_FMTu__ = "lu";
pub const __UINT_LEAST64_FMTx__ = "lx";
pub const __UINT_LEAST64_FMTX__ = "lX";
pub const __INT_FAST8_TYPE__ = i8;
pub const __INT_FAST8_MAX__ = @as(c_int, 127);
pub const __INT_FAST8_WIDTH__ = @as(c_int, 8);
pub const __INT_FAST8_FMTd__ = "hhd";
pub const __INT_FAST8_FMTi__ = "hhi";
pub const __UINT_FAST8_TYPE__ = u8;
pub const __UINT_FAST8_MAX__ = @as(c_int, 255);
pub const __UINT_FAST8_FMTo__ = "hho";
pub const __UINT_FAST8_FMTu__ = "hhu";
pub const __UINT_FAST8_FMTx__ = "hhx";
pub const __UINT_FAST8_FMTX__ = "hhX";
pub const __INT_FAST16_TYPE__ = c_short;
pub const __INT_FAST16_MAX__ = @as(c_int, 32767);
pub const __INT_FAST16_WIDTH__ = @as(c_int, 16);
pub const __INT_FAST16_FMTd__ = "hd";
pub const __INT_FAST16_FMTi__ = "hi";
pub const __UINT_FAST16_TYPE__ = c_ushort;
pub const __UINT_FAST16_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal);
pub const __UINT_FAST16_FMTo__ = "ho";
pub const __UINT_FAST16_FMTu__ = "hu";
pub const __UINT_FAST16_FMTx__ = "hx";
pub const __UINT_FAST16_FMTX__ = "hX";
pub const __INT_FAST32_TYPE__ = c_int;
pub const __INT_FAST32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __INT_FAST32_WIDTH__ = @as(c_int, 32);
pub const __INT_FAST32_FMTd__ = "d";
pub const __INT_FAST32_FMTi__ = "i";
pub const __UINT_FAST32_TYPE__ = c_uint;
pub const __UINT_FAST32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const __UINT_FAST32_FMTo__ = "o";
pub const __UINT_FAST32_FMTu__ = "u";
pub const __UINT_FAST32_FMTx__ = "x";
pub const __UINT_FAST32_FMTX__ = "X";
pub const __INT_FAST64_TYPE__ = c_long;
pub const __INT_FAST64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INT_FAST64_WIDTH__ = @as(c_int, 64);
pub const __INT_FAST64_FMTd__ = "ld";
pub const __INT_FAST64_FMTi__ = "li";
pub const __UINT_FAST64_TYPE__ = c_ulong;
pub const __UINT_FAST64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __UINT_FAST64_FMTo__ = "lo";
pub const __UINT_FAST64_FMTu__ = "lu";
pub const __UINT_FAST64_FMTx__ = "lx";
pub const __UINT_FAST64_FMTX__ = "lX";
pub const __USER_LABEL_PREFIX__ = "";
pub const __FINITE_MATH_ONLY__ = @as(c_int, 0);
pub const __GNUC_STDC_INLINE__ = @as(c_int, 1);
pub const __GCC_ATOMIC_TEST_AND_SET_TRUEVAL = @as(c_int, 1);
pub const __CLANG_ATOMIC_BOOL_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_CHAR_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_CHAR16_T_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_CHAR32_T_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_WCHAR_T_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_SHORT_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_INT_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_LONG_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_LLONG_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_POINTER_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_BOOL_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_CHAR_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_CHAR16_T_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_CHAR32_T_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_WCHAR_T_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_SHORT_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_INT_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_LONG_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_LLONG_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_POINTER_LOCK_FREE = @as(c_int, 2);
pub const __NO_INLINE__ = @as(c_int, 1);
pub const __FLT_EVAL_METHOD__ = @as(c_int, 0);
pub const __FLT_RADIX__ = @as(c_int, 2);
pub const __DECIMAL_DIG__ = __LDBL_DECIMAL_DIG__;
pub const __GCC_ASM_FLAG_OUTPUTS__ = @as(c_int, 1);
pub const __code_model_small__ = @as(c_int, 1);
pub const __amd64__ = @as(c_int, 1);
pub const __amd64 = @as(c_int, 1);
pub const __x86_64 = @as(c_int, 1);
pub const __x86_64__ = @as(c_int, 1);
pub const __SEG_GS = @as(c_int, 1);
pub const __SEG_FS = @as(c_int, 1);
pub const __k8 = @as(c_int, 1);
pub const __k8__ = @as(c_int, 1);
pub const __tune_k8__ = @as(c_int, 1);
pub const __REGISTER_PREFIX__ = "";
pub const __NO_MATH_INLINES = @as(c_int, 1);
pub const __AES__ = @as(c_int, 1);
pub const __PCLMUL__ = @as(c_int, 1);
pub const __LAHF_SAHF__ = @as(c_int, 1);
pub const __LZCNT__ = @as(c_int, 1);
pub const __RDRND__ = @as(c_int, 1);
pub const __FSGSBASE__ = @as(c_int, 1);
pub const __BMI__ = @as(c_int, 1);
pub const __BMI2__ = @as(c_int, 1);
pub const __POPCNT__ = @as(c_int, 1);
pub const __PRFCHW__ = @as(c_int, 1);
pub const __RDSEED__ = @as(c_int, 1);
pub const __ADX__ = @as(c_int, 1);
pub const __MOVBE__ = @as(c_int, 1);
pub const __FMA__ = @as(c_int, 1);
pub const __F16C__ = @as(c_int, 1);
pub const __FXSR__ = @as(c_int, 1);
pub const __XSAVE__ = @as(c_int, 1);
pub const __XSAVEOPT__ = @as(c_int, 1);
pub const __XSAVEC__ = @as(c_int, 1);
pub const __XSAVES__ = @as(c_int, 1);
pub const __PKU__ = @as(c_int, 1);
pub const __CLFLUSHOPT__ = @as(c_int, 1);
pub const __SGX__ = @as(c_int, 1);
pub const __INVPCID__ = @as(c_int, 1);
pub const __AVX2__ = @as(c_int, 1);
pub const __AVX__ = @as(c_int, 1);
pub const __SSE4_2__ = @as(c_int, 1);
pub const __SSE4_1__ = @as(c_int, 1);
pub const __SSSE3__ = @as(c_int, 1);
pub const __SSE3__ = @as(c_int, 1);
pub const __SSE2__ = @as(c_int, 1);
pub const __SSE2_MATH__ = @as(c_int, 1);
pub const __SSE__ = @as(c_int, 1);
pub const __SSE_MATH__ = @as(c_int, 1);
pub const __MMX__ = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = @as(c_int, 1);
pub const __SIZEOF_FLOAT128__ = @as(c_int, 16);
pub const unix = @as(c_int, 1);
pub const __unix = @as(c_int, 1);
pub const __unix__ = @as(c_int, 1);
pub const linux = @as(c_int, 1);
pub const __linux = @as(c_int, 1);
pub const __linux__ = @as(c_int, 1);
pub const __ELF__ = @as(c_int, 1);
pub const __gnu_linux__ = @as(c_int, 1);
pub const __FLOAT128__ = @as(c_int, 1);
pub const __STDC__ = @as(c_int, 1);
pub const __STDC_HOSTED__ = @as(c_int, 1);
pub const __STDC_VERSION__ = @as(c_long, 201710);
pub const __STDC_UTF_16__ = @as(c_int, 1);
pub const __STDC_UTF_32__ = @as(c_int, 1);
pub const _DEBUG = @as(c_int, 1);
pub const __GCC_HAVE_DWARF2_CFI_ASM = @as(c_int, 1);
pub const JANETCONF_H = "";
pub const JANET_VERSION_MAJOR = @as(c_int, 1);
pub const JANET_VERSION_MINOR = @as(c_int, 25);
pub const JANET_VERSION_PATCH = @as(c_int, 0);
pub const JANET_VERSION_EXTRA = "-dev";
pub const JANET_VERSION = "1.25.0-dev";
pub const JANET_H_defined = "";
pub const JANET_BUILD = "local";
pub const JANET_LINUX = @as(c_int, 1);
pub const JANET_POSIX = @as(c_int, 1);
pub const JANET_64 = @as(c_int, 1);
pub const JANET_LITTLE_ENDIAN = @as(c_int, 1);
pub const JANET_INTMAX_DOUBLE = 9007199254740992.0;
pub const JANET_INTMIN_DOUBLE = -9007199254740992.0;
pub const JANET_INTMAX_INT64 = @import("std").zig.c_translation.promoteIntLiteral(c_int, 9007199254740992, .decimal);
pub const JANET_INTMIN_INT64 = -@import("std").zig.c_translation.promoteIntLiteral(c_int, 9007199254740992, .decimal);
pub const JANET_DYNAMIC_MODULES = "";
pub const JANET_FFI = "";
pub const JANET_ASSEMBLER = "";
pub const JANET_PEG = "";
pub const JANET_EV = "";
pub const JANET_NET = "";
pub const JANET_INT_TYPES = "";
pub const JANET_EV_EPOLL = "";
pub const JANET_RECURSION_GUARD = @as(c_int, 1024);
pub const JANET_MAX_PROTO_DEPTH = @as(c_int, 200);
pub const JANET_MAX_MACRO_EXPAND = @as(c_int, 200);
pub const JANET_STACK_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x7fffffff, .hexadecimal);
pub const JANET_NANBOX_64 = "";
pub const JANET_NANBOX_BIT = @as(c_int, 0x1);
pub const JANET_SINGLE_THREADED_BIT = @as(c_int, 0);
pub const JANET_CURRENT_CONFIG_BITS = JANET_SINGLE_THREADED_BIT | JANET_NANBOX_BIT;
pub inline fn janet_config_current() JanetBuildConfig {
    return @import("std").mem.zeroInit(JanetBuildConfig, .{ JANET_VERSION_MAJOR, JANET_VERSION_MINOR, JANET_VERSION_PATCH, JANET_CURRENT_CONFIG_BITS });
}
pub const __GLIBC_INTERNAL_STARTING_HEADER_IMPLEMENTATION = "";
pub const _FEATURES_H = @as(c_int, 1);
pub const __KERNEL_STRICT_NAMES = "";
pub inline fn __GNUC_PREREQ(maj: anytype, min: anytype) @TypeOf(((__GNUC__ << @as(c_int, 16)) + __GNUC_MINOR__) >= ((maj << @as(c_int, 16)) + min)) {
    return ((__GNUC__ << @as(c_int, 16)) + __GNUC_MINOR__) >= ((maj << @as(c_int, 16)) + min);
}
pub inline fn __glibc_clang_prereq(maj: anytype, min: anytype) @TypeOf(((__clang_major__ << @as(c_int, 16)) + __clang_minor__) >= ((maj << @as(c_int, 16)) + min)) {
    return ((__clang_major__ << @as(c_int, 16)) + __clang_minor__) >= ((maj << @as(c_int, 16)) + min);
}
pub const _DEFAULT_SOURCE = @as(c_int, 1);
pub const __GLIBC_USE_ISOC2X = @as(c_int, 0);
pub const __USE_ISOC11 = @as(c_int, 1);
pub const __USE_ISOC99 = @as(c_int, 1);
pub const __USE_ISOC95 = @as(c_int, 1);
pub const __USE_POSIX_IMPLICITLY = @as(c_int, 1);
pub const _POSIX_SOURCE = @as(c_int, 1);
pub const _POSIX_C_SOURCE = @as(c_long, 200809);
pub const __USE_POSIX = @as(c_int, 1);
pub const __USE_POSIX2 = @as(c_int, 1);
pub const __USE_POSIX199309 = @as(c_int, 1);
pub const __USE_POSIX199506 = @as(c_int, 1);
pub const __USE_XOPEN2K = @as(c_int, 1);
pub const __USE_XOPEN2K8 = @as(c_int, 1);
pub const _ATFILE_SOURCE = @as(c_int, 1);
pub const __WORDSIZE = @as(c_int, 64);
pub const __WORDSIZE_TIME64_COMPAT32 = @as(c_int, 1);
pub const __SYSCALL_WORDSIZE = @as(c_int, 64);
pub const __TIMESIZE = __WORDSIZE;
pub const __USE_MISC = @as(c_int, 1);
pub const __USE_ATFILE = @as(c_int, 1);
pub const __USE_FORTIFY_LEVEL = @as(c_int, 0);
pub const __GLIBC_USE_DEPRECATED_GETS = @as(c_int, 0);
pub const __GLIBC_USE_DEPRECATED_SCANF = @as(c_int, 0);
pub const _STDC_PREDEF_H = @as(c_int, 1);
pub const __STDC_IEC_559__ = @as(c_int, 1);
pub const __STDC_IEC_60559_BFP__ = @as(c_long, 201404);
pub const __STDC_IEC_559_COMPLEX__ = @as(c_int, 1);
pub const __STDC_IEC_60559_COMPLEX__ = @as(c_long, 201404);
pub const __STDC_ISO_10646__ = @as(c_long, 201706);
pub const __GNU_LIBRARY__ = @as(c_int, 6);
pub const __GLIBC__ = @as(c_int, 2);
pub const __GLIBC_MINOR__ = @as(c_int, 36);
pub inline fn __GLIBC_PREREQ(maj: anytype, min: anytype) @TypeOf(((__GLIBC__ << @as(c_int, 16)) + __GLIBC_MINOR__) >= ((maj << @as(c_int, 16)) + min)) {
    return ((__GLIBC__ << @as(c_int, 16)) + __GLIBC_MINOR__) >= ((maj << @as(c_int, 16)) + min);
}
pub const _SYS_CDEFS_H = @as(c_int, 1);
pub inline fn __glibc_has_builtin(name: anytype) @TypeOf(__has_builtin(name)) {
    return __has_builtin(name);
}
pub const __LEAF = "";
pub const __LEAF_ATTR = "";
pub inline fn __P(args: anytype) @TypeOf(args) {
    return args;
}
pub inline fn __PMT(args: anytype) @TypeOf(args) {
    return args;
}
pub const __ptr_t = ?*anyopaque;
pub const __BEGIN_DECLS = "";
pub const __END_DECLS = "";
pub inline fn __bos(ptr: anytype) @TypeOf(__builtin_object_size(ptr, __USE_FORTIFY_LEVEL > @as(c_int, 1))) {
    return __builtin_object_size(ptr, __USE_FORTIFY_LEVEL > @as(c_int, 1));
}
pub inline fn __bos0(ptr: anytype) @TypeOf(__builtin_object_size(ptr, @as(c_int, 0))) {
    return __builtin_object_size(ptr, @as(c_int, 0));
}
pub inline fn __glibc_objsize0(__o: anytype) @TypeOf(__bos0(__o)) {
    return __bos0(__o);
}
pub inline fn __glibc_objsize(__o: anytype) @TypeOf(__bos(__o)) {
    return __bos(__o);
}
pub inline fn __glibc_safe_len_cond(__l: anytype, __s: anytype, __osz: anytype) @TypeOf(__l <= (__osz / __s)) {
    return __l <= (__osz / __s);
}
pub inline fn __glibc_safe_or_unknown_len(__l: anytype, __s: anytype, __osz: anytype) @TypeOf(((__builtin_constant_p(__osz) != 0) and (__osz == (__SIZE_TYPE__ - @as(c_int, 1)))) or (((__glibc_unsigned_or_positive(__l) != 0) and (__builtin_constant_p(__glibc_safe_len_cond(__SIZE_TYPE__(__l), __s, __osz)) != 0)) and (__glibc_safe_len_cond(__SIZE_TYPE__(__l), __s, __osz) != 0))) {
    return ((__builtin_constant_p(__osz) != 0) and (__osz == (__SIZE_TYPE__ - @as(c_int, 1)))) or (((__glibc_unsigned_or_positive(__l) != 0) and (__builtin_constant_p(__glibc_safe_len_cond(__SIZE_TYPE__(__l), __s, __osz)) != 0)) and (__glibc_safe_len_cond(__SIZE_TYPE__(__l), __s, __osz) != 0));
}
pub inline fn __glibc_unsafe_len(__l: anytype, __s: anytype, __osz: anytype) @TypeOf(((__glibc_unsigned_or_positive(__l) != 0) and (__builtin_constant_p(__glibc_safe_len_cond(__SIZE_TYPE__(__l), __s, __osz)) != 0)) and !(__glibc_safe_len_cond(__SIZE_TYPE__(__l), __s, __osz) != 0)) {
    return ((__glibc_unsigned_or_positive(__l) != 0) and (__builtin_constant_p(__glibc_safe_len_cond(__SIZE_TYPE__(__l), __s, __osz)) != 0)) and !(__glibc_safe_len_cond(__SIZE_TYPE__(__l), __s, __osz) != 0);
}
pub const __glibc_c99_flexarr_available = @as(c_int, 1);
pub inline fn __ASMNAME(cname: anytype) @TypeOf(__ASMNAME2(__USER_LABEL_PREFIX__, cname)) {
    return __ASMNAME2(__USER_LABEL_PREFIX__, cname);
}
pub inline fn __nonnull(params: anytype) @TypeOf(__attribute_nonnull__(params)) {
    return __attribute_nonnull__(params);
}
pub const __wur = "";
pub const __fortify_function = __extern_always_inline ++ __attribute_artificial__;
pub inline fn __glibc_unlikely(cond: anytype) @TypeOf(__builtin_expect(cond, @as(c_int, 0))) {
    return __builtin_expect(cond, @as(c_int, 0));
}
pub inline fn __glibc_likely(cond: anytype) @TypeOf(__builtin_expect(cond, @as(c_int, 1))) {
    return __builtin_expect(cond, @as(c_int, 1));
}
pub const __attribute_nonstring__ = "";
pub const __LDOUBLE_REDIRECTS_TO_FLOAT128_ABI = @as(c_int, 0);
pub inline fn __LDBL_REDIR1(name: anytype, proto: anytype, alias: anytype) @TypeOf(name ++ proto) {
    _ = @TypeOf(alias);
    return name ++ proto;
}
pub inline fn __LDBL_REDIR(name: anytype, proto: anytype) @TypeOf(name ++ proto) {
    return name ++ proto;
}
pub inline fn __LDBL_REDIR1_NTH(name: anytype, proto: anytype, alias: anytype) @TypeOf(name ++ proto ++ __THROW) {
    _ = @TypeOf(alias);
    return name ++ proto ++ __THROW;
}
pub inline fn __LDBL_REDIR_NTH(name: anytype, proto: anytype) @TypeOf(name ++ proto ++ __THROW) {
    return name ++ proto ++ __THROW;
}
pub inline fn __REDIRECT_LDBL(name: anytype, proto: anytype, alias: anytype) @TypeOf(__REDIRECT(name, proto, alias)) {
    return __REDIRECT(name, proto, alias);
}
pub inline fn __REDIRECT_NTH_LDBL(name: anytype, proto: anytype, alias: anytype) @TypeOf(__REDIRECT_NTH(name, proto, alias)) {
    return __REDIRECT_NTH(name, proto, alias);
}
pub const __HAVE_GENERIC_SELECTION = @as(c_int, 1);
pub const __attr_dealloc_free = "";
pub const __stub___compat_bdflush = "";
pub const __stub_chflags = "";
pub const __stub_fchflags = "";
pub const __stub_gtty = "";
pub const __stub_revoke = "";
pub const __stub_setlogin = "";
pub const __stub_sigreturn = "";
pub const __stub_stty = "";
pub const __GLIBC_USE_LIB_EXT2 = @as(c_int, 0);
pub const __GLIBC_USE_IEC_60559_BFP_EXT = @as(c_int, 0);
pub const __GLIBC_USE_IEC_60559_BFP_EXT_C2X = @as(c_int, 0);
pub const __GLIBC_USE_IEC_60559_EXT = @as(c_int, 0);
pub const __GLIBC_USE_IEC_60559_FUNCS_EXT = @as(c_int, 0);
pub const __GLIBC_USE_IEC_60559_FUNCS_EXT_C2X = @as(c_int, 0);
pub const __GLIBC_USE_IEC_60559_TYPES_EXT = @as(c_int, 0);
pub const __need_size_t = "";
pub const __need_wchar_t = "";
pub const __need_NULL = "";
pub const _SIZE_T = "";
pub const _WCHAR_T = "";
pub const NULL = @import("std").zig.c_translation.cast(?*anyopaque, @as(c_int, 0));
pub const _STDLIB_H = @as(c_int, 1);
pub const WNOHANG = @as(c_int, 1);
pub const WUNTRACED = @as(c_int, 2);
pub const WSTOPPED = @as(c_int, 2);
pub const WEXITED = @as(c_int, 4);
pub const WCONTINUED = @as(c_int, 8);
pub const WNOWAIT = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x01000000, .hexadecimal);
pub const __WNOTHREAD = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x20000000, .hexadecimal);
pub const __WALL = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x40000000, .hexadecimal);
pub const __WCLONE = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x80000000, .hexadecimal);
pub inline fn __WEXITSTATUS(status: anytype) @TypeOf((status & @import("std").zig.c_translation.promoteIntLiteral(c_int, 0xff00, .hexadecimal)) >> @as(c_int, 8)) {
    return (status & @import("std").zig.c_translation.promoteIntLiteral(c_int, 0xff00, .hexadecimal)) >> @as(c_int, 8);
}
pub inline fn __WTERMSIG(status: anytype) @TypeOf(status & @as(c_int, 0x7f)) {
    return status & @as(c_int, 0x7f);
}
pub inline fn __WSTOPSIG(status: anytype) @TypeOf(__WEXITSTATUS(status)) {
    return __WEXITSTATUS(status);
}
pub inline fn __WIFEXITED(status: anytype) @TypeOf(__WTERMSIG(status) == @as(c_int, 0)) {
    return __WTERMSIG(status) == @as(c_int, 0);
}
pub inline fn __WIFSIGNALED(status: anytype) @TypeOf((@import("std").zig.c_translation.cast(i8, (status & @as(c_int, 0x7f)) + @as(c_int, 1)) >> @as(c_int, 1)) > @as(c_int, 0)) {
    return (@import("std").zig.c_translation.cast(i8, (status & @as(c_int, 0x7f)) + @as(c_int, 1)) >> @as(c_int, 1)) > @as(c_int, 0);
}
pub inline fn __WIFSTOPPED(status: anytype) @TypeOf((status & @as(c_int, 0xff)) == @as(c_int, 0x7f)) {
    return (status & @as(c_int, 0xff)) == @as(c_int, 0x7f);
}
pub inline fn __WIFCONTINUED(status: anytype) @TypeOf(status == __W_CONTINUED) {
    return status == __W_CONTINUED;
}
pub inline fn __WCOREDUMP(status: anytype) @TypeOf(status & __WCOREFLAG) {
    return status & __WCOREFLAG;
}
pub inline fn __W_EXITCODE(ret: anytype, sig: anytype) @TypeOf((ret << @as(c_int, 8)) | sig) {
    return (ret << @as(c_int, 8)) | sig;
}
pub inline fn __W_STOPCODE(sig: anytype) @TypeOf((sig << @as(c_int, 8)) | @as(c_int, 0x7f)) {
    return (sig << @as(c_int, 8)) | @as(c_int, 0x7f);
}
pub const __W_CONTINUED = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0xffff, .hexadecimal);
pub const __WCOREFLAG = @as(c_int, 0x80);
pub inline fn WEXITSTATUS(status: anytype) @TypeOf(__WEXITSTATUS(status)) {
    return __WEXITSTATUS(status);
}
pub inline fn WTERMSIG(status: anytype) @TypeOf(__WTERMSIG(status)) {
    return __WTERMSIG(status);
}
pub inline fn WSTOPSIG(status: anytype) @TypeOf(__WSTOPSIG(status)) {
    return __WSTOPSIG(status);
}
pub inline fn WIFEXITED(status: anytype) @TypeOf(__WIFEXITED(status)) {
    return __WIFEXITED(status);
}
pub inline fn WIFSIGNALED(status: anytype) @TypeOf(__WIFSIGNALED(status)) {
    return __WIFSIGNALED(status);
}
pub inline fn WIFSTOPPED(status: anytype) @TypeOf(__WIFSTOPPED(status)) {
    return __WIFSTOPPED(status);
}
pub inline fn WIFCONTINUED(status: anytype) @TypeOf(__WIFCONTINUED(status)) {
    return __WIFCONTINUED(status);
}
pub const _BITS_FLOATN_H = "";
pub const __HAVE_FLOAT128 = @as(c_int, 0);
pub const __HAVE_DISTINCT_FLOAT128 = @as(c_int, 0);
pub const __HAVE_FLOAT64X = @as(c_int, 1);
pub const __HAVE_FLOAT64X_LONG_DOUBLE = @as(c_int, 1);
pub const _BITS_FLOATN_COMMON_H = "";
pub const __HAVE_FLOAT16 = @as(c_int, 0);
pub const __HAVE_FLOAT32 = @as(c_int, 1);
pub const __HAVE_FLOAT64 = @as(c_int, 1);
pub const __HAVE_FLOAT32X = @as(c_int, 1);
pub const __HAVE_FLOAT128X = @as(c_int, 0);
pub const __HAVE_DISTINCT_FLOAT16 = __HAVE_FLOAT16;
pub const __HAVE_DISTINCT_FLOAT32 = @as(c_int, 0);
pub const __HAVE_DISTINCT_FLOAT64 = @as(c_int, 0);
pub const __HAVE_DISTINCT_FLOAT32X = @as(c_int, 0);
pub const __HAVE_DISTINCT_FLOAT64X = @as(c_int, 0);
pub const __HAVE_DISTINCT_FLOAT128X = __HAVE_FLOAT128X;
pub const __HAVE_FLOAT128_UNLIKE_LDBL = (__HAVE_DISTINCT_FLOAT128 != 0) and (__LDBL_MANT_DIG__ != @as(c_int, 113));
pub const __HAVE_FLOATN_NOT_TYPEDEF = @as(c_int, 0);
pub const __f32 = @import("std").zig.c_translation.Macros.F_SUFFIX;
pub inline fn __f64(x: anytype) @TypeOf(x) {
    return x;
}
pub inline fn __f32x(x: anytype) @TypeOf(x) {
    return x;
}
pub const __f64x = @import("std").zig.c_translation.Macros.L_SUFFIX;
pub inline fn __builtin_huge_valf32() @TypeOf(__builtin_huge_valf()) {
    return __builtin_huge_valf();
}
pub inline fn __builtin_inff32() @TypeOf(__builtin_inff()) {
    return __builtin_inff();
}
pub inline fn __builtin_nanf32(x: anytype) @TypeOf(__builtin_nanf(x)) {
    return __builtin_nanf(x);
}
pub const __ldiv_t_defined = @as(c_int, 1);
pub const __lldiv_t_defined = @as(c_int, 1);
pub const RAND_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const EXIT_FAILURE = @as(c_int, 1);
pub const EXIT_SUCCESS = @as(c_int, 0);
pub const MB_CUR_MAX = __ctype_get_mb_cur_max();
pub const _SYS_TYPES_H = @as(c_int, 1);
pub const _BITS_TYPES_H = @as(c_int, 1);
pub const __S16_TYPE = c_short;
pub const __U16_TYPE = c_ushort;
pub const __S32_TYPE = c_int;
pub const __U32_TYPE = c_uint;
pub const __SLONGWORD_TYPE = c_long;
pub const __ULONGWORD_TYPE = c_ulong;
pub const __SQUAD_TYPE = c_long;
pub const __UQUAD_TYPE = c_ulong;
pub const __SWORD_TYPE = c_long;
pub const __UWORD_TYPE = c_ulong;
pub const __SLONG32_TYPE = c_int;
pub const __ULONG32_TYPE = c_uint;
pub const __S64_TYPE = c_long;
pub const __U64_TYPE = c_ulong;
pub const _BITS_TYPESIZES_H = @as(c_int, 1);
pub const __SYSCALL_SLONG_TYPE = __SLONGWORD_TYPE;
pub const __SYSCALL_ULONG_TYPE = __ULONGWORD_TYPE;
pub const __DEV_T_TYPE = __UQUAD_TYPE;
pub const __UID_T_TYPE = __U32_TYPE;
pub const __GID_T_TYPE = __U32_TYPE;
pub const __INO_T_TYPE = __SYSCALL_ULONG_TYPE;
pub const __INO64_T_TYPE = __UQUAD_TYPE;
pub const __MODE_T_TYPE = __U32_TYPE;
pub const __NLINK_T_TYPE = __SYSCALL_ULONG_TYPE;
pub const __FSWORD_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __OFF_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __OFF64_T_TYPE = __SQUAD_TYPE;
pub const __PID_T_TYPE = __S32_TYPE;
pub const __RLIM_T_TYPE = __SYSCALL_ULONG_TYPE;
pub const __RLIM64_T_TYPE = __UQUAD_TYPE;
pub const __BLKCNT_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __BLKCNT64_T_TYPE = __SQUAD_TYPE;
pub const __FSBLKCNT_T_TYPE = __SYSCALL_ULONG_TYPE;
pub const __FSBLKCNT64_T_TYPE = __UQUAD_TYPE;
pub const __FSFILCNT_T_TYPE = __SYSCALL_ULONG_TYPE;
pub const __FSFILCNT64_T_TYPE = __UQUAD_TYPE;
pub const __ID_T_TYPE = __U32_TYPE;
pub const __CLOCK_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __TIME_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __USECONDS_T_TYPE = __U32_TYPE;
pub const __SUSECONDS_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __SUSECONDS64_T_TYPE = __SQUAD_TYPE;
pub const __DADDR_T_TYPE = __S32_TYPE;
pub const __KEY_T_TYPE = __S32_TYPE;
pub const __CLOCKID_T_TYPE = __S32_TYPE;
pub const __TIMER_T_TYPE = ?*anyopaque;
pub const __BLKSIZE_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __SSIZE_T_TYPE = __SWORD_TYPE;
pub const __CPU_MASK_TYPE = __SYSCALL_ULONG_TYPE;
pub const __OFF_T_MATCHES_OFF64_T = @as(c_int, 1);
pub const __INO_T_MATCHES_INO64_T = @as(c_int, 1);
pub const __RLIM_T_MATCHES_RLIM64_T = @as(c_int, 1);
pub const __STATFS_MATCHES_STATFS64 = @as(c_int, 1);
pub const __KERNEL_OLD_TIMEVAL_MATCHES_TIMEVAL64 = @as(c_int, 1);
pub const __FD_SETSIZE = @as(c_int, 1024);
pub const _BITS_TIME64_H = @as(c_int, 1);
pub const __TIME64_T_TYPE = __TIME_T_TYPE;
pub const __u_char_defined = "";
pub const __ino_t_defined = "";
pub const __dev_t_defined = "";
pub const __gid_t_defined = "";
pub const __mode_t_defined = "";
pub const __nlink_t_defined = "";
pub const __uid_t_defined = "";
pub const __off_t_defined = "";
pub const __pid_t_defined = "";
pub const __id_t_defined = "";
pub const __ssize_t_defined = "";
pub const __daddr_t_defined = "";
pub const __key_t_defined = "";
pub const __clock_t_defined = @as(c_int, 1);
pub const __clockid_t_defined = @as(c_int, 1);
pub const __time_t_defined = @as(c_int, 1);
pub const __timer_t_defined = @as(c_int, 1);
pub const _BITS_STDINT_INTN_H = @as(c_int, 1);
pub const __BIT_TYPES_DEFINED__ = @as(c_int, 1);
pub const _ENDIAN_H = @as(c_int, 1);
pub const _BITS_ENDIAN_H = @as(c_int, 1);
pub const __LITTLE_ENDIAN = @as(c_int, 1234);
pub const __BIG_ENDIAN = @as(c_int, 4321);
pub const __PDP_ENDIAN = @as(c_int, 3412);
pub const _BITS_ENDIANNESS_H = @as(c_int, 1);
pub const __BYTE_ORDER = __LITTLE_ENDIAN;
pub const __FLOAT_WORD_ORDER = __BYTE_ORDER;
pub inline fn __LONG_LONG_PAIR(HI: anytype, LO: anytype) @TypeOf(HI) {
    return blk: {
        _ = @TypeOf(LO);
        break :blk HI;
    };
}
pub const LITTLE_ENDIAN = __LITTLE_ENDIAN;
pub const BIG_ENDIAN = __BIG_ENDIAN;
pub const PDP_ENDIAN = __PDP_ENDIAN;
pub const BYTE_ORDER = __BYTE_ORDER;
pub const _BITS_BYTESWAP_H = @as(c_int, 1);
pub inline fn __bswap_constant_16(x: anytype) __uint16_t {
    return @import("std").zig.c_translation.cast(__uint16_t, ((x >> @as(c_int, 8)) & @as(c_int, 0xff)) | ((x & @as(c_int, 0xff)) << @as(c_int, 8)));
}
pub inline fn __bswap_constant_32(x: anytype) @TypeOf(((((x & @import("std").zig.c_translation.promoteIntLiteral(c_uint, 0xff000000, .hexadecimal)) >> @as(c_int, 24)) | ((x & @import("std").zig.c_translation.promoteIntLiteral(c_uint, 0x00ff0000, .hexadecimal)) >> @as(c_int, 8))) | ((x & @as(c_uint, 0x0000ff00)) << @as(c_int, 8))) | ((x & @as(c_uint, 0x000000ff)) << @as(c_int, 24))) {
    return ((((x & @import("std").zig.c_translation.promoteIntLiteral(c_uint, 0xff000000, .hexadecimal)) >> @as(c_int, 24)) | ((x & @import("std").zig.c_translation.promoteIntLiteral(c_uint, 0x00ff0000, .hexadecimal)) >> @as(c_int, 8))) | ((x & @as(c_uint, 0x0000ff00)) << @as(c_int, 8))) | ((x & @as(c_uint, 0x000000ff)) << @as(c_int, 24));
}
pub inline fn __bswap_constant_64(x: anytype) @TypeOf(((((((((x & @as(c_ulonglong, 0xff00000000000000)) >> @as(c_int, 56)) | ((x & @as(c_ulonglong, 0x00ff000000000000)) >> @as(c_int, 40))) | ((x & @as(c_ulonglong, 0x0000ff0000000000)) >> @as(c_int, 24))) | ((x & @as(c_ulonglong, 0x000000ff00000000)) >> @as(c_int, 8))) | ((x & @as(c_ulonglong, 0x00000000ff000000)) << @as(c_int, 8))) | ((x & @as(c_ulonglong, 0x0000000000ff0000)) << @as(c_int, 24))) | ((x & @as(c_ulonglong, 0x000000000000ff00)) << @as(c_int, 40))) | ((x & @as(c_ulonglong, 0x00000000000000ff)) << @as(c_int, 56))) {
    return ((((((((x & @as(c_ulonglong, 0xff00000000000000)) >> @as(c_int, 56)) | ((x & @as(c_ulonglong, 0x00ff000000000000)) >> @as(c_int, 40))) | ((x & @as(c_ulonglong, 0x0000ff0000000000)) >> @as(c_int, 24))) | ((x & @as(c_ulonglong, 0x000000ff00000000)) >> @as(c_int, 8))) | ((x & @as(c_ulonglong, 0x00000000ff000000)) << @as(c_int, 8))) | ((x & @as(c_ulonglong, 0x0000000000ff0000)) << @as(c_int, 24))) | ((x & @as(c_ulonglong, 0x000000000000ff00)) << @as(c_int, 40))) | ((x & @as(c_ulonglong, 0x00000000000000ff)) << @as(c_int, 56));
}
pub const _BITS_UINTN_IDENTITY_H = @as(c_int, 1);
pub inline fn htobe16(x: anytype) @TypeOf(__bswap_16(x)) {
    return __bswap_16(x);
}
pub inline fn htole16(x: anytype) @TypeOf(__uint16_identity(x)) {
    return __uint16_identity(x);
}
pub inline fn be16toh(x: anytype) @TypeOf(__bswap_16(x)) {
    return __bswap_16(x);
}
pub inline fn le16toh(x: anytype) @TypeOf(__uint16_identity(x)) {
    return __uint16_identity(x);
}
pub inline fn htobe32(x: anytype) @TypeOf(__bswap_32(x)) {
    return __bswap_32(x);
}
pub inline fn htole32(x: anytype) @TypeOf(__uint32_identity(x)) {
    return __uint32_identity(x);
}
pub inline fn be32toh(x: anytype) @TypeOf(__bswap_32(x)) {
    return __bswap_32(x);
}
pub inline fn le32toh(x: anytype) @TypeOf(__uint32_identity(x)) {
    return __uint32_identity(x);
}
pub inline fn htobe64(x: anytype) @TypeOf(__bswap_64(x)) {
    return __bswap_64(x);
}
pub inline fn htole64(x: anytype) @TypeOf(__uint64_identity(x)) {
    return __uint64_identity(x);
}
pub inline fn be64toh(x: anytype) @TypeOf(__bswap_64(x)) {
    return __bswap_64(x);
}
pub inline fn le64toh(x: anytype) @TypeOf(__uint64_identity(x)) {
    return __uint64_identity(x);
}
pub const _SYS_SELECT_H = @as(c_int, 1);
pub inline fn __FD_ISSET(d: anytype, s: anytype) @TypeOf((__FDS_BITS(s)[__FD_ELT(d)] & __FD_MASK(d)) != @as(c_int, 0)) {
    return (__FDS_BITS(s)[__FD_ELT(d)] & __FD_MASK(d)) != @as(c_int, 0);
}
pub const __sigset_t_defined = @as(c_int, 1);
pub const ____sigset_t_defined = "";
pub const _SIGSET_NWORDS = @as(c_int, 1024) / (@as(c_int, 8) * @import("std").zig.c_translation.sizeof(c_ulong));
pub const __timeval_defined = @as(c_int, 1);
pub const _STRUCT_TIMESPEC = @as(c_int, 1);
pub const __suseconds_t_defined = "";
pub const __NFDBITS = @as(c_int, 8) * @import("std").zig.c_translation.cast(c_int, @import("std").zig.c_translation.sizeof(__fd_mask));
pub inline fn __FD_ELT(d: anytype) @TypeOf(d / __NFDBITS) {
    return d / __NFDBITS;
}
pub inline fn __FD_MASK(d: anytype) __fd_mask {
    return @import("std").zig.c_translation.cast(__fd_mask, @as(c_ulong, 1) << (d % __NFDBITS));
}
pub inline fn __FDS_BITS(set: anytype) @TypeOf(set.*.__fds_bits) {
    return set.*.__fds_bits;
}
pub const FD_SETSIZE = __FD_SETSIZE;
pub const NFDBITS = __NFDBITS;
pub inline fn FD_SET(fd: anytype, fdsetp: anytype) @TypeOf(__FD_SET(fd, fdsetp)) {
    return __FD_SET(fd, fdsetp);
}
pub inline fn FD_CLR(fd: anytype, fdsetp: anytype) @TypeOf(__FD_CLR(fd, fdsetp)) {
    return __FD_CLR(fd, fdsetp);
}
pub inline fn FD_ISSET(fd: anytype, fdsetp: anytype) @TypeOf(__FD_ISSET(fd, fdsetp)) {
    return __FD_ISSET(fd, fdsetp);
}
pub inline fn FD_ZERO(fdsetp: anytype) @TypeOf(__FD_ZERO(fdsetp)) {
    return __FD_ZERO(fdsetp);
}
pub const __blksize_t_defined = "";
pub const __blkcnt_t_defined = "";
pub const __fsblkcnt_t_defined = "";
pub const __fsfilcnt_t_defined = "";
pub const _BITS_PTHREADTYPES_COMMON_H = @as(c_int, 1);
pub const _THREAD_SHARED_TYPES_H = @as(c_int, 1);
pub const _BITS_PTHREADTYPES_ARCH_H = @as(c_int, 1);
pub const __SIZEOF_PTHREAD_MUTEX_T = @as(c_int, 40);
pub const __SIZEOF_PTHREAD_ATTR_T = @as(c_int, 56);
pub const __SIZEOF_PTHREAD_RWLOCK_T = @as(c_int, 56);
pub const __SIZEOF_PTHREAD_BARRIER_T = @as(c_int, 32);
pub const __SIZEOF_PTHREAD_MUTEXATTR_T = @as(c_int, 4);
pub const __SIZEOF_PTHREAD_COND_T = @as(c_int, 48);
pub const __SIZEOF_PTHREAD_CONDATTR_T = @as(c_int, 4);
pub const __SIZEOF_PTHREAD_RWLOCKATTR_T = @as(c_int, 8);
pub const __SIZEOF_PTHREAD_BARRIERATTR_T = @as(c_int, 4);
pub const __LOCK_ALIGNMENT = "";
pub const __ONCE_ALIGNMENT = "";
pub const _BITS_ATOMIC_WIDE_COUNTER_H = "";
pub const _THREAD_MUTEX_INTERNAL_H = @as(c_int, 1);
pub const __PTHREAD_MUTEX_HAVE_PREV = @as(c_int, 1);
pub const _RWLOCK_INTERNAL_H = "";
pub inline fn __PTHREAD_RWLOCK_INITIALIZER(__flags: anytype) @TypeOf(__flags) {
    return blk: {
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @TypeOf(__PTHREAD_RWLOCK_ELISION_EXTRA);
        _ = @as(c_int, 0);
        break :blk __flags;
    };
}
pub const __have_pthread_attr_t = @as(c_int, 1);
pub const _ALLOCA_H = @as(c_int, 1);
pub const __COMPAR_FN_T = "";
pub const _STDINT_H = @as(c_int, 1);
pub const _BITS_WCHAR_H = @as(c_int, 1);
pub const __WCHAR_MAX = __WCHAR_MAX__;
pub const __WCHAR_MIN = -__WCHAR_MAX - @as(c_int, 1);
pub const _BITS_STDINT_UINTN_H = @as(c_int, 1);
pub const __intptr_t_defined = "";
pub const __INT64_C = @import("std").zig.c_translation.Macros.L_SUFFIX;
pub const __UINT64_C = @import("std").zig.c_translation.Macros.UL_SUFFIX;
pub const INT8_MIN = -@as(c_int, 128);
pub const INT16_MIN = -@as(c_int, 32767) - @as(c_int, 1);
pub const INT32_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal) - @as(c_int, 1);
pub const INT64_MIN = -__INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal)) - @as(c_int, 1);
pub const INT8_MAX = @as(c_int, 127);
pub const INT16_MAX = @as(c_int, 32767);
pub const INT32_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const INT64_MAX = __INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal));
pub const UINT8_MAX = @as(c_int, 255);
pub const UINT16_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal);
pub const UINT32_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const UINT64_MAX = __UINT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 18446744073709551615, .decimal));
pub const INT_LEAST8_MIN = -@as(c_int, 128);
pub const INT_LEAST16_MIN = -@as(c_int, 32767) - @as(c_int, 1);
pub const INT_LEAST32_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal) - @as(c_int, 1);
pub const INT_LEAST64_MIN = -__INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal)) - @as(c_int, 1);
pub const INT_LEAST8_MAX = @as(c_int, 127);
pub const INT_LEAST16_MAX = @as(c_int, 32767);
pub const INT_LEAST32_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const INT_LEAST64_MAX = __INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal));
pub const UINT_LEAST8_MAX = @as(c_int, 255);
pub const UINT_LEAST16_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal);
pub const UINT_LEAST32_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const UINT_LEAST64_MAX = __UINT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 18446744073709551615, .decimal));
pub const INT_FAST8_MIN = -@as(c_int, 128);
pub const INT_FAST16_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal) - @as(c_int, 1);
pub const INT_FAST32_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal) - @as(c_int, 1);
pub const INT_FAST64_MIN = -__INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal)) - @as(c_int, 1);
pub const INT_FAST8_MAX = @as(c_int, 127);
pub const INT_FAST16_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const INT_FAST32_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const INT_FAST64_MAX = __INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal));
pub const UINT_FAST8_MAX = @as(c_int, 255);
pub const UINT_FAST16_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const UINT_FAST32_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const UINT_FAST64_MAX = __UINT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 18446744073709551615, .decimal));
pub const INTPTR_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal) - @as(c_int, 1);
pub const INTPTR_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const UINTPTR_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const INTMAX_MIN = -__INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal)) - @as(c_int, 1);
pub const INTMAX_MAX = __INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal));
pub const UINTMAX_MAX = __UINT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 18446744073709551615, .decimal));
pub const PTRDIFF_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal) - @as(c_int, 1);
pub const PTRDIFF_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const SIG_ATOMIC_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal) - @as(c_int, 1);
pub const SIG_ATOMIC_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const SIZE_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const WCHAR_MIN = __WCHAR_MIN;
pub const WCHAR_MAX = __WCHAR_MAX;
pub const WINT_MIN = @as(c_uint, 0);
pub const WINT_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub inline fn INT8_C(c: anytype) @TypeOf(c) {
    return c;
}
pub inline fn INT16_C(c: anytype) @TypeOf(c) {
    return c;
}
pub inline fn INT32_C(c: anytype) @TypeOf(c) {
    return c;
}
pub const INT64_C = @import("std").zig.c_translation.Macros.L_SUFFIX;
pub inline fn UINT8_C(c: anytype) @TypeOf(c) {
    return c;
}
pub inline fn UINT16_C(c: anytype) @TypeOf(c) {
    return c;
}
pub const UINT32_C = @import("std").zig.c_translation.Macros.U_SUFFIX;
pub const UINT64_C = @import("std").zig.c_translation.Macros.UL_SUFFIX;
pub const INTMAX_C = @import("std").zig.c_translation.Macros.L_SUFFIX;
pub const UINTMAX_C = @import("std").zig.c_translation.Macros.UL_SUFFIX;
pub const _STRING_H = @as(c_int, 1);
pub const _BITS_TYPES_LOCALE_T_H = @as(c_int, 1);
pub const _BITS_TYPES___LOCALE_T_H = @as(c_int, 1);
pub const _STRINGS_H = @as(c_int, 1);
pub const __STDARG_H = "";
pub const _VA_LIST = "";
pub const __GNUC_VA_LIST = @as(c_int, 1);
pub const _SETJMP_H = @as(c_int, 1);
pub const _BITS_SETJMP_H = @as(c_int, 1);
pub const __jmp_buf_tag_defined = @as(c_int, 1);
pub inline fn sigsetjmp(env: anytype, savemask: anytype) @TypeOf(__sigsetjmp(env, savemask)) {
    return __sigsetjmp(env, savemask);
}
pub const __STDDEF_H = "";
pub const __need_ptrdiff_t = "";
pub const __need_STDDEF_H_misc = "";
pub const _PTRDIFF_T = "";
pub const __CLANG_MAX_ALIGN_T_DEFINED = "";
pub const _STDIO_H = @as(c_int, 1);
pub const __need___va_list = "";
pub const _____fpos_t_defined = @as(c_int, 1);
pub const ____mbstate_t_defined = @as(c_int, 1);
pub const _____fpos64_t_defined = @as(c_int, 1);
pub const ____FILE_defined = @as(c_int, 1);
pub const __FILE_defined = @as(c_int, 1);
pub const __struct_FILE_defined = @as(c_int, 1);
pub const _IO_EOF_SEEN = @as(c_int, 0x0010);
pub inline fn __feof_unlocked_body(_fp: anytype) @TypeOf((_fp.*._flags & _IO_EOF_SEEN) != @as(c_int, 0)) {
    return (_fp.*._flags & _IO_EOF_SEEN) != @as(c_int, 0);
}
pub const _IO_ERR_SEEN = @as(c_int, 0x0020);
pub inline fn __ferror_unlocked_body(_fp: anytype) @TypeOf((_fp.*._flags & _IO_ERR_SEEN) != @as(c_int, 0)) {
    return (_fp.*._flags & _IO_ERR_SEEN) != @as(c_int, 0);
}
pub const _IO_USER_LOCK = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x8000, .hexadecimal);
pub const _VA_LIST_DEFINED = "";
pub const _IOFBF = @as(c_int, 0);
pub const _IOLBF = @as(c_int, 1);
pub const _IONBF = @as(c_int, 2);
pub const BUFSIZ = @as(c_int, 8192);
pub const EOF = -@as(c_int, 1);
pub const SEEK_SET = @as(c_int, 0);
pub const SEEK_CUR = @as(c_int, 1);
pub const SEEK_END = @as(c_int, 2);
pub const P_tmpdir = "/tmp";
pub const _BITS_STDIO_LIM_H = @as(c_int, 1);
pub const L_tmpnam = @as(c_int, 20);
pub const TMP_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 238328, .decimal);
pub const FILENAME_MAX = @as(c_int, 4096);
pub const L_ctermid = @as(c_int, 9);
pub const FOPEN_MAX = @as(c_int, 16);
pub const __attr_dealloc_fclose = __attr_dealloc(fclose, @as(c_int, 1));
pub const JANET_HANDLE_NONE = -@as(c_int, 1);
pub const JANET_SIGNAL_EVENT = JANET_SIGNAL_USER9;
pub const JANET_SIGNAL_INTERRUPT = JANET_SIGNAL_USER8;
pub const JANET_COUNT_TYPES = JANET_POINTER + @as(c_int, 1);
pub const JANET_TFLAG_NIL = @as(c_int, 1) << JANET_NIL;
pub const JANET_TFLAG_BOOLEAN = @as(c_int, 1) << JANET_BOOLEAN;
pub const JANET_TFLAG_FIBER = @as(c_int, 1) << JANET_FIBER;
pub const JANET_TFLAG_NUMBER = @as(c_int, 1) << JANET_NUMBER;
pub const JANET_TFLAG_STRING = @as(c_int, 1) << JANET_STRING;
pub const JANET_TFLAG_SYMBOL = @as(c_int, 1) << JANET_SYMBOL;
pub const JANET_TFLAG_KEYWORD = @as(c_int, 1) << JANET_KEYWORD;
pub const JANET_TFLAG_ARRAY = @as(c_int, 1) << JANET_ARRAY;
pub const JANET_TFLAG_TUPLE = @as(c_int, 1) << JANET_TUPLE;
pub const JANET_TFLAG_TABLE = @as(c_int, 1) << JANET_TABLE;
pub const JANET_TFLAG_STRUCT = @as(c_int, 1) << JANET_STRUCT;
pub const JANET_TFLAG_BUFFER = @as(c_int, 1) << JANET_BUFFER;
pub const JANET_TFLAG_FUNCTION = @as(c_int, 1) << JANET_FUNCTION;
pub const JANET_TFLAG_CFUNCTION = @as(c_int, 1) << JANET_CFUNCTION;
pub const JANET_TFLAG_ABSTRACT = @as(c_int, 1) << JANET_ABSTRACT;
pub const JANET_TFLAG_POINTER = @as(c_int, 1) << JANET_POINTER;
pub const JANET_TFLAG_BYTES = ((JANET_TFLAG_STRING | JANET_TFLAG_SYMBOL) | JANET_TFLAG_BUFFER) | JANET_TFLAG_KEYWORD;
pub const JANET_TFLAG_INDEXED = JANET_TFLAG_ARRAY | JANET_TFLAG_TUPLE;
pub const JANET_TFLAG_DICTIONARY = JANET_TFLAG_TABLE | JANET_TFLAG_STRUCT;
pub const JANET_TFLAG_LENGTHABLE = (JANET_TFLAG_BYTES | JANET_TFLAG_INDEXED) | JANET_TFLAG_DICTIONARY;
pub const JANET_TFLAG_CALLABLE = ((JANET_TFLAG_FUNCTION | JANET_TFLAG_CFUNCTION) | JANET_TFLAG_LENGTHABLE) | JANET_TFLAG_ABSTRACT;
pub const JANET_STREAM_CLOSED = @as(c_int, 0x1);
pub const JANET_STREAM_SOCKET = @as(c_int, 0x2);
pub const JANET_STREAM_IOCP = @as(c_int, 0x4);
pub const JANET_STREAM_READABLE = @as(c_int, 0x200);
pub const JANET_STREAM_WRITABLE = @as(c_int, 0x400);
pub const JANET_STREAM_ACCEPTABLE = @as(c_int, 0x800);
pub const JANET_STREAM_UDPSERVER = @as(c_int, 0x1000);
pub const JANET_ASYNC_LISTEN_READ = @as(c_int, 1) << JANET_ASYNC_EVENT_READ;
pub const JANET_ASYNC_LISTEN_WRITE = @as(c_int, 1) << JANET_ASYNC_EVENT_WRITE;
pub const _MATH_H = @as(c_int, 1);
pub const _BITS_LIBM_SIMD_DECL_STUBS_H = @as(c_int, 1);
pub const __DECL_SIMD_cos = "";
pub const __DECL_SIMD_cosf = "";
pub const __DECL_SIMD_cosl = "";
pub const __DECL_SIMD_cosf16 = "";
pub const __DECL_SIMD_cosf32 = "";
pub const __DECL_SIMD_cosf64 = "";
pub const __DECL_SIMD_cosf128 = "";
pub const __DECL_SIMD_cosf32x = "";
pub const __DECL_SIMD_cosf64x = "";
pub const __DECL_SIMD_cosf128x = "";
pub const __DECL_SIMD_sin = "";
pub const __DECL_SIMD_sinf = "";
pub const __DECL_SIMD_sinl = "";
pub const __DECL_SIMD_sinf16 = "";
pub const __DECL_SIMD_sinf32 = "";
pub const __DECL_SIMD_sinf64 = "";
pub const __DECL_SIMD_sinf128 = "";
pub const __DECL_SIMD_sinf32x = "";
pub const __DECL_SIMD_sinf64x = "";
pub const __DECL_SIMD_sinf128x = "";
pub const __DECL_SIMD_sincos = "";
pub const __DECL_SIMD_sincosf = "";
pub const __DECL_SIMD_sincosl = "";
pub const __DECL_SIMD_sincosf16 = "";
pub const __DECL_SIMD_sincosf32 = "";
pub const __DECL_SIMD_sincosf64 = "";
pub const __DECL_SIMD_sincosf128 = "";
pub const __DECL_SIMD_sincosf32x = "";
pub const __DECL_SIMD_sincosf64x = "";
pub const __DECL_SIMD_sincosf128x = "";
pub const __DECL_SIMD_log = "";
pub const __DECL_SIMD_logf = "";
pub const __DECL_SIMD_logl = "";
pub const __DECL_SIMD_logf16 = "";
pub const __DECL_SIMD_logf32 = "";
pub const __DECL_SIMD_logf64 = "";
pub const __DECL_SIMD_logf128 = "";
pub const __DECL_SIMD_logf32x = "";
pub const __DECL_SIMD_logf64x = "";
pub const __DECL_SIMD_logf128x = "";
pub const __DECL_SIMD_exp = "";
pub const __DECL_SIMD_expf = "";
pub const __DECL_SIMD_expl = "";
pub const __DECL_SIMD_expf16 = "";
pub const __DECL_SIMD_expf32 = "";
pub const __DECL_SIMD_expf64 = "";
pub const __DECL_SIMD_expf128 = "";
pub const __DECL_SIMD_expf32x = "";
pub const __DECL_SIMD_expf64x = "";
pub const __DECL_SIMD_expf128x = "";
pub const __DECL_SIMD_pow = "";
pub const __DECL_SIMD_powf = "";
pub const __DECL_SIMD_powl = "";
pub const __DECL_SIMD_powf16 = "";
pub const __DECL_SIMD_powf32 = "";
pub const __DECL_SIMD_powf64 = "";
pub const __DECL_SIMD_powf128 = "";
pub const __DECL_SIMD_powf32x = "";
pub const __DECL_SIMD_powf64x = "";
pub const __DECL_SIMD_powf128x = "";
pub const __DECL_SIMD_acos = "";
pub const __DECL_SIMD_acosf = "";
pub const __DECL_SIMD_acosl = "";
pub const __DECL_SIMD_acosf16 = "";
pub const __DECL_SIMD_acosf32 = "";
pub const __DECL_SIMD_acosf64 = "";
pub const __DECL_SIMD_acosf128 = "";
pub const __DECL_SIMD_acosf32x = "";
pub const __DECL_SIMD_acosf64x = "";
pub const __DECL_SIMD_acosf128x = "";
pub const __DECL_SIMD_atan = "";
pub const __DECL_SIMD_atanf = "";
pub const __DECL_SIMD_atanl = "";
pub const __DECL_SIMD_atanf16 = "";
pub const __DECL_SIMD_atanf32 = "";
pub const __DECL_SIMD_atanf64 = "";
pub const __DECL_SIMD_atanf128 = "";
pub const __DECL_SIMD_atanf32x = "";
pub const __DECL_SIMD_atanf64x = "";
pub const __DECL_SIMD_atanf128x = "";
pub const __DECL_SIMD_asin = "";
pub const __DECL_SIMD_asinf = "";
pub const __DECL_SIMD_asinl = "";
pub const __DECL_SIMD_asinf16 = "";
pub const __DECL_SIMD_asinf32 = "";
pub const __DECL_SIMD_asinf64 = "";
pub const __DECL_SIMD_asinf128 = "";
pub const __DECL_SIMD_asinf32x = "";
pub const __DECL_SIMD_asinf64x = "";
pub const __DECL_SIMD_asinf128x = "";
pub const __DECL_SIMD_hypot = "";
pub const __DECL_SIMD_hypotf = "";
pub const __DECL_SIMD_hypotl = "";
pub const __DECL_SIMD_hypotf16 = "";
pub const __DECL_SIMD_hypotf32 = "";
pub const __DECL_SIMD_hypotf64 = "";
pub const __DECL_SIMD_hypotf128 = "";
pub const __DECL_SIMD_hypotf32x = "";
pub const __DECL_SIMD_hypotf64x = "";
pub const __DECL_SIMD_hypotf128x = "";
pub const __DECL_SIMD_exp2 = "";
pub const __DECL_SIMD_exp2f = "";
pub const __DECL_SIMD_exp2l = "";
pub const __DECL_SIMD_exp2f16 = "";
pub const __DECL_SIMD_exp2f32 = "";
pub const __DECL_SIMD_exp2f64 = "";
pub const __DECL_SIMD_exp2f128 = "";
pub const __DECL_SIMD_exp2f32x = "";
pub const __DECL_SIMD_exp2f64x = "";
pub const __DECL_SIMD_exp2f128x = "";
pub const __DECL_SIMD_exp10 = "";
pub const __DECL_SIMD_exp10f = "";
pub const __DECL_SIMD_exp10l = "";
pub const __DECL_SIMD_exp10f16 = "";
pub const __DECL_SIMD_exp10f32 = "";
pub const __DECL_SIMD_exp10f64 = "";
pub const __DECL_SIMD_exp10f128 = "";
pub const __DECL_SIMD_exp10f32x = "";
pub const __DECL_SIMD_exp10f64x = "";
pub const __DECL_SIMD_exp10f128x = "";
pub const __DECL_SIMD_cosh = "";
pub const __DECL_SIMD_coshf = "";
pub const __DECL_SIMD_coshl = "";
pub const __DECL_SIMD_coshf16 = "";
pub const __DECL_SIMD_coshf32 = "";
pub const __DECL_SIMD_coshf64 = "";
pub const __DECL_SIMD_coshf128 = "";
pub const __DECL_SIMD_coshf32x = "";
pub const __DECL_SIMD_coshf64x = "";
pub const __DECL_SIMD_coshf128x = "";
pub const __DECL_SIMD_expm1 = "";
pub const __DECL_SIMD_expm1f = "";
pub const __DECL_SIMD_expm1l = "";
pub const __DECL_SIMD_expm1f16 = "";
pub const __DECL_SIMD_expm1f32 = "";
pub const __DECL_SIMD_expm1f64 = "";
pub const __DECL_SIMD_expm1f128 = "";
pub const __DECL_SIMD_expm1f32x = "";
pub const __DECL_SIMD_expm1f64x = "";
pub const __DECL_SIMD_expm1f128x = "";
pub const __DECL_SIMD_sinh = "";
pub const __DECL_SIMD_sinhf = "";
pub const __DECL_SIMD_sinhl = "";
pub const __DECL_SIMD_sinhf16 = "";
pub const __DECL_SIMD_sinhf32 = "";
pub const __DECL_SIMD_sinhf64 = "";
pub const __DECL_SIMD_sinhf128 = "";
pub const __DECL_SIMD_sinhf32x = "";
pub const __DECL_SIMD_sinhf64x = "";
pub const __DECL_SIMD_sinhf128x = "";
pub const __DECL_SIMD_cbrt = "";
pub const __DECL_SIMD_cbrtf = "";
pub const __DECL_SIMD_cbrtl = "";
pub const __DECL_SIMD_cbrtf16 = "";
pub const __DECL_SIMD_cbrtf32 = "";
pub const __DECL_SIMD_cbrtf64 = "";
pub const __DECL_SIMD_cbrtf128 = "";
pub const __DECL_SIMD_cbrtf32x = "";
pub const __DECL_SIMD_cbrtf64x = "";
pub const __DECL_SIMD_cbrtf128x = "";
pub const __DECL_SIMD_atan2 = "";
pub const __DECL_SIMD_atan2f = "";
pub const __DECL_SIMD_atan2l = "";
pub const __DECL_SIMD_atan2f16 = "";
pub const __DECL_SIMD_atan2f32 = "";
pub const __DECL_SIMD_atan2f64 = "";
pub const __DECL_SIMD_atan2f128 = "";
pub const __DECL_SIMD_atan2f32x = "";
pub const __DECL_SIMD_atan2f64x = "";
pub const __DECL_SIMD_atan2f128x = "";
pub const __DECL_SIMD_log10 = "";
pub const __DECL_SIMD_log10f = "";
pub const __DECL_SIMD_log10l = "";
pub const __DECL_SIMD_log10f16 = "";
pub const __DECL_SIMD_log10f32 = "";
pub const __DECL_SIMD_log10f64 = "";
pub const __DECL_SIMD_log10f128 = "";
pub const __DECL_SIMD_log10f32x = "";
pub const __DECL_SIMD_log10f64x = "";
pub const __DECL_SIMD_log10f128x = "";
pub const __DECL_SIMD_log2 = "";
pub const __DECL_SIMD_log2f = "";
pub const __DECL_SIMD_log2l = "";
pub const __DECL_SIMD_log2f16 = "";
pub const __DECL_SIMD_log2f32 = "";
pub const __DECL_SIMD_log2f64 = "";
pub const __DECL_SIMD_log2f128 = "";
pub const __DECL_SIMD_log2f32x = "";
pub const __DECL_SIMD_log2f64x = "";
pub const __DECL_SIMD_log2f128x = "";
pub const __DECL_SIMD_log1p = "";
pub const __DECL_SIMD_log1pf = "";
pub const __DECL_SIMD_log1pl = "";
pub const __DECL_SIMD_log1pf16 = "";
pub const __DECL_SIMD_log1pf32 = "";
pub const __DECL_SIMD_log1pf64 = "";
pub const __DECL_SIMD_log1pf128 = "";
pub const __DECL_SIMD_log1pf32x = "";
pub const __DECL_SIMD_log1pf64x = "";
pub const __DECL_SIMD_log1pf128x = "";
pub const __DECL_SIMD_atanh = "";
pub const __DECL_SIMD_atanhf = "";
pub const __DECL_SIMD_atanhl = "";
pub const __DECL_SIMD_atanhf16 = "";
pub const __DECL_SIMD_atanhf32 = "";
pub const __DECL_SIMD_atanhf64 = "";
pub const __DECL_SIMD_atanhf128 = "";
pub const __DECL_SIMD_atanhf32x = "";
pub const __DECL_SIMD_atanhf64x = "";
pub const __DECL_SIMD_atanhf128x = "";
pub const __DECL_SIMD_acosh = "";
pub const __DECL_SIMD_acoshf = "";
pub const __DECL_SIMD_acoshl = "";
pub const __DECL_SIMD_acoshf16 = "";
pub const __DECL_SIMD_acoshf32 = "";
pub const __DECL_SIMD_acoshf64 = "";
pub const __DECL_SIMD_acoshf128 = "";
pub const __DECL_SIMD_acoshf32x = "";
pub const __DECL_SIMD_acoshf64x = "";
pub const __DECL_SIMD_acoshf128x = "";
pub const __DECL_SIMD_erf = "";
pub const __DECL_SIMD_erff = "";
pub const __DECL_SIMD_erfl = "";
pub const __DECL_SIMD_erff16 = "";
pub const __DECL_SIMD_erff32 = "";
pub const __DECL_SIMD_erff64 = "";
pub const __DECL_SIMD_erff128 = "";
pub const __DECL_SIMD_erff32x = "";
pub const __DECL_SIMD_erff64x = "";
pub const __DECL_SIMD_erff128x = "";
pub const __DECL_SIMD_tanh = "";
pub const __DECL_SIMD_tanhf = "";
pub const __DECL_SIMD_tanhl = "";
pub const __DECL_SIMD_tanhf16 = "";
pub const __DECL_SIMD_tanhf32 = "";
pub const __DECL_SIMD_tanhf64 = "";
pub const __DECL_SIMD_tanhf128 = "";
pub const __DECL_SIMD_tanhf32x = "";
pub const __DECL_SIMD_tanhf64x = "";
pub const __DECL_SIMD_tanhf128x = "";
pub const __DECL_SIMD_asinh = "";
pub const __DECL_SIMD_asinhf = "";
pub const __DECL_SIMD_asinhl = "";
pub const __DECL_SIMD_asinhf16 = "";
pub const __DECL_SIMD_asinhf32 = "";
pub const __DECL_SIMD_asinhf64 = "";
pub const __DECL_SIMD_asinhf128 = "";
pub const __DECL_SIMD_asinhf32x = "";
pub const __DECL_SIMD_asinhf64x = "";
pub const __DECL_SIMD_asinhf128x = "";
pub const __DECL_SIMD_erfc = "";
pub const __DECL_SIMD_erfcf = "";
pub const __DECL_SIMD_erfcl = "";
pub const __DECL_SIMD_erfcf16 = "";
pub const __DECL_SIMD_erfcf32 = "";
pub const __DECL_SIMD_erfcf64 = "";
pub const __DECL_SIMD_erfcf128 = "";
pub const __DECL_SIMD_erfcf32x = "";
pub const __DECL_SIMD_erfcf64x = "";
pub const __DECL_SIMD_erfcf128x = "";
pub const __DECL_SIMD_tan = "";
pub const __DECL_SIMD_tanf = "";
pub const __DECL_SIMD_tanl = "";
pub const __DECL_SIMD_tanf16 = "";
pub const __DECL_SIMD_tanf32 = "";
pub const __DECL_SIMD_tanf64 = "";
pub const __DECL_SIMD_tanf128 = "";
pub const __DECL_SIMD_tanf32x = "";
pub const __DECL_SIMD_tanf64x = "";
pub const __DECL_SIMD_tanf128x = "";
pub const HUGE_VALF = __builtin_huge_valf();
pub const INFINITY = __builtin_inff();
pub const NAN = __builtin_nanf("");
pub const __GLIBC_FLT_EVAL_METHOD = __FLT_EVAL_METHOD__;
pub const __FP_LOGB0_IS_MIN = @as(c_int, 1);
pub const __FP_LOGBNAN_IS_MIN = @as(c_int, 1);
pub const FP_ILOGB0 = -@import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal) - @as(c_int, 1);
pub const FP_ILOGBNAN = -@import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal) - @as(c_int, 1);
pub inline fn __MATHCALL(function: anytype, suffix: anytype, args: anytype) @TypeOf(__MATHDECL(_Mdouble_, function, suffix, args)) {
    return __MATHDECL(_Mdouble_, function, suffix, args);
}
pub inline fn __MATHCALLX(function: anytype, suffix: anytype, args: anytype, attrib: anytype) @TypeOf(__MATHDECLX(_Mdouble_, function, suffix, args, attrib)) {
    return __MATHDECLX(_Mdouble_, function, suffix, args, attrib);
}
pub inline fn __MATHDECL_1(@"type": anytype, function: anytype, suffix: anytype, args: anytype) @TypeOf(__MATHDECL_1_IMPL(@"type", function, suffix, args)) {
    return __MATHDECL_1_IMPL(@"type", function, suffix, args);
}
pub inline fn __MATHDECL_ALIAS(@"type": anytype, function: anytype, suffix: anytype, args: anytype, alias: anytype) @TypeOf(__MATHDECL_1(@"type", function, suffix, args)) {
    _ = @TypeOf(alias);
    return __MATHDECL_1(@"type", function, suffix, args);
}
pub const _Mdouble_ = f64;
pub inline fn __MATH_PRECNAME(name: anytype, r: anytype) @TypeOf(__CONCAT(name, r)) {
    return __CONCAT(name, r);
}
pub const __MATH_DECLARING_DOUBLE = @as(c_int, 1);
pub const __MATH_DECLARING_FLOATN = @as(c_int, 0);
pub const __MATH_DECLARE_LDOUBLE = @as(c_int, 1);
pub inline fn __MATHCALL_NARROW(func: anytype, redir: anytype, nargs: anytype) @TypeOf(__MATHCALL_NARROW_NORMAL(func, nargs)) {
    _ = @TypeOf(redir);
    return __MATHCALL_NARROW_NORMAL(func, nargs);
}
pub inline fn signbit(x: anytype) @TypeOf(__builtin_signbit(x)) {
    return __builtin_signbit(x);
}
pub const MATH_ERRNO = @as(c_int, 1);
pub const MATH_ERREXCEPT = @as(c_int, 2);
pub const math_errhandling = MATH_ERRNO | MATH_ERREXCEPT;
pub const M_E = 2.7182818284590452354;
pub const M_LOG2E = 1.4426950408889634074;
pub const M_LOG10E = 0.43429448190325182765;
pub const M_LN2 = 0.69314718055994530942;
pub const M_LN10 = 2.30258509299404568402;
pub const M_PI = 3.14159265358979323846;
pub const M_PI_2 = 1.57079632679489661923;
pub const M_PI_4 = 0.78539816339744830962;
pub const M_1_PI = 0.31830988618379067154;
pub const M_2_PI = 0.63661977236758134308;
pub const M_2_SQRTPI = 1.12837916709551257390;
pub const M_SQRT2 = 1.41421356237309504880;
pub const M_SQRT1_2 = 0.70710678118654752440;
pub inline fn janet_u64(x: anytype) @TypeOf(x.u64) {
    return x.u64;
}
pub const JANET_NANBOX_TAGBITS = @as(c_ulonglong, 0xFFFF800000000000);
pub const JANET_NANBOX_PAYLOADBITS = @as(c_ulonglong, 0x00007FFFFFFFFFFF);
pub inline fn janet_nanbox_lowtag(@"type": anytype) @TypeOf(@import("std").zig.c_translation.cast(u64, @"type") | @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x1FFF0, .hexadecimal)) {
    return @import("std").zig.c_translation.cast(u64, @"type") | @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x1FFF0, .hexadecimal);
}
pub inline fn janet_nanbox_tag(@"type": anytype) @TypeOf(janet_nanbox_lowtag(@"type") << @as(c_int, 47)) {
    return janet_nanbox_lowtag(@"type") << @as(c_int, 47);
}
pub inline fn janet_nanbox_checkauxtype(x: anytype, @"type": anytype) @TypeOf((x.u64 & JANET_NANBOX_TAGBITS) == janet_nanbox_tag(@"type")) {
    return (x.u64 & JANET_NANBOX_TAGBITS) == janet_nanbox_tag(@"type");
}
pub inline fn janet_nanbox_isnumber(x: anytype) @TypeOf(!(isnan(x.number) != 0) or (((x.u64 >> @as(c_int, 47)) & @as(c_int, 0xF)) == JANET_NUMBER)) {
    return !(isnan(x.number) != 0) or (((x.u64 >> @as(c_int, 47)) & @as(c_int, 0xF)) == JANET_NUMBER);
}
pub inline fn janet_nanbox_from_payload(t: anytype, p: anytype) @TypeOf(janet_nanbox_from_bits(janet_nanbox_tag(t) | p)) {
    return janet_nanbox_from_bits(janet_nanbox_tag(t) | p);
}
pub inline fn janet_nanbox_wrap_(p: anytype, t: anytype) @TypeOf(janet_nanbox_from_pointer(p, janet_nanbox_tag(t))) {
    return janet_nanbox_from_pointer(p, janet_nanbox_tag(t));
}
pub inline fn janet_nanbox_wrap_c(p: anytype, t: anytype) @TypeOf(janet_nanbox_from_cpointer(p, janet_nanbox_tag(t))) {
    return janet_nanbox_from_cpointer(p, janet_nanbox_tag(t));
}
pub inline fn janet_checkintrange(x: anytype) @TypeOf(((x >= INT32_MIN) and (x <= INT32_MAX)) and (x == @import("std").zig.c_translation.cast(i32, x))) {
    return ((x >= INT32_MIN) and (x <= INT32_MAX)) and (x == @import("std").zig.c_translation.cast(i32, x));
}
pub inline fn janet_checkint64range(x: anytype) @TypeOf(((x >= JANET_INTMIN_DOUBLE) and (x <= JANET_INTMAX_DOUBLE)) and (x == @import("std").zig.c_translation.cast(i64, x))) {
    return ((x >= JANET_INTMIN_DOUBLE) and (x <= JANET_INTMAX_DOUBLE)) and (x == @import("std").zig.c_translation.cast(i64, x));
}
pub const JANET_STACKFRAME_TAILCALL = @as(c_int, 1);
pub const JANET_STACKFRAME_ENTRANCE = @as(c_int, 2);
pub const JANET_FRAME_SIZE = @as(c_int, 4);
pub const JANET_FUNCDEF_FLAG_VARARG = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x10000, .hexadecimal);
pub const JANET_FUNCDEF_FLAG_NEEDSENV = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x20000, .hexadecimal);
pub const JANET_FUNCDEF_FLAG_HASNAME = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x80000, .hexadecimal);
pub const JANET_FUNCDEF_FLAG_HASSOURCE = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x100000, .hexadecimal);
pub const JANET_FUNCDEF_FLAG_HASDEFS = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x200000, .hexadecimal);
pub const JANET_FUNCDEF_FLAG_HASENVS = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x400000, .hexadecimal);
pub const JANET_FUNCDEF_FLAG_HASSOURCEMAP = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x800000, .hexadecimal);
pub const JANET_FUNCDEF_FLAG_STRUCTARG = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x1000000, .hexadecimal);
pub const JANET_FUNCDEF_FLAG_HASCLOBITSET = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x2000000, .hexadecimal);
pub const JANET_FUNCDEF_FLAG_TAG = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0xFFFF, .hexadecimal);
pub const JANET_FUNCFLAG_TRACE = @as(c_int, 1) << @as(c_int, 16);
pub const JANET_ATEND_NAME = blk: {
    _ = @TypeOf(NULL);
    break :blk JANET_ATEND_GC;
};
pub const JANET_ATEND_GC = blk: {
    _ = @TypeOf(NULL);
    break :blk JANET_ATEND_GCMARK;
};
pub const JANET_ATEND_GCMARK = blk: {
    _ = @TypeOf(NULL);
    break :blk JANET_ATEND_GET;
};
pub const JANET_ATEND_GET = blk: {
    _ = @TypeOf(NULL);
    break :blk JANET_ATEND_PUT;
};
pub const JANET_ATEND_PUT = blk: {
    _ = @TypeOf(NULL);
    break :blk JANET_ATEND_MARSHAL;
};
pub const JANET_ATEND_MARSHAL = blk: {
    _ = @TypeOf(NULL);
    break :blk JANET_ATEND_UNMARSHAL;
};
pub const JANET_ATEND_UNMARSHAL = blk: {
    _ = @TypeOf(NULL);
    break :blk JANET_ATEND_TOSTRING;
};
pub const JANET_ATEND_TOSTRING = blk: {
    _ = @TypeOf(NULL);
    break :blk JANET_ATEND_COMPARE;
};
pub const JANET_ATEND_COMPARE = blk: {
    _ = @TypeOf(NULL);
    break :blk JANET_ATEND_HASH;
};
pub const JANET_ATEND_HASH = blk: {
    _ = @TypeOf(NULL);
    break :blk JANET_ATEND_NEXT;
};
pub const JANET_ATEND_NEXT = blk: {
    _ = @TypeOf(NULL);
    break :blk JANET_ATEND_CALL;
};
pub const JANET_ATEND_CALL = "";
pub const JANET_EV_TCTAG_NIL = @as(c_int, 0);
pub const JANET_EV_TCTAG_INTEGER = @as(c_int, 1);
pub const JANET_EV_TCTAG_STRING = @as(c_int, 2);
pub const JANET_EV_TCTAG_STRINGF = @as(c_int, 3);
pub const JANET_EV_TCTAG_KEYWORD = @as(c_int, 4);
pub const JANET_EV_TCTAG_ERR_STRING = @as(c_int, 5);
pub const JANET_EV_TCTAG_ERR_STRINGF = @as(c_int, 6);
pub const JANET_EV_TCTAG_ERR_KEYWORD = @as(c_int, 7);
pub const JANET_EV_TCTAG_BOOLEAN = @as(c_int, 8);
pub const JANET_TUPLE_FLAG_BRACKETCTOR = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x10000, .hexadecimal);
pub inline fn janet_tuple_length(t: anytype) @TypeOf(janet_tuple_head(t).*.length) {
    return janet_tuple_head(t).*.length;
}
pub inline fn janet_tuple_hash(t: anytype) @TypeOf(janet_tuple_head(t).*.hash) {
    return janet_tuple_head(t).*.hash;
}
pub inline fn janet_tuple_sm_line(t: anytype) @TypeOf(janet_tuple_head(t).*.sm_line) {
    return janet_tuple_head(t).*.sm_line;
}
pub inline fn janet_tuple_sm_column(t: anytype) @TypeOf(janet_tuple_head(t).*.sm_column) {
    return janet_tuple_head(t).*.sm_column;
}
pub inline fn janet_tuple_flag(t: anytype) @TypeOf(janet_tuple_head(t).*.gc.flags) {
    return janet_tuple_head(t).*.gc.flags;
}
pub inline fn janet_string_length(s: anytype) @TypeOf(janet_string_head(s).*.length) {
    return janet_string_head(s).*.length;
}
pub inline fn janet_string_hash(s: anytype) @TypeOf(janet_string_head(s).*.hash) {
    return janet_string_head(s).*.hash;
}
pub inline fn janet_cstringv(cstr: anytype) @TypeOf(janet_wrap_string(janet_cstring(cstr))) {
    return janet_wrap_string(janet_cstring(cstr));
}
pub inline fn janet_stringv(str: anytype, len: anytype) @TypeOf(janet_wrap_string(janet_string(str, len))) {
    return janet_wrap_string(janet_string(str, len));
}
pub inline fn janet_symbolv(str: anytype, len: anytype) @TypeOf(janet_wrap_symbol(janet_symbol(str, len))) {
    return janet_wrap_symbol(janet_symbol(str, len));
}
pub inline fn janet_csymbolv(cstr: anytype) @TypeOf(janet_wrap_symbol(janet_csymbol(cstr))) {
    return janet_wrap_symbol(janet_csymbol(cstr));
}
pub const janet_keyword = janet_symbol;
pub const janet_ckeyword = janet_csymbol;
pub inline fn janet_keywordv(str: anytype, len: anytype) @TypeOf(janet_wrap_keyword(janet_keyword(str, len))) {
    return janet_wrap_keyword(janet_keyword(str, len));
}
pub inline fn janet_ckeywordv(cstr: anytype) @TypeOf(janet_wrap_keyword(janet_ckeyword(cstr))) {
    return janet_wrap_keyword(janet_ckeyword(cstr));
}
pub inline fn janet_struct_length(t: anytype) @TypeOf(janet_struct_head(t).*.length) {
    return janet_struct_head(t).*.length;
}
pub inline fn janet_struct_capacity(t: anytype) @TypeOf(janet_struct_head(t).*.capacity) {
    return janet_struct_head(t).*.capacity;
}
pub inline fn janet_struct_hash(t: anytype) @TypeOf(janet_struct_head(t).*.hash) {
    return janet_struct_head(t).*.hash;
}
pub inline fn janet_struct_proto(t: anytype) @TypeOf(janet_struct_head(t).*.proto) {
    return janet_struct_head(t).*.proto;
}
pub inline fn janet_abstract_type(u: anytype) @TypeOf(janet_abstract_head(u).*.type) {
    return janet_abstract_head(u).*.type;
}
pub inline fn janet_abstract_size(u: anytype) @TypeOf(janet_abstract_head(u).*.size) {
    return janet_abstract_head(u).*.size;
}
pub const JANET_MARSHAL_UNSAFE = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x20000, .hexadecimal);
pub const JANET_MARSHAL_NO_CYCLES = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x40000, .hexadecimal);
pub const JANET_PRETTY_COLOR = @as(c_int, 1);
pub const JANET_PRETTY_ONELINE = @as(c_int, 2);
pub const JANET_PRETTY_NOTRUNC = @as(c_int, 4);
pub inline fn janet_try(state: anytype) JanetSignal {
    return blk: {
        _ = janet_try_init(state);
        break :blk @import("std").zig.c_translation.cast(JanetSignal, setjmp(state.*.buf));
    };
}
pub inline fn janet_flag_at(F: anytype, I: anytype) @TypeOf(F & (@as(c_ulonglong, 1) << I)) {
    return F & (@as(c_ulonglong, 1) << I);
}
pub inline fn JANET_DEF_(ENV: anytype, JNAME: anytype, VAL: anytype, DOC: anytype) @TypeOf(janet_def(ENV, JNAME, VAL, NULL)) {
    _ = @TypeOf(DOC);
    return janet_def(ENV, JNAME, VAL, NULL);
}
pub inline fn JANET_DEF_D(ENV: anytype, JNAME: anytype, VAL: anytype, DOC: anytype) @TypeOf(janet_def(ENV, JNAME, VAL, DOC)) {
    return janet_def(ENV, JNAME, VAL, DOC);
}
pub const JANET_REG = JANET_REG_SD;
pub const JANET_FN = JANET_FN_SD;
pub const JANET_DEF = JANET_DEF_SD;
pub const JANET_MODULE_PREFIX = "";
pub const JANET_FILE_WRITE = @as(c_int, 1);
pub const JANET_FILE_READ = @as(c_int, 2);
pub const JANET_FILE_APPEND = @as(c_int, 4);
pub const JANET_FILE_UPDATE = @as(c_int, 8);
pub const JANET_FILE_NOT_CLOSEABLE = @as(c_int, 16);
pub const JANET_FILE_CLOSED = @as(c_int, 32);
pub const JANET_FILE_BINARY = @as(c_int, 64);
pub const JANET_FILE_SERIALIZABLE = @as(c_int, 128);
pub const JANET_FILE_NONIL = @as(c_int, 512);
pub const timeval = struct_timeval;
pub const timespec = struct_timespec;
pub const __pthread_internal_list = struct___pthread_internal_list;
pub const __pthread_internal_slist = struct___pthread_internal_slist;
pub const __pthread_mutex_s = struct___pthread_mutex_s;
pub const __pthread_rwlock_arch_t = struct___pthread_rwlock_arch_t;
pub const __pthread_cond_s = struct___pthread_cond_s;
pub const random_data = struct_random_data;
pub const drand48_data = struct_drand48_data;
pub const __locale_data = struct___locale_data;
pub const __locale_struct = struct___locale_struct;
pub const __va_list_tag = struct___va_list_tag;
pub const __jmp_buf_tag = struct___jmp_buf_tag;
pub const _G_fpos_t = struct__G_fpos_t;
pub const _G_fpos64_t = struct__G_fpos64_t;
pub const _IO_marker = struct__IO_marker;
pub const _IO_codecvt = struct__IO_codecvt;
pub const _IO_wide_data = struct__IO_wide_data;
pub const _IO_FILE = struct__IO_FILE;
pub const JanetParserStatus = enum_JanetParserStatus;
pub const JanetOpArgType = enum_JanetOpArgType;
pub const JanetInstructionType = enum_JanetInstructionType;
pub const JanetOpCode = enum_JanetOpCode;
pub const JanetAssembleStatus = enum_JanetAssembleStatus;
pub const JanetCompileStatus = enum_JanetCompileStatus;
