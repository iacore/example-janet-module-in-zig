const c = @import("./janet.h.zig");
const std = @import("std");

const Janet = c.Janet;
const JanetBuildConfig = c.JanetBuildConfig;
const JanetTable = c.JanetTable;
const JanetRegExt = c.JanetRegExt;

fn _janet_func(name: [:0]const u8, function: anytype, docstring: [:0]const u8) JanetRegExt {
    const srcinfo = @src(); // todo: get line number of function, not of here
    return .{
        .name = name.ptr,
        .cfun = function,
        .documentation = docstring.ptr,
        .source_file = srcinfo.file.ptr,
        .source_line = 0,
        // .source_line = @intCast(i32, @src().line), // zig bug
    };
}

export fn _janet_init(arg_env: [*c]JanetTable) void {
    var env = arg_env;
    var cfuns = [_]JanetRegExt{
        _janet_hello_native,
        _janet_plus,
        // sentinel
        std.mem.zeroInit(JanetRegExt, .{}),
    };
    c.janet_cfuns_ext(env, _janet_module_name, &cfuns);
}

export fn _janet_mod_config() JanetBuildConfig {
    return c.janet_config_current();
}

/// definitions

const _janet_module_name = "zig-example";


const _janet_hello_native = _janet_func("hello-native", hello_native,
    \\Greetings from Zig
);
fn hello_native(argc: i32, argv: [*c]Janet) callconv(.C) Janet {
    c.janet_fixarity(argc, 0); _ = argv;
    return c.janet_cstringv("Hello from Zig");
}

const _janet_plus = _janet_func("+", zig_plus,
    \\Addition of 2 integers
);
fn zig_plus(argc: i32, argv: [*c]Janet) callconv(.C) Janet {
    c.janet_fixarity(argc, 2);
    // const args: []Janet = argv[0..@intCast(usize, argc)];
    // _ = args;
    const a = c.janet_getinteger(argv, 0);
    const b = c.janet_getinteger(argv, 1);
    return c.janet_wrap_integer(a + b);
}
