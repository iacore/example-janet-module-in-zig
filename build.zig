const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    const lib = b.addSharedLibrary("janet-zig", "src/mod.zig", .unversioned);
    lib.linkSystemLibrary("janet"); // the module don't *need* to link janet, if the janet interpreter is dynamic executable (thus already had libjanet.so loaded)
    lib.setBuildMode(mode);
    lib.install();

    const main_tests = b.addTest("src/mod.zig");
    main_tests.linkSystemLibrary("janet");
    main_tests.setBuildMode(mode);

    const flycheck = b.addSystemCommand(&.{"janet", "flycheck.janet"});
    flycheck.step.dependOn(&lib.install_step.?.step);

    const test_step = b.step("test", "Run library tests");
    test_step.dependOn(&main_tests.step);
    test_step.dependOn(&flycheck.step);
}
